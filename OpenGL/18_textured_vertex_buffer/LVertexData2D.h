/*
Boris Merminod le 25/01/2019

	Ici aussi on ajoute une nouvelle structure de données. Pour chaque sommet, on a une position dans l'espace et des coordonnées de texture, c'est la raison d'être de notre structure LVertexData2D
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php
*/

#ifndef LVERTEX_DATA_2D_H
#define LVERTEX_DATA_2D_H

#include "LVertexPos2D.h"
#include "LTexCoord.h"

struct LVertexData2D
{
	LVertexPos2D position;
	LTexCoord texCoord;
};

#endif
