/*
Boris Merminod le 25/01/2019

	Le constructeur de la classe va en plus de tout le reste initialiser les ID de notre VBO/IBO à 0.	
	
	Voici ce qui se passe dans notre fonction initVBO() : Déjà on commence par vérifir qu'une texture existe bel et bien (et que l'ID du VBO est à 0 donc qu'il n'y a pas déjà de VBO actif).
	D'abord on va déclarer notre vertex data et notre index data sous la forme de deux tables de 4 éléments. On va se servir de ses tables pour les envoyer au GPU. Ensuite on va set les valeur de notre index data, mais pas celle de notre vertex data qui seront mise en place directement dans la fonction render(). Dans la fonction glBufferData, on utilisera le flag GL_DYNAMIC_DRAW en différence au chapitre précédent, en raison du faite que l'on va effectuer une mise à jour régulière des données du vertex data.
	Ensuite on va générer un Buffer, on lui envoie les données puis après sa création on détache le Buffer en passant NULL à glBindBuffer
	
	Notre fonction loadTextureFromPixels32 est sensiblement pareil que dans les versions précédentes, à ceci prêt qu'on lui rajoute un appel vers la fonction initVBO().
	
	Dans la fonction render(), on va commencer de la même façon que das les tutos précédent. le changement va venir du "Comment j'envoie mes données au GPU ?". La fonction commence donc par calculer les coordonnées de positions des sommets ainsi que les coordonnées des textures, le tout est ensuite set dans notre vertex data.
	Ensuite c'est le moment d'effectuer le rendu de tout ça, on va pour ça Bind notre texture via glBindTexture(), déverrouiller notre vertex coordinate array ainsi que notre texture coordinate array respectivement à l'aide des flags GL_VERTEX_ARRAY puis GL_TEXTURE_COORD_ARRAY à l'aide de la fonction glEnableClientState
	Juste avant de faire le rendu il va falloir mettre à jour les coordonnées de notre vertex data. Pour cela on va commencer par choper notre Buffer avec glBindBuffer(), puis on va appeler ensuite :
	
	glBufferSubData( GL_ARRAY_BUFFER, 0, 4 * sizeof(LVertexData2D), vData );

qui va mettre à jours les coordonnées de notre vertex data. La fonction prend en arguments le type de données que l'on va mettre à jour, le second est le byte offset par lequel on veut commencer la mise à jour(et comme je veux tout mettre à jour, je commence à 0), le troisième argument est la taille de nos données en octets soit 4 fois la structure de données LVertexData2D, le dernier argument est un pointeur sur notre vertex data.
	
	glTexCoordPointer( 2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof( LVertexData2D, texCoord ) );
	
	glVertexPointer( 2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof( LVertexData2D, position ) );
	
	Les deux fonctions ci-dessus vont permettre de set un vertex pointer et un texture coordinate pointer sur notre VBO. La deuxième ligne fonctionne comme dans le chapitre précédent, la première ligne fonctionne de façon similaire (il est si j'ai bien compris important de commencer par glTexCoordPointer avant glVertexPointer mais bon à vérifier).
	Ici on a compliqué un peu les choses puisque j'ai en une structure de données, la position et les coordonnées de textures. Si je décompose les argument des deux fonctions ci-dessus on a :
	- la dimension dans laquelle on travaille (2D)
	- le type de données pour notre pointeur (GL_FLOAT)
	- le troisième argument est ce qu'on appelle le stride que je défini plus bas
	- le quatrième argument est l'adresse de départ de la fonction que l'on va donner par le byte offset du membre correspondant dans la structure de données
	
	A propos du stride, pour rappel si une structure se compose entièrement d'éléments ayant le même type, comme le GLfloat par exemple, alors je peux traiter ma structure comme étant une table du type concerné, donc du type GLfloat.
	vertexData2D[0].position.x -> floatArray[0]
	vertexData2D[0].position.y -> floatArray[1]
	vertexData2D[0].texCoord.t -> floatArray[2]
	vertexData2D[0].texCoord.s -> floatArray[3]
	vertexData2D[1].position.x -> floatArray[4]
	vertexData2D[1].position.y -> floatArray[5]
	vertexData2D[1].texCoord.t -> floatArray[6]
	vertexData2D[1].texCoord.s -> floatArray[7]
	...
	
Le stride, c'est l'espace entre chaque set de données. Si je dis mon stride est sizeof(LVertexData2D) pour mon vertex pointer, alors l'adresse de départ de chaque vertex est une taille complètre de LVertexData2D en octets. 
	Pour positionner notre vertex pointer, je vais donc dire à OpenGL de lire deux GLfloat consécutif, puis de se décaler de la taille d'un LVertexData2D en octer, puis de relire deux autres GLfloats consécutifs, et ainsi de suite. 
	Pour les coordonnées de texture, c'est le même principe si ce n'est qu'elle sont placées différement dans la structure de données.
	L'utilisation de la macro offsetof permet de donner à notre vertex pointer ou notre texture coordinate pointer des adresses de départ différentes. offsetof prend en argument le membre de la structure de données à partir duquel on veut positionner notre pointeur.
	
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mIBOID );
	glDrawElements( GL_QUADS, 4, GL_UNSIGNED_INT, NULL );
	
	Reprenons le cours d'exécution de notre fonction render(). Une fois que nos coordonnées de vertex et de texture sont mise en place dans notre VBO et IBO, nous allons effectuer le rendu de notre figure géométrique après avoir bind notre Buffer.
	On termine en désactivant notre vertex et texture coordinate array via glDisableClientState.
	
	La fonction freeVBO() va libérer la mémoire allouée par notre buffer via glDeleteBuffers. Dans notre fonction freeTexture() on ne fait pas de freeVBO() car si je dois réutiliser une autre texture, je pourrai me servir du VBO déjà présent, ça fonctionne. La fonction freeVBO() est appelée par le destructeur de la classe pour effectuer cette libération à la destruction de l'objet LTexture.
	

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LVertexData2D.h"
//#include <il.h>

GLenum DEFAULT_TEXTURE_WRAP = GL_REPEAT;

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels = NULL;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
	
	//Initialisation du VBO
	mVBOID = 0;
	mIBOID = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
	
	freeVBO();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadPixelsFromFile(std::string path)
{
	//Libération de la mémoire allouée éventuelle pour d'autre texture
	freeTexture();

	//Flag de chargement de texture
	bool pixelsLoaded = false;

	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());

	//Si l'image s'est chargée comme il faut 
	if(success == IL_TRUE)
	{
		//Conversion de l'image vers le RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requise de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une mauvaise taille
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Place l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);

				//Redimensionne l'image
				iluEnlargeCanvas((int) texWidth, (int) texHeight, 1);
			}

			//Allocation de mémoire pour les données de texture
			GLuint size = texWidth * texHeight;
			mPixels = new GLuint[size];

			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;

			//Copier les pixels en mémoire
			memcpy(mPixels, ilGetData(), size * 4);
			pixelsLoaded = true;
		}

		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
	}

	//Report des erreurs
	if(!pixelsLoaded)
		fprintf(stderr, "Impossible de charger %s\n", path.c_str());
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromFileWithColorKey(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{
	//Charger les pixels
	if(!loadPixelsFromFile(path))
		return false;
	
	//Parcourir les pixels
	GLuint size = mTextureWidth * mTextureHeight;
	for(unsigned int i = 0; i < size; i++)
	{
		// Récupérer la couleur du pixel
		GLubyte* colors = (GLubyte*)&mPixels[i];

		//Si la couleur correspond
		if(colors[0] == r && colors[1] == g && colors[2] == b && (0 == a || colors[3] == a))
		{
			//Ajuster la transparence
			colors[0] = 255;
			colors[1] = 255;
			colors[2] = 255;
			colors[3] = 000;
		}
	}

	//Créer la texture
	return loadTextureFromPixels32();
}

bool LTexture::loadTextureFromPixels32()
{
	//Flag de succès de la fonction
	bool success = true;

	//Si l'attribut pixels est chargé
	if(mTextureID == 0 && mPixels != NULL)
	{
		//Générer une ID de texture
		glGenTextures(1, &mTextureID);

		//Attacher la textureID
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Mise en place des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur de chargement de %p pixels ! %s\n", mPixels, gluErrorString(error));
			success = false;
		}
		else 
		{
			//Suppression de l'attribut pixels
			delete [] mPixels;
			mPixels = NULL;
		}
	}
	// En cas d'erreur
	else 
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Structure mPixels non chargé, impossible de créer la texture !\n");

		//La texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La texture existe déjà \n");
		else if(mPixels == NULL)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La structure mPixels n'est pas initialisée\n");
		
		success = false;
	}

	return success;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	//Générer le VBO
	initVBO();
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels
	if(mPixels != NULL)
	{
		delete [] mPixels;
		mPixels = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		mPixels = new GLuint[size];

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if(mPixels != NULL && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Suppression de la structure pixels
		delete [] mPixels;
		mPixels = NULL;

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{	
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Set vertex data
		LVertexData2D vData[4];
		
		//Texture coordinates
		vData[0].texCoord.s = texLeft; vData[0].texCoord.t = texTop;
		vData[1].texCoord.s = texRight; vData[1].texCoord.t = texTop;
		vData[2].texCoord.s = texRight; vData[2].texCoord.t = texBottom;
		vData[3].texCoord.s = texLeft; vData[3].texCoord.t = texBottom;
		
		//Vertex positions
		vData[0].position.x = 0.0f; vData[0].position.y = 0.0f;
		vData[1].position.x = quadWidth; vData[1].position.y = 0.0f;
		vData[2].position.x = quadWidth; vData[2].position.y = quadHeight;
		vData[3].position.x = 0.0f; vData[3].position.y = quadHeight;
		
		//Set texture ID
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
			//Bind Vertex Buffer
			glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
			
			//Update vertex buffer data
			glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(LVertexData2D), vData);
			
			//Set texture coordinate data
			glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, texCoord));
			
			//Set vertex data
			glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, position));
			
			//Draw quad using vertex data and index date
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
			
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}

void LTexture::initVBO()
{
	//Si la texture est déjà chargée et que le VBO existe déjà
	if(mTextureID == 0 || mVBOID != 0)
		return;
		
	LVertexData2D vData[4];
	GLuint iData[4];
	
	//Set rendering indices
	iData[0] = 0;
	iData[1] = 1;
	iData[2] = 2;
	iData[3] = 3;
	
	//Création du VBO
	glGenBuffers(1, &mVBOID);
	glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(LVertexData2D), vData, GL_DYNAMIC_DRAW);
	
	//Création de l'IBO
	glGenBuffers(1, &mIBOID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), iData, GL_DYNAMIC_DRAW);
	
	//Détacher les buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void LTexture::freeVBO()
{
	if(mVBOID == 0)
		return;
	
	glDeleteBuffers(1, &mVBOID);
	glDeleteBuffers(1, &mIBOID);
	
}
