/*
	Boris Merminod le  25/01/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
