/*
Boris Merminod le 25/01/2019


Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php
*/

#ifndef LVERTEX_POS_2D_H
#define LVERTEX_POS_2D_H

#include "LOpenGL.h"

struct LVertexPos2D
{
	GLfloat x;
	GLfloat y;
};

#endif
