/*
Boris merminod le 25/01/2019

	Il n'y a pas de grosse inovation ici, on utilise notre texture dans un objet déclaré de manière globale nommé gVBOTexture. Une seule remarque ceci-dit, les opérations effectuée  dans le Buffer sont complètement invisible sortie de la classe LTexture.
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//VBO rendered texture
LTexture gVBOTexture;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	if(!gVBOTexture.loadTextureFromFile("sprite/opengl.png"))
	{
		fprintf(stderr,"loadMedia : Impossible de charger la texture OpenGL\n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Initialisation de la matrice modelview
	glLoadIdentity();
	
	//Render textured quad using VBOs
	gVBOTexture.render((SCREEN_WIDTH - gVBOTexture.imageWidth()) / 2.f, (SCREEN_HEIGHT - gVBOTexture.imageHeight())/2.f);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

