/*

Boris Merminod le 25/01/2019

	Ici on va créer une nouvelle structure de données qui va permettre de manipuler plus facilement les coordonnées de texture : LTexCoord
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/18_textured_vertex_buffers/index.php

*/

#ifndef LTEX_COORD_H
#define LTEX_COORD_H

#include "LOpenGL.h"

struct LTexCoord
{
	GLfloat s;
	GLfloat t;
};

#endif
