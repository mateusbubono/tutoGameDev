/*
	Boris Merminod le 11/07/2018
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/09_updating_textures/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
