/*
	Boris Merminod le 11/07/2018

	Dans le constructeur on rajoute l'initialisation du pointeur sur mPixels à NULL.

	De la même façon dans la fonction freeTexture() on rajoute la libération de la mémoire allouée pour gérer l'attribut mPixels

	Vient ensuite la méthode lock(). Pour vérrouiller une texture, on commence par vérifier que l'attribut mPixels soit NULL. Dès lors que l'attribut existe la texture est alors déjà verrouillée. Ensuite on vérifie si la texture existe via la présence de son ID. Ensuite on va allouer de la mémoire pour gérer nos pixels. 
	On va alors attacher la texture dont on veut manipuler les pixels, récupérer les données de pixels via glGetTexImage(), puis détacher la texture.

	La méthode unlock() intervient lorsque les opérations sur les pixels sont terminé et que l'on souhaite déverrouiller la texture et retourner les données des pixels vers la texture. Pour cela on utilise la fonction glTexSubImage2D(). Il est par ailleur plus efficace de faire cela en opposition à utiliser un nouvel appel à la fonction glTexImage2D(). Avant de déverrouiller on va d'abord contrôler si l'attribut mPixels existe bien et si la texture existe bien ce qui signifie que la texture est belle et bien verrouillée. Ensuite on va attacher la texture, puis mettre à jour les pixels de la texture via glTexSubImage2D(). Voici à quoi ressemble l'appel de cette fonction :
	
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

	Les paramètres 3, 4, 5 et 6 représente la portion de la texture que l'on souhaite mettre à jour en définissant le décalage en x, en y, et les dimension de la zone dans laquelle on intervient. Dans cet exemple toute la texture est mise à jour.

	Les données de nos pixels représentés sur deux dimensions, sont contenu dans une table mPixels à une dimension. Pour accéder à un pixel en particulier on va utiliser que ce soit pour l'accesseur que pour le mutateur la formulle suivante : 

	"y * mTextureWidth + x"

	On retrouve cette formule dans getPixel32() et setPixel32() et la formule permet d'accéder aux données représentées en deux dimensions dans une table à une dimension. 

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/09_updating_textures/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels = NULL;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels
	if(mPixels != NULL)
	{
		delete [] mPixels;
		mPixels = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		mPixels = new GLuint[size];

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if(mPixels != NULL && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Suppression de la structure pixels
		delete [] mPixels;
		mPixels = NULL;

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(0.f,0.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth,0.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth,quadHeight);
			glTexCoord2f(texLeft,texBottom); glVertex2f(0.f,quadHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}
