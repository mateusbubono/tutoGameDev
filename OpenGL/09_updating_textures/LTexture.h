/*
	Boris Merminod le 11/07/2018

	Dans le fichier LTexture.h, on ajoute à la classe LTexture un attribut mPixels qui va contenir les données de chaque pixel de la texture que nous allons manipuler

	Plusieurs méthodes sont également ajoutées à la classe. lock() va récupérer les pixels de la texture, verrouiller celle-ci afin que l'on puisse les manipuler. unlock() va déverrouiller() la texture après la manipulation. getPixelData32() va retourner un pointeur sur la table de l'attribut mPixels. getPixel32() et setPixel32() vont permettre d'accéder et modifier chaque pixel individuellement en accédant à un élément particulier de la table de l'attribut mPixels
 
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/09_updating_textures/index.php
*/

#ifndef LTEXTURE_H
#define LTEXTURE_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "LFRect.h"

class LTexture
{
	public :
		LTexture();
		/*
		Pré-conditions :
		-None
		Post-conditions :
		- Initialise les attributs membres
		Side effects :
		- None
		*/
		
		~LTexture();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Libère la mémoire allouée pour gérer un objet de texture
		Side effects:
		- None
		*/
		
		bool loadTextureFromFile(std::string path);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Librairie DevIL initialisée
		Post-conditions :
		- Création d'une texture à partir d'un fichier
		- Report des erreur à la sortie stderr
		Side Effects :
		- Association d'une texture NULL 
		*/
		
		bool loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Création d'une texture à partir de pixels donnés
		- Reporte les erreurs à la console si la texture n'a pas pu être créée
		Side Effects: 
		- Attache une texture NULL
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Suppression des texutres si elles existes
		- Mise en place d'un texture ID à 0
		Side Effects :
		- None
		*/

		bool lock();
		/*
		Pré-conditions :
		- Une texture existante et non verrouillée
		Post-conditions :
		- Mise en place de l'attribut pixel à partir des données de textures
		- Retourne true si les pixels de la texture ont été retrouvé
		Side Effects :
		- Attache une texture NULL
		*/

		bool unlock();
		/*
		Pré-conditions :
		- Une texture verrouillée
		Post-conditions :
		- Mise à jour de la texture à partir de ses pixels
		- Retourne true si les pixels de la texture ont été mis à jour
		Side Effects :
		- Attache une texture NULL
		*/

		GLuint * getPixelData32();
		/*
		Pré-conditions :
		- Attribut d'un ensemble de pixels disponibles
		Post-conditions :
		- Retourne l'attribut membre pixels
		Side Effects :
		- None
		*/

		GLuint getPixel32(GLuint x, GLuint y);
		/*
		Pré-conditions :
		- Attribut membre pixels valide et disponible
		Post-conditions :
		- Retourne le pixel d'une position donnée
		- La fonction devrait segfault si la texture n'est pas verrouillée au préalable
		Side Effects :
		- None
		*/

		void setPixel32(GLuint x, GLuint y, GLuint pixel);
		/*
		Pré-conditions :
		- Un attribut membre pixels valide et disponible
		Post-conditions :
		- Modifier un pixel à une position donnée
		- La fonction peut Segfault si la texture n'est pas verrouillée au préalable
		Side Effects :
		- None
		*/
		
		void render(GLfloat x, GLfloat y, LFRect* clip = NULL);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Une matrice modelview active
		Post-conditions :
		- opération de translation à une position données puis rendu de carré texturés
		- Si le paramètre clip est NULL alors la totalité de la texture est rendue
		Side  effects :
		- Associe à la texture membre une ID
		*/
		
		GLuint getTextureID();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne le nom et l'ID de la texture
		Side effects :
		- None
		*/
		
		GLuint textureWidth();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- retourne la largeur (width) de la texture
		Side effects :
		- None
		*/
		
		GLuint textureHeight();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne la hauteur de la texture (height)
		Side effects :
		- None
		*/

		GLuint imageWidth();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image width
		Side Effects :
		- None
		*/

		GLuint imageHeight();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image height
		Side Effects :
		- None
		*/
		
	private :
		GLuint powerOfTwo(GLuint num);
		/*
		Pré-conditions :
		- None
		Post Conditions :
		- Retourne la puissance de deux entières la plus proche (et plus grande) que la valeur passée en paramètres
		Side Effects :
		- None
		*/
	
		//Nom de la texture
		GLuint mTextureID;

		//Les pixels courant
		GLuint * mPixels;
		
		//Dimensions de la texture
		GLuint mTextureWidth;
		GLuint mTextureHeight;	
		
		//Dimension Unpadded d'une image
		GLuint mImageWidth;
		GLuint mImageHeight;
};

#endif
