/*
Boris merminod le 11/07/2018

Le fichier LUtil.cpp va commencer par la déclaration d'un objet de la classe LTexture gCircleTexture. 

Dans la fonction loadMedia() on va charger la texture, puis on va la verrouiller via sa méthode lock() pour accéder à ses pixels. Notre cercle possède un background de couleur cyan soit (r000g255b255a255). On va commencer par rendre ce background noir. Pour cela on va parcourir les pixels et chacun possédant le code ci-dessus (cyan) va être changé en noir.
Ensuite on va parcourir chaque pixel de nouveau pour changer en noir uniquement certains d'entre eux pour former des diagonales noires. A la suite de ces modifications, on va déverrouiller la texture pour la mettre à jour.

Enfin on effectue le rendu de la texture via la fonction render()

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/09_updating_textures/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Image circulaire
LTexture gCircleTexture;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gCircleTexture.loadTextureFromFile("sprite/circle.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}

	//Verrouillage de la texture pour les modifications
	gCircleTexture.lock();

	//Calcul de la target color
	GLuint targetColor;
	GLubyte* colors = (GLubyte*)&targetColor;
	colors[0] = 000;
	colors[1] = 255;
	colors[2] = 255;
	colors[3] = 255;

	//Remplacer la target color par une transparence noire
	GLuint * pixels = gCircleTexture.getPixelData32();
	GLuint pixelCount = gCircleTexture.textureWidth() * gCircleTexture.textureHeight();
	for(unsigned int i=0; i<pixelCount; i++)
		if(pixels[i] == targetColor)
			pixels[i] = 0;
	
	//Lignes diagonales
	for(unsigned int y = 0; y<gCircleTexture.imageHeight(); y++)
		for(unsigned int x = 0; x < gCircleTexture.imageWidth(); x++)
			if(y % 10 != x % 10)
				gCircleTexture.setPixel32(x, y, 0);
	
	//Mise à jour de la texture
	gCircleTexture.unlock();
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des textures
	gCircleTexture.render((SCREEN_WIDTH - gCircleTexture.imageWidth())/2.f, (SCREEN_HEIGHT - gCircleTexture.imageHeight())/2.f );
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
