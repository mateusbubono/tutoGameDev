/*
Boris merminod le 

	Au niveau des variables globales, on utilise ici gColorMode qui contrôle quel type de rectangle va être rendu, ici il s'agit par défaut de la couleur cyan. La variable gProjectionScale contrôle la largeur de la zone de coordonnée que dont on souhaite effectuer le rendu (Cela permet de gérer des zooms).
	
	Dans la fonction initGL on utilise une nouvelle fonction glOrtho(). Cestt fonction va multiplier la matrice courante avec une matrice à perspective orthographique (ou 2D) avec en argument le bord gauche, droit bas, haut, proche et loin en argument. Le résultat est que la fenêtre possède désormais un système de coordonnées qui ressemble à celui de la SDL avec x = 0 représentant le bord gauche, x = SCREEN_WIDTH représentant le bord droit, y = 0 représentant le bord supérieur et y = SCREEN_HEIGHT représentant le bord inférieur. 
	Le rôle de la matrice de projection est de contrôler comment on voit notre géométrie. Si je voulais de la 3D il suffirait de multiplier la matrice de projection avec une matrice de perspective ce qui peut être réalisé par gluPerspective ou glFrustum.
	
	La fonction render commence par faire un clear de l'écran avant d'initialiser le mode courant de la matrice en modelview. Cette étape est nécessaire du fait que la fonction handleKeys modifie la matrice courante en matrice de projection il est donc nécessaire d'avoir une initialisation suivante de la matrice en modelview pour que les opérations matricielles restent correctes. La matrice modelview permet de réaliser des transformations géométriques tel que des translations et des rotations. Pour résumer la matrice de projection contrôle comment on voit la représentation géométrique, tandis que la matrice modelview de transformation contrôle comment la représentation est disposé dans le rendering world.
	De part la manière dont on a initialisé la matrice de projection, l'origine de la scène se trouve au coin supérieur gauche de la scène. Comme on souhaite représenter notre rectangle au centre de celle ci, on va déplacer l'origine de la scène via la fonction glTranslate(). Cette fonction va multiplier une matrice de translation avec la matrice courante (la matrice modelview). Ainsi chaque rendu géométrique va subir cette translation géométrique. 
	A chaque frame on va recharger la matrice d'identité modelview avant d'effectuer le rendu. Si ce n'est pas fait les transformation de translation vont s'accumuler, ce qui va avoir tendance d'augmenter le décalage à chaque frame et de ne pas effectuer le rendu comme on le souhaite. Ceci est le rôle pour rappelle de la fonction glLoadIdentity().
	Il reste à la fonction d'effectuer le rendu du rectangle. Si le mode de couleur est cyan alors on commence notre glBegin(), on indique la couleur grâce à glColor et on place ensuite chaque sommet grâce à glVertex() puis on termine le tout par glEnd(). Notre méthode de projection nous permet de représenter le rectangle en utilisant des valeurs de coordonnées plus habituelle ce qui nous donne un carré de longueur et de largeur à pixels.
	Si le mode de couleur est multicolor, on va appeler glColor avant chaque appel de placement d'un sommet par glVertex. Celà permet de moduler la couleur aux quatre coin du carré.
	Pour effectuer le rendu d'un polygone en pixels, openGL procède en plusieurs étapes : pour commencer une matrice modelview est appliquée ce qui permet d'effectuer les opération de translation, rotation, changement d'échelle et de "skew", ensuite la matrice de projection va prendre l'ensemble des sommets de notre polygone et les multiplier pour les transformé en un coordonnées à perspective normalisé, ensuite les sommets sont connectés, puis les pixels à l'intérieur sont rempli dans une phase qui en anglais est appelée "rasterization".
		A la fin de toutes les opérations de rendu  on va afficher via glutSwapBuffer le résultat de notre rendu à l'utilisateur.

	La fonction handleKeys va contrôler les actions sur le clavier. Si l'utilisateur appuie sur q cela va permettre de switcher de mode de couleurs vers cyan ou multicolor. Si l'utilisateur appuie sur e on va changer l'échelle de représentation du cube le défaut est à 100%. Le deuxième mode est 200% le tout apparaît alors plus petit. Le troisième mode est 50% le tout apparaît alors plus grand. Lorsque le changement d'échelle est sélectionné la matrice de projection est multiplié avec la matrice de perspective à échelle ortographique.  
 
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/02_matrices_and_coloring_polygons/index.php
*/

#include "LUtil.h"

//La couleur actuellement utilisée pour le rendering mode
int gColorMode = COLOR_MODE_CYAN;

//L'échelle de projection
GLfloat gProjectionScale = 1.f;

bool initGL()
{
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Reset la matrice modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Bouger au centre de l'écran
	glTranslatef(SCREEN_WIDTH/2.f, SCREEN_HEIGHT / 2.f, 0.f);
	
	//Rendu d'un rectangle
	if(gColorMode == COLOR_MODE_CYAN)
	{
		glBegin(GL_QUADS);
			glColor3f(0.f, 1.f, 1.f);
			glVertex2f(-50.f, -50.f);
			glVertex2f(50.f, -50.f);
			glVertex2f(50.f, 50.f);
			glVertex2f(-50.f, 50.f);
		glEnd();
	}
	else
	{
		glBegin(GL_QUADS);
			glColor3f(1.f, 0.f, 0.f); glVertex2f(-50.f, -50.f);
			glColor3f(1.f, 1.f, 0.f); glVertex2f(50.f, -50.f);
			glColor3f(0.f, 1.f, 0.f); glVertex2f(50.f, 50.f);
			glColor3f(0.f, 0.f, 1.f); glVertex2f(-50.f, 50.f);
		glEnd();
	}
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Si on appuie sur q
	if(key == 'q')
	{
		//Toggle color mode
		if(gColorMode == COLOR_MODE_CYAN)
			gColorMode = COLOR_MODE_MULTI;
		else
			gColorMode = COLOR_MODE_CYAN;
	}
	else if(key == 'e')
	{
		//Cycle d'échelle de projection
		if(gProjectionScale == 1.f) //Zoom out
			gProjectionScale = 2.f;
		else if(gProjectionScale == 2.f) //Zoom in
			gProjectionScale = 0.5f;
		else if (gProjectionScale == 0.5f) //Zoom normal
			gProjectionScale = 1.f;
			
		//Mise à jour de la matrice de projection
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, SCREEN_WIDTH * gProjectionScale, SCREEN_HEIGHT * gProjectionScale, 0.0, 1.0, -1.0);
	}	
}
