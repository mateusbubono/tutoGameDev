/*
	Boris Merminod le
	
	On va déclarer ici les constantes ainsi que quelques prototypes de fonctions.
	On ajoute ici deux constantes :
		//Modes de couleurs
		const int COLOR_MODE_CYAN =0;
		const int COLOR_MODE_MULTI = 1;
Cela va nous permettre d'effectuer le rendu de deux type de rectangles différents, un rectangle cyan et un rectangle multicolor. On ajoute aussi un gestionnaire de clavier handleKeys. Avec la SDL les événements sont gérés via des queues d'événements. Avec GLUT les événements sont géré par des callbacks. On indiquera dans le main que handleKeys est le gestionnaire du clavier.
	
	
	code source tiré de : http://lazyfoo.net/tutorials/OpenGL/02_matrices_and_coloring_polygons/index.php
*/

#ifndef LUTIL_H
#define LUTIL_H

#include "LOpenGL.h"
#include <stdio.h>

//Les constantes d'affichage
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

//Modes de couleurs
const int COLOR_MODE_CYAN =0;
const int COLOR_MODE_MULTI = 1;

bool initGL();
/*
Pre Condition:
 -Un contexte OpenGL valide
Post Condition:
 - Initialisation des matrice et des couleurs 
 - Affichage des erreurs OpenGL à la console
 - Retourne false en cas d'erreur OpenGL
Side Effects:
 - La matrice de projection est initialisée en matrice d'identité
 - La matrice Modelview est initialisée en matrice d'identité
 - Le mode matrice est initialisée en modelview
 - La couleur est initialisée en noir 
*/

void update();
/*
Pre Condition:
 -None
Post Condition:
 -Does per frame logic
Side Effects:
 -None
*/

void render();
/*
Pre Condition:
 - Un contexte OpenGL valide
 - Une matrice modelview active
Post Condition:
 - Rendu de la scène
Side Effects:
 -Vide le buffer de couleur
 -Swaps le front/back buffer
*/

void handleKeys(unsigned char key, int x, int y);
/*
Pré conditions :
 -None
Post conditions :
 - "Toggles" le mode couleur lorsque l'utilisateur appuie sur q
 - Le Cycle passe par différentes échelles de projection quand l'utilisateur appuie sur e
 - Si l'utilisateur appuie sur e, le matrix mode est initialisé en projection
*/

#endif
