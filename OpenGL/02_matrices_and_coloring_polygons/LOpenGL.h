/*
	Boris Merminod le 06/03/2018
	
	Ce fichier contient l'ensemble des bibliothèque à inclure pour le fonctionnement de OpenGL
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/02_matrices_and_coloring_polygons/index.php
*/

#ifndef LOPENGL_H
#define LOPENGL_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

#endif
