/*
	Boris Merminod le 09/07/2018
	
	Dans le fichier LFRect.h, nous allons définir une structure de données caractérisant un rectangle (Un peu comme le SDL_Rect). Cette structure permettra de désigner la zone partielle de la texture dont on va effectuer le rendu. 

	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/07_clipping_textures/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
