/*
	Boris Merminod le 09/07/2018

	Notre fonction render est ici modifiée pour prendre en compte notre clip. Elle va commencer par définir des points de coordonnées GLfloat par défaut pour la position de notre texture. Les dimensions de la texture sont aussi déterminée par défaut à la taille de l'image de cette texture via deux structure GLfloat. On dispose donc de variables par défaut permettant de calculer les coordonnées des sommets de la texture. Si la fonction utilise un clip rectangulaire, nous devons ajuster les coordonnées de la texture et des sommets en conséquence. Pour les sommets c'est plus simple il suffit d'utiliser les dimensions du clip.
	Les coordonnées de textures sont gérée selon une valeur comprise entre 0 et 1. Ainsi si je veux obtenir la position en x de la flèche inférieure droite de la texture, il me faut donc la position du coin supérieur gauche de cette flèche pour mon clip. En coordonnées pixel cela correspondrait sur l'image d'origine à 128 sur 256 soit 0.5. Donc si je veux convertir ma coordonnées dans un système entre 0 et 1 il me suffit de diviser sa position x en pixel par sa dimension en largeur pour obtenir la valeur dont j'ai besoin.
	Ainsi avec les coordonnées de texture et les dimensions calculée du sprite nous n'avons plus qu'à effectuer le rendu de notre quad

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/07_clipping_textures/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), (GLuint)ilGetInteger(IL_IMAGE_WIDTH), (GLuint)ilGetInteger(IL_IMAGE_HEIGHT));
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mTextureWidth = width;
	mTextureHeight = height;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}
	
	mTextureWidth = 0;
	mTextureHeight = 0;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = 1.f;
		GLfloat texLeft = 0.f;
		GLfloat texRight = 1.f;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mTextureWidth;
		GLfloat quadHeight = mTextureHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(0.f,0.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth,0.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth,quadHeight);
			glTexCoord2f(texLeft,texBottom); glVertex2f(0.f,quadHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}
