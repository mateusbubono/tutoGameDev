/*
Boris merminod le 09/07/2018

	Nous utilisons ici deux variables globales. gArrowTexture contient notre texture en tant que telle. gArrowClips est une table qui contient les clips qui vont être appliqué à la texture.
	
	Dans la fonction loadMedia(), les dimensions des clips sont déterminés et la texture à utilisée est chargée.
		
	Finalement dans la fonction render() on va effectuer le rendu de chaque sprite de flèche dans chaque coin de l'écran 

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/07_clipping_textures/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Sprite texture
LTexture gArrowTexture;

//Sprite area
LFRect gArrowClips[4];


bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	
	//Mise en place des clips de la texture
	gArrowClips[0].x = 0.f;
	gArrowClips[0].y = 0.f;
	gArrowClips[0].w = 128.f;
	gArrowClips[0].h = 128.f;
	
	gArrowClips[1].x = 128.f;
	gArrowClips[1].y = 0.f;
	gArrowClips[1].w = 128.f;
	gArrowClips[1].h = 128.f;
	
	gArrowClips[2].x = 0.f;
	gArrowClips[2].y = 128.f;
	gArrowClips[2].w = 128.f;
	gArrowClips[2].h = 128.f;
	
	gArrowClips[3].x = 128.f;
	gArrowClips[3].y = 128.f;
	gArrowClips[3].w = 128.f;
	gArrowClips[3].h = 128.f;
		
	
	//Chargement de la texture
	if(!gArrowTexture.loadTextureFromFile("sprite/arrows.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des flèches
	gArrowTexture.render(0.f, 0.f, &gArrowClips[0]);
	gArrowTexture.render(SCREEN_WIDTH - gArrowClips[1].w, 0.f, &gArrowClips[1]);
	gArrowTexture.render(0.f, SCREEN_HEIGHT - gArrowClips[2].h, &gArrowClips[2]);
	gArrowTexture.render(SCREEN_WIDTH - gArrowClips[3].w, SCREEN_HEIGHT - gArrowClips[3].h, &gArrowClips[3]);
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
