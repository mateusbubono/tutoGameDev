Boris Merminod le 09/07/2018

Introduction au clipping avec OpenGL

LFRect.h : 
	
	Dans le fichier LFRect.h, nous allons définir une structure de données caractérisant un rectangle (Un peu comme le SDL_Rect). Cette structure permettra de désigner la zone partielle de la texture dont on va effectuer le rendu. 
	
For example, this sample code would actually output "sum is not equal to 1". The variable "sum" will actually equal 0.9999something. If you need consistent integer representation, you may need a integer rectangle. 

LTexture.h :

	
	Dans le fichier LTexture.h, on va effectuer une modification de la fonction render pour qu'elle prennent en compte la zone rectangulaire pour effectuer le rendu partiel de la texture. Le paramètre de la fonction est à NULL par défaut au cas ou l'on souhaiterais faire le rendu de la texture entière.

LTexture.cpp :

	Notre fonction render est ici modifiée pour prendre en compte notre clip. Elle va commencer par définir des points de coordonnées GLfloat par défaut pour la position de notre texture. Les dimensions de la texture sont aussi déterminée par défaut à la taille de l'image de cette texture via deux structure GLfloat. On dispose donc de variables par défaut permettant de calculer les coordonnées des sommets de la texture. Si la fonction utilise un clip rectangulaire, nous devons ajuster les coordonnées de la texture et des sommets en conséquence. Pour les sommets c'est plus simple il suffit d'utiliser les dimensions du clip.
	Les coordonnées de textures sont gérée selon une valeur comprise entre 0 et 1. Ainsi si je veux obtenir la position en x de la flèche inférieure droite de la texture, il me faut donc la position du coin supérieur gauche de cette flèche pour mon clip. En coordonnées pixel cela correspondrait sur l'image d'origine à 128 sur 256 soit 0.5. Donc si je veux convertir ma coordonnées dans un système entre 0 et 1 il me suffit de diviser sa position x en pixel par sa dimension en largeur pour obtenir la valeur dont j'ai besoin.
	Ainsi avec les coordonnées de texture et les dimensions calculée du sprite nous n'avons plus qu'à effectuer le rendu de notre quad

LUtil.cpp :

	Nous utilisons ici deux variables globales. gArrowTexture contient notre texture en tant que telle. gArrowClips est une table qui contient les clips qui vont être appliqué à la texture.
	
	Dans la fonction loadMedia(), les dimensions des clips sont déterminés et la texture à utilisée est chargée.
		
	Finalement dans la fonction render() on va effectuer le rendu de chaque sprite de flèche dans chaque coin de l'écran

