/*
	Boris Merminod le 11/01/2019
	
	On ajoute ici l'importation de la bibliothèque glew
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/15_extensions_and_glew/index.php
*/

#ifndef LOPENGL_H
#define LOPENGL_H

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

#endif
