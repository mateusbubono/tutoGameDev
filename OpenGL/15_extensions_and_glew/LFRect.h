/*
	Boris Merminod le  11/01/2019
	
	Code source tiré de :http://lazyfoo.net/tutorials/OpenGL/15_extensions_and_glew/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
