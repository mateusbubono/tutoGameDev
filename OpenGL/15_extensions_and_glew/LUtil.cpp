/*
Boris merminod le 11/01/2019

	Ici dans notre fonction initGL, on va rajouter l'initialisation de GLEW. C'est le rôle de glewInit(). L'énumération GLenum glewError permet de récupérer le retour de glewInit() et d'identifier une erreur en cas d'échec. La constante GLEW_VERSION_2_1 permet de vérifier qu'on travaille bien avec une version de opengl d'une version supérieure ou égale à là 2.1. Le reste de l'initialisation reste la même.
	Les fonctions loadMedia, render et update restent strictement les mêmes que lors du chapitre précédent.
	On va légérement changer la fonction handleKeys pour utiliser de nouveaux types de wraps apporté par la bibliothèque Glew : 
	- GL_CLAMP_TO_BORDER : va clamper les bords de la textures ce qui va stopper le mapping entre 0 et 1.0
	- GL_CLAMP_TO_EDGE : va clamper la texture à 0 ou 1.0 puis va ensuite utiliser les valeurs des texels du bords et va les stretcher au bord du polygon.
	- GL_MIRRORED_REPEAT : va faire se répeter la texture au delà de 0.0 et 1.0, en réalisant à chaque repeat un mirror de la texture.
	
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/15_extensions_and_glew/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//L'image qui va être répetée
LTexture gRepeatingTexture;

//Texture offset
GLfloat gTexX = 0.f, gTexY = 0.f;

//Texture wrap type
int gTextureWrapType = 0;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gRepeatingTexture.loadTextureFromFile("sprite/texture2.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

	//Déroule la texture
	gTexX++;
	gTexY++;
	
	//Cap Scrolling
	if(gTexX >= gRepeatingTexture.textureWidth())
		gTexX = 0.f;
	
	if(gTexY >= gRepeatingTexture.textureHeight())
		gTexY = 0.f;
}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Calculate texture maxima
	GLfloat textureRight = (GLfloat)SCREEN_WIDTH / (GLfloat) gRepeatingTexture.textureWidth();
	GLfloat textureBottom = (GLfloat)SCREEN_HEIGHT / (GLfloat)gRepeatingTexture.textureHeight();
	
	//Utiliser la répetition de la texture
	glBindTexture(GL_TEXTURE_2D, gRepeatingTexture.getTextureID());
	
	glMatrixMode(GL_TEXTURE);
	
	//Reset des transformations
	glLoadIdentity();
	
	glTranslatef(gTexX / gRepeatingTexture.textureWidth(), gTexY / gRepeatingTexture.textureHeight(), 0.f);
	
	//Rendu
	glBegin(GL_QUADS);
		glTexCoord2f(0.f, 0.f); glVertex2f(0.f, 0.f);
		glTexCoord2f(textureRight, 0.f); glVertex2f(SCREEN_WIDTH, 0.f);
		glTexCoord2f(textureRight, textureBottom); glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);
		glTexCoord2f(0.f, textureBottom); glVertex2f(0.f, SCREEN_HEIGHT);
	glEnd();
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Si q est pressé
	if(key == 'q')
	{
		//Cycle de répeition de textures
		gTextureWrapType++;
		if(gTextureWrapType >= 5)
			gTextureWrapType = 0;
		
		//Set la répetition de textures
		glBindTexture(GL_TEXTURE_2D, gRepeatingTexture.getTextureID());
		switch(gTextureWrapType)
		{
			case 0 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				printf("%d : GL_REPEAT\n", gTextureWrapType);
				break;
			case 1 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				printf("%d : GL_CLAMP\n", gTextureWrapType);
				break;
			case 2 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				printf("%d : GL_CLAMP_TO_BORDER\n", gTextureWrapType);
				break;
			case 3 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				printf("%d : GL_CLAMP_TO_EDGE\n", gTextureWrapType);
				break;
			case 4 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
				printf("%d : GL_MIRRORED_REPEAT\n", gTextureWrapType);
				break;
		}
	}
}


