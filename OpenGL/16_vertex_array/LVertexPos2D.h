/*
Boris Merminod le 14/01/2019

	On va commencer par créer un nouveau header contenant une structure pour décrire une position d'un sommet en 2D, c'est le rôle de LVertexPos2D. Avec ça il devient plus facile de définir une géométrie

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/16_vertex_arrays/index.php
*/

#ifndef LVERTEX_POS_2D_H
#define LVERTEX_POS_2D_H

#include "LOpenGL.h"

struct LVertexPos2D
{
	GLfloat x;
	GLfloat y;
};

#endif
