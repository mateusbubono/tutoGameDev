/*
Boris merminod le 14/01/2019
	
	On commence par déclarer une structure de données LVertexPos2D en variable globale sous la forme d'un tableau de 4 éléments (on représente un carré).
	
	Dans la fonction loadMedia(), on ne charge pas de fichier, mais on va définir les points de coordonnées pour notre carré
	
	Dans la fonction render(), on commence par faire un glClear() de l'écran, comme d'habitude. Puis on va appeler glEnablaClientState(GL_VERTEX_ARRAY) afin d'utiliser notre Vertex Array pour dessiner notre carré.
	Pour définir les données des sommets de notre Vertex Array, on a besoin d'utiliser la fonction : 
	glVertexPointer(2, GL_FLOAT, 0, gQuadVertices);
	
Dont les arguments sont :
- 2 : Le nombre de coordonnées pour définir la position d'un sommet (en l'occurrence ici on bosse en 2D)
- GL_FLOAT : le type de données utilisées pour représenter une coordonnées (ici le GLfloat).
- 0 : Le nombre d'octets de décalage entre chaque sommets (on verra ça dans les chapitres qui suivent)
- Un pointeur sur notre tableau de GLfloat. Bienque notre pointeur soit du type LVertexPos2D, on peut le traiter comme une table de GLfloat.

	Petite explication sur le dernier point :
	
	LVertexPos2D position; 
	GLfloat* components = (GLfloat*)&position; 
	components[ 0 ];//First member of the struct (x) 
	components[ 1 ];//Second member of the struct (y)

La structure de données peut grouper des données de types différents. Cependant si les données regroupent des types similaires, on peut les traiter comme un tableau de ces éléments. C'est le cas de l'exemple ci-dessus, je cast ma structure LVertexPos2D en une table de deux GLfloat. l'élément 0 sera ma coordonnée en x et l'élément 1 ma coordonnée en y.
	Il y a une exceptions dans le traitement d'une structure en une table de float. Pour réaliser la conversion, certaines architecture de compilateur vont étendre chaque élément de la structure pour pouvoir les traiter plus efficacement (c'est rare mais ça existe). Dans ce cas chaque élément de la table va recevoir un surplus d'octets dont il faut tenir compte au risque d'avoir des anomalies à l'utilisation de la table converties. Dans ces cas la il faut ajouter ce surplus à la taille de la structure LVertexPos2D via un sizeof.
	
	//Draw quad using vertex data 
	glDrawArrays( GL_QUADS, 0, 4 );
	
	Ensuite on va dessiner notre carré via glDrawArrays(). Cette fonction prend trois argument :	
- la figure géométrique que l'on va dessiner
- l'index de la table par lequel on va commencer (le point de départ)
- le nombre de sommets que nous allons tracer

	On termine en desactivant l'utilisation du Vertex array via glDisableClientState(GL_VERTEX_ARRAY).
	
	Avec ce système, on dispose d'une manière différente d'envoyer les données concernant les sommets. Les transformations de matrices fonctionnent en revanche de la même façon. Il suffit de les réaliser avant de dessiner notre carré comme d'haibitude.
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/16_vertex_arrays/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LVertexPos2D.h"

//Les sommets d'un Quad
LVertexPos2D gQuadVertices[4];

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Mise en place des sommet du carré
	gQuadVertices[0].x = SCREEN_WIDTH * 1.f/ 4.f;
	gQuadVertices[0].y = SCREEN_HEIGHT * 1.f/ 4.f;
	
	gQuadVertices[1].x = SCREEN_WIDTH * 3.f/ 4.f;
	gQuadVertices[1].y = SCREEN_HEIGHT * 1.f/ 4.f;
	
	gQuadVertices[2].x = SCREEN_WIDTH * 3.f/ 4.f;
	gQuadVertices[2].y = SCREEN_HEIGHT * 3.f/ 4.f;
	
	gQuadVertices[3].x = SCREEN_WIDTH * 1.f/ 4.f;
	gQuadVertices[3].y = SCREEN_HEIGHT * 3.f/ 4.f;
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Activer le vertex array
	glEnableClientState(GL_VERTEX_ARRAY);
	
		//Set vertex data
		glVertexPointer(2, GL_FLOAT, 0, gQuadVertices);
		
		//Dessiner le carré en utilisé les données du vertex data
		glDrawArrays(GL_QUADS, 0, 4);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

