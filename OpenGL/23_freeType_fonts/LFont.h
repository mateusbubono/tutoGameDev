/*
Boris Merminod le 03/09/2019

	La bibliothèque FreeType utilise un mode
d'inclusion un peu différent que le mode habituel. On commence par inclure le header ft2build.h qui contient un ensemble de macros, ensuite on va inclure chaque macros en fonction de nos besoins. On commence donc par inclure la librairie FreeType de base.

	On va ensuite ajouter la capacité à notre
classe LFont de charger des FreeType font via l'utilisation statique initFreeType dont l'objectif est de l'initialisé. 
	La fonction loadFreeType va permettre par la 
suite de charger un FreeType font particulier. La seule modification de la classe est de lui rajouter la possibilité de gérer les pixels de caractères à partir de fichier TTF à la place de fichiers bitmap font. Ainsi la représentation de l'ensemble des caractères se fera dans un fichier .ttf.

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/23_freetype_fonts/index.php
*/

#ifndef LFONT_H
#define LFONT_H

#include "LSpriteSheet.h"
#include <ft2build.h>
//#include <ft2build.h>
#include FT_FREETYPE_H

class LFont: private LSpriteSheet
{
	public :

		static  bool initFreeType();
		/*
		Pre Condition :
		-None
		Post Condition:
		-Création d'une instance de la librairie Free Type utilisable pour tous les objets de la classe LFont
		Side Effects:
		-None
		*/

		LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Initialisation de spacing variables
		Side Effects :
		- None
		*/
		
		~LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer des Fonts
		Side Effects :
		- None
		*/
		
		bool loadBitmap(std::string path);
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Chargement de fonts bitmap
		- Retourne true s'il n'y a pas d'erreurs
		- Reporting des erreurs sur la console
		Side Effects :
		- None
		*/

		bool loadFreeType(std::string path, GLuint pixelSize);
		/*
		Pre condition :
		-None
		Post Condition :
		- Création d'une texture interne de 8 bit avec une font file donné
		- Report des erreurs freetype si elle survienne
		Side Effects :
		- None
		*/
		
		void freeFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer les textures et les sprites sheet du bitmap font
		Side Effects :
		- None
		*/
				
		void renderText(GLfloat x, GLfloat y, std::string text);
		/*
		Pré-conditions :
		- un font chargé
		Post conditions :
		- Rendu de textes
		Side Effects :
		- Binds la texture membre ainsi que les data buffers
		*/
		
	private :

		//Font de la librairie TTF
		static FT_Library mLibrary;
		//Spacing variables
		GLfloat mSpace;
		GLfloat mLineHeight;
		GLfloat mNewLine;

};

#endif
