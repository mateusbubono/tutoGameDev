/*
	Boris Merminod le 03/09/2019

	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/23_freetype_fonts/index.php
*/

#ifndef LTEXTURE_H
#define LTEXTURE_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string.h>
#include "LFRect.h"

class LTexture
{
	public :
		LTexture();
		/*
		Pré-conditions :
		-None
		Post-conditions :
		- Initialise les attributs membres
		Side effects :
		- None
		*/
		
		virtual ~LTexture();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Libère la mémoire allouée pour gérer un objet de texture
		Side effects:
		- None
		*/
		
		bool loadTextureFromFile32(std::string path);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Librairie DevIL initialisée
		Post-conditions :
		- Création d'une texture RGBA à partir d'un fichier
		- Pads de l'image pour l'avoir en une dimension en puissance de deux
		- Report des erreur à la sortie stderr
		Side Effects :
		- Association d'une texture NULL 
		*/

		bool loadPixelsFromFile32(std::string path);
		/*
		Pré-conditions :
		- Bibliothèque DevIL initialisée
		Post-conditions :
		- Attribution à l'attribut pixel (à 32 bits) de données chargées à partir d'un fichier
		- Redimensionnement de l'image pour obtenir une dimension en puissance de deux
		- Report des erreurs à la console en cas d'erreur
		Side Effects :
		- None
		*/
		
		void createPixels32(GLuint imgWidth, GLuint imgHeight);
		/*
		Pré-condition :
		- Un contexte OpenGL valide
		Post conditions :
		- Libère la mémoire allouée pour gérer des données de textures existante au préalable
		- Allocation de données de pixel à 32 bit associé à l'attribut mPixels32
		Side Effects :
		- None
		*/
		
		void copyPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post conditions :
		- Copie les données de pixels vers l'attribut membre mPixels32
		Side Effects :
		- None
		*/
		
		void padPixels32();
		/*
		Pré-condition :
		- L'attribut mPixels à 32 bit est valide
		Post conditions :
		- Extension de l'attribut mPixels32 vers une dimension en puissance de deux.
		Side Effects :
		- None
		*/

		bool loadTextureFromFileWithColorKey32(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a = 000);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- La bibliothèque DevIL initialisée
		Post-conditions :
		- Création d'une texture RGBA depuis un fichier donné
		- Redimensionnement de l'image pour avoir une dimension en puissance de deux
		- Sets given RGBA value to RFFGFFBFFA00
		- Si a = 0, alors seul la composante RGB est comparée
		- Report des erreurs à la console, si la texture n'a pas pu être chargée
		Side Effects :
		- Attache une texture NULL
		*/

		bool loadTextureFromPixels32();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide 
		- L'attribut pixel valide
		Post-conditions :
		- Création d'une texture depuis l'attribut pixel (à 32 bits)
		- Suppression de l'attribut pixel en cas de succès
		- Report des erreur à la console si la texture n'a pas pu être chargée
		Side Effets :
		- Attache une texture NULL
		*/
		
		bool loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Création d'une texture à partir de pixels donnés
		- Reporte les erreurs à la console si la texture n'a pas pu être créée
		Side Effects: 
		- Attache une texture NULL
		*/
		
		bool loadPixelsFromFile8(std::string path);
		/*
		Pré-conditions :
		- DevIL initialisé
		Post-conditions :
		- Attributs de pixels à 8 bit chargé à partir d'un fichier donné
		- Pads de l'image pour avoir une image en puissance de deux
		- Reporting des erreurs à la console si les pixels n'ont pas pu être chargé
		Side Effects :
		- None
		*/
		
		bool loadTextureFromPixels8();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Un attribut membre pixels valide
		Post-conditions :
		- Création d'une alpha texture à partir de l'attribut membre à 8bits pixels
		- Suppression du membre pixels en cas de succès
		- Reporting des erreurs à la console si la texture n'a pas pu être crée
		Side Effects :
		- Binds une texture NULL
		*/
		
		void createPixels8(GLuint imgWidth, GLuint imgHeight);
		/*
		Pré-condition :
		- Un contexte OpenGL valide
		Post conditions :
		- Libère la mémoire allouée pour gérer des données de textures existante au préalable
		- Allocation de données de pixel à 8 bits associé à l'attribut mPixels8
		Side Effects :
		- None
		*/
		
		void copyPixels8(GLubyte* pixels, GLuint imgWidth, GLuint imgHeight);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post conditions :
		- Copie les données de pixels vers l'attribut membre mPixels8
		Side Effects :
		- None
		*/
		
		void padPixels8();
		/*
		Pré-condition :
		- L'attribut mPixels à 8 bits est valide
		Post conditions :
		- Extension de l'attribut mPixels8 vers une dimension en puissance de deux.
		Side Effects :
		- None
		*/
		
		virtual void freeTexture();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Suppression des texutres si elles existes
		- Mise en place d'un texture ID à 0
		Side Effects :
		- None
		*/

		bool lock();
		/*
		Pré-conditions :
		- Une texture existante et non verrouillée
		Post-conditions :
		- Mise en place de l'attribut pixel à partir des données de textures
		- Retourne true si les pixels de la texture ont été retrouvé
		Side Effects :
		- Attache une texture NULL
		*/

		bool unlock();
		/*
		Pré-conditions :
		- Une texture verrouillée
		Post-conditions :
		- Mise à jour de la texture à partir de ses pixels
		- Retourne true si les pixels de la texture ont été mis à jour
		Side Effects :
		- Attache une texture NULL
		*/

		GLuint * getPixelData32();
		/*
		Pré-conditions :
		- Attribut d'un ensemble de pixels disponibles
		Post-conditions :
		- Retourne l'attribut membre pixels
		Side Effects :
		- None
		*/
		
		GLubyte* getPixelData8();
		/*
		Pré-conditions : 
		- Attribut membre pixels à 8 bits valide
		Post-conditions :
		- Retourne l'attribut membre pixels à 8 bits
		Side Effects :
		- None
		*/

		GLuint getPixel32(GLuint x, GLuint y);
		/*
		Pré-conditions :
		- Attribut membre pixels valide et disponible
		Post-conditions :
		- Retourne le pixel d'une position donnée
		- La fonction devrait segfault si la texture n'est pas verrouillée au préalable
		Side Effects :
		- None
		*/

		void setPixel32(GLuint x, GLuint y, GLuint pixel);
		/*
		Pré-conditions :
		- Un attribut membre pixels valide et disponible
		Post-conditions :
		- Modifier un pixel à une position donnée
		- La fonction peut Segfault si la texture n'est pas verrouillée au préalable
		Side Effects :
		- None
		*/
		
		GLubyte getPixel8(GLuint x, GLuint y);
		/*
		Pré-conditions :
		- Membre pixels à 8 bits valide
		Post-conditions :
		- Retourne un élément du membre pixel à une position donnée
		- La fonction segfault si la texture n'a pas été lock
		Side Effects :
		- None
		*/
		
		void setPixel8(GLuint x, GLuint y, GLubyte pixel);
		/*
		Pré-condition :
		- Attribut membre pixel à 8 bit valide
		Post-conditions :
		- Sets l'élément pixel de l'attribut membre pixel à 8 bit à une position donnée
		- La fonction segfault si la texture  n'est pas locked
		Side Effects :
		- None
		*/
		
		void render(GLfloat x, GLfloat y, LFRect* clip = NULL);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Une matrice modelview active
		Post-conditions :
		- opération de translation à une position données puis rendu de carré texturés
		- Si le paramètre clip est NULL alors la totalité de la texture est rendue
		- Si une zone de stretch est donnée, alors la texture est redimensionnée à l'échelle de la zone de stretch définie
		Side  effects :
		- Associe à la texture membre une ID
		*/
		
		void blitPixels32(GLuint x, GLuint y, LTexture & destination);
		/*
		Pré-conditions :
		- Attribut membre mPixels32 valide et un ensemble de pixels de destination
		- Coordonnées de Blitting valides
		Post conditions :
		- Copie des pixels de l'attribut membre mPixels32 vers un ensemble de pixels de destination (normalement un autre attribut membre) à une position donnée
		- La fonction ne fonctionnera pas si des coordonnées invalides sont utilisée
		Side Effects :
		- None
		*/
		
		void blitPixels8(GLuint x, GLuint y, LTexture & destination);
		/*
		Pré-conditions :
		- Attribut membre mPixels8 valide et un ensemble de pixels de destination
		- Coordonnées de Blitting valides
		Post conditions :
		- Copie des pixels de l'attribut membre mPixels32 vers un ensemble de pixels de destination (normalement un autre attribut membre) à une position donnée
		- La fonction ne fonctionnera pas si des coordonnées invalides sont utilisée
		Side Effects :
		- None
		*/
		
		GLuint getTextureID();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne le nom et l'ID de la texture
		Side effects :
		- None
		*/
		
		GLuint textureWidth();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- retourne la largeur (width) de la texture
		Side effects :
		- None
		*/
		
		GLuint textureHeight();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne la hauteur de la texture (height)
		Side effects :
		- None
		*/

		GLuint imageWidth();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image width
		Side Effects :
		- None
		*/

		GLuint imageHeight();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image height
		Side Effects :
		- None
		*/
		
	private :
		GLuint powerOfTwo(GLuint num);
		/*
		Pré-conditions :
		- None
		Post Conditions :
		- Retourne la puissance de deux entières la plus proche (et plus grande) que la valeur passée en paramètres
		Side Effects :
		- None
		*/
		
		void initVBO();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- une texture chargée
		Post-conditions :
		- Générer un VBO et un IBO à utiliser pour le rendering
		Side Effects :
		- Associe NULL au VBO et à l'IBO
		*/
		
		void freeVBO();
		/*
		Pré-condition :
		- Un VBO généré
		Post-condition :
		- Libère la mémoire allouée pour le VBO et l'IBO
		Side Effects :
		- None
		*/
	
		//Nom de la texture
		GLuint mTextureID;

		//Les pixels courant
		GLuint * mPixels32;
		GLubyte* mPixels8;
		
		//Pixels format
		GLuint mPixelFormat;
		
		//Dimensions de la texture
		GLuint mTextureWidth;
		GLuint mTextureHeight;	
		
		//Dimension Unpadded d'une image
		GLuint mImageWidth;
		GLuint mImageHeight;
		
		//VBO IDs
		GLuint mVBOID;
		GLuint mIBOID;
};

#endif
