/*
Boris Merminod le 03/09/2019

	On fait ici une petite modification de la fonction createPixels8() en initialisant la valeur de la table mPixels8 à 0 pour chacun de ses éléments. Chaque pixel alloué sera ainsi transparent par défaut

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/23_freetype_fonts/index.php
*/

//Déclenche un bug d'une fonction OpenGL dans loadTextureFromPixels8
#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LVertexData2D.h"
//#include <il.h>

GLenum DEFAULT_TEXTURE_WRAP = GL_REPEAT;

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels32 = NULL;
	mPixels8 = NULL; 
	mPixelFormat = 0;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
	
	//Initialisation du VBO
	mVBOID = 0;
	mIBOID = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
	
	freeVBO();
}

bool LTexture::loadTextureFromFile32(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
		
		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
	
		//Set du format des pixels
		mPixelFormat = GL_RGBA;
	}
	
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadPixelsFromFile32(std::string path)
{
	//Libération de la mémoire allouée éventuelle pour d'autre texture
	freeTexture();

	//Flag de chargement de texture
	bool pixelsLoaded = false;

	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());

	//Si l'image s'est chargée comme il faut 
	if(success == IL_TRUE)
	{
		//Conversion de l'image vers le RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requise de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une mauvaise taille
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Place l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);

				//Redimensionne l'image
				iluEnlargeCanvas((int) texWidth, (int) texHeight, 1);
			}

			//Allocation de mémoire pour les données de texture
			GLuint size = texWidth * texHeight;
			mPixels32 = new GLuint[size];

			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;

			//Copier les pixels en mémoire
			memcpy(mPixels32, ilGetData(), size * 4);
			pixelsLoaded = true;
		}

		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
		
		//Set du format des pixels
		mPixelFormat = GL_RGBA;
	}

	//Report des erreurs
	if(!pixelsLoaded)
		fprintf(stderr, "Impossible de charger %s\n", path.c_str());
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromFileWithColorKey32(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{
	//Charger les pixels
	if(!loadPixelsFromFile32(path))
		return false;
	
	//Parcourir les pixels
	GLuint size = mTextureWidth * mTextureHeight;
	for(unsigned int i = 0; i < size; i++)
	{
		// Récupérer la couleur du pixel
		GLubyte* colors = (GLubyte*)&mPixels32[i];

		//Si la couleur correspond
		if(colors[0] == r && colors[1] == g && colors[2] == b && (0 == a || colors[3] == a))
		{
			//Ajuster la transparence
			colors[0] = 255;
			colors[1] = 255;
			colors[2] = 255;
			colors[3] = 000;
		}
	}

	//Créer la texture
	if( !loadTextureFromPixels32())
		return false;
		
	//Set du format des pixels
	mPixelFormat = GL_RGBA;
	
	return true;
}

bool LTexture::loadTextureFromPixels32()
{
	//Flag de succès de la fonction
	bool success = true;

	//Si l'attribut pixels est chargé
	if(mTextureID == 0 && mPixels32 != NULL)
	{
		//Générer une ID de texture
		glGenTextures(1, &mTextureID);

		//Attacher la textureID
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels32);

		//Mise en place des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur de chargement de %p pixels ! %s\n", mPixels32, gluErrorString(error));
			success = false;
		}
		else 
		{
			//Suppression de l'attribut pixels
			delete [] mPixels32;
			mPixels32 = NULL;
			
			//Generate VBO
			initVBO();
			
			//Set du format des pixels
			mPixelFormat = GL_RGBA;
		}
	}
	// En cas d'erreur
	else 
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Structure mPixels non chargé, impossible de créer la texture !\n");

		//La texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La texture existe déjà \n");
		else if(mPixels32 == NULL)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La structure mPixels n'est pas initialisée\n");
		
		success = false;
	}

	return success;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	//Générer le VBO
	initVBO();
	
	//Set pixels format
	mPixelFormat = GL_RGBA;
	
	return true;
}

void LTexture::createPixels32(GLuint imgWidth, GLuint imgHeight)
{
	//Les dimensions sont valides
	if(imgWidth > 0 && imgHeight > 0)
	{
		//Suppression d'une éventuelle texture existante
		freeTexture();
		
		//Création des pixels
		GLuint size = imgWidth * imgHeight;
		mPixels32 = new GLuint[size];
		
		//Copie des données de pixels
		mImageWidth = imgWidth;
		mImageHeight = imgHeight;
		mTextureWidth = mImageWidth;
		mTextureHeight = mImageWidth;
		
		//Set du format des pixels
		mPixelFormat = GL_RGBA;
	}
}

void LTexture::copyPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight)
{
	//On vérifie que les dimensions sont valides
	if(imgWidth > 0 && imgHeight > 0)
	{
		//Suppression d'une éventuelle texture existante
		freeTexture();
		
		//Copie des pixels
		GLuint size = imgWidth * imgHeight;
		mPixels32 = new GLuint[size];
		memcpy(mPixels32, pixels, size * 4);
		
		//Copie des données des pixels
		mImageWidth = imgWidth;
		mImageHeight = imgHeight;
		mTextureWidth = mImageWidth;
		
		//WTF?
		mTextureHeight = mImageWidth;
		
		//Set du format des pixels
		mPixelFormat = GL_RGBA;
	}
}

void LTexture::padPixels32()
{
	//Existe-ils des pixels à étendre
	if(mPixels32 != NULL)
	{
		//On récupère les attributs de textures qui vont devenir obsolètes
		GLuint oTextureWidth = mTextureWidth;
		//GLuint oTextureHeight = mTextureHeight; La variable est dans le tuto ,ais n'est pas utilisée
		
		//Calcul des dimensions en puissance de deux
		mTextureWidth = powerOfTwo(mImageWidth);
		mTextureHeight = powerOfTwo(mImageHeight);
		
		//Si le bitmap a besoin de changer
		if(mTextureWidth != mImageWidth || mTextureHeight != mImageHeight)
		{
			//Allocation des pixels
			GLuint size = mTextureWidth * mTextureHeight;
			GLuint * pixels = new GLuint[size];
			
			//Copie des pixels par ligne
			for(unsigned int i = 0; i<mImageHeight; i++)
				memcpy(&pixels[i*mTextureWidth], &mPixels32[i*oTextureWidth], mImageWidth * 4);
				
			//Changement des pixels
			delete [] mPixels32;
			mPixels32 = pixels;
		}
	}
}

bool LTexture::loadPixelsFromFile8(std::string path)
{
	//Libérer la précédente texture si besoin
	freeTexture();
	
	//booléen sur la réussite du chargement de la texture
	bool pixelsLoaded = false;
	
	//Générer l'image et lui attacher un ID
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est chargée avec succès
	if(success == IL_TRUE)
	{
		success = ilConvertImage(IL_LUMINANCE, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialise les dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
			
			//Calculer les dimensions optimales de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);
			
			//Si la texture n'est pas aux bonnes dimensions
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				
				//Redimensionner l'image
				iluEnlargeCanvas((int)texWidth, (int) texHeight, 1);
			}
			
			//Allocation de mémoire pour les texture data
			GLuint size = texWidth * texHeight;
			mPixels8 = new GLubyte[size];
			
			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;
			
			memcpy(mPixels8, ilGetData(), size);
			pixelsLoaded = true;
		}
		
		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
		
		//Set du format de pixel
		mPixelFormat = GL_ALPHA;
	}
	
	//Reporting des erreurs
	if(!pixelsLoaded)
	{
		fprintf(stderr, "loadPixelsFromFile8 : Impossible de charger %s\n", path.c_str());
	}
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromPixels8()
{
	//Flag de chargement
	bool success = true;
	
	//Pixels déjà chargé
	if(mTextureID == 0 && mPixels8 != NULL)
	{
		//Générer une Texture ID
		glGenTextures(1, &mTextureID);
		
		//Attacher la texture à cette ID
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, mTextureWidth, mTextureHeight, 0, GL_ALPHA, GL_UNSIGNED_BYTE, mPixels8);
		
		//Setting des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
		
		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);
		
		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "loadTextureFromPixels8 : Erreur de chargement de texture à partir de %p pixels ! %s\n", mPixels8, gluErrorString(error));
			success = false;
		}
		else
		{
			//Libérer les pixels
			delete[] mPixels8;
			mPixels8 = NULL;
			
			//Générer le VBO
			initVBO();
			
			//Setting du format de pixel
			mPixelFormat = GL_ALPHA;
		}
	}
	else // erreur
	{
		fprintf(stderr, "loadTextureFromPixels8 : Impossible de charger la texture à partir des pixels! ");
		
		//Si la texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "Une texture est déjà chargée\n");
		else if(mPixels8 == NULL)
			fprintf(stderr, "Pas de pixels disponible pour créer une texture ! \n");
	}
	
	return success;
}

void LTexture::createPixels8(GLuint imgWidth, GLuint imgHeight)
{
	//Les dimensions doivent être valides
	if(imgWidth > 0 && imgHeight > 0)
	{
		//Suppression d'éventuel texture déjà présente
		freeTexture();
		
		//Création des pixels
		GLuint size = imgWidth * imgHeight;
		mPixels8 = new GLubyte[size];
		memset(mPixels8, 0, size);
		
		//Copie des données de pixels
		mImageWidth = imgWidth;
		mImageHeight = imgHeight;
		mTextureWidth = mImageWidth;
		
		//WTF?
		mTextureHeight = mImageWidth;
		
		//Set du format des pixels
		mPixelFormat = GL_ALPHA;
	}
}

void LTexture::copyPixels8(GLubyte* pixels, GLuint imgWidth, GLuint imgHeight)
{
	//On vérifie que les pixels ont des dimensions valides
	if(imgWidth > 0 && imgHeight > 0)
	{
		// On supprime une éventuelle texture déjà existante
		freeTexture();
		
		//Copie des pixels
		GLuint size = imgWidth * imgHeight;
		mPixels8 = new GLubyte[size];
		memcpy(mPixels8, pixels, size);
		
		//Copie des données de pixels
		mImageWidth = imgWidth;
		mImageHeight = imgHeight;
		mTextureWidth = mImageWidth;
		mTextureHeight = mImageWidth;
		
		//Set du format des pixels
		mPixelFormat = GL_ALPHA;
	}
}

void LTexture::padPixels8()
{
	//mPixels8 doit être valide
	if(mPixels8 != NULL)
	{
		//Ancien attribut de textures
		GLuint oTextureWidth = mTextureWidth;
		//GLuint oTextureHeight = mTextureHeight; La variable est dans le tuto mais n'est jamais utilisée
		
		//Calcule des nouvelles dimensions en puissance de deux
		mTextureWidth = powerOfTwo(mImageWidth);
		mTextureHeight = powerOfTwo(mImageHeight);
		
		//Si on doit changer la taille de l'image en bitmap
		if(mTextureWidth != mImageWidth || mTextureHeight != mImageHeight)
		{
			GLuint size = mTextureWidth * mTextureHeight;
			GLubyte * pixels = new GLubyte[size];
			
			//Copie des pixels par ligne
			for(unsigned int i =0; i<mImageHeight; i++)
				memcpy(&pixels[i*mTextureWidth], &mPixels8[i*oTextureWidth], mImageWidth);
				
			//Changement de mPixels8
			delete[] mPixels8;
			mPixels8 = pixels;
		}
	}
}


void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels 32 bits
	if(mPixels32 != NULL)
	{
		delete [] mPixels32;
		mPixels32 = NULL;
	}
	
	//Suppression des pixels de 8bits
	if(mPixels8 != NULL)
	{
		delete [] mPixels8;
		mPixels8 = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
	
	//Réinitialiser le format des pixels
	mPixelFormat = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels32 == NULL && mPixels8 == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		
		if(mPixelFormat == GL_RGBA)
		{
			mPixels32 = new GLuint[size];
		}
		else if(mPixelFormat == GL_ALPHA)
		{
			mPixels8 = new GLubyte[size];
		}

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, mPixelFormat, GL_UNSIGNED_BYTE, mPixels32);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if((mPixels32 != NULL || mPixels8 != NULL) && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		void * pixels = (mPixelFormat == GL_RGBA) ? (void*)mPixels32 : (void*)mPixels8;
		
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, mPixelFormat, GL_UNSIGNED_BYTE, pixels);

		//Suppression de la structure pixels
		if(mPixels32 != NULL)
		{
			delete [] mPixels32;
			mPixels32 = NULL;
		}
		if(mPixels8 != NULL)
		{
			delete[] mPixels8;
			mPixels8 = NULL;
		}

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels32;
}

GLubyte * LTexture::getPixelData8()
{
	return mPixels8;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels32[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels32[y * mTextureWidth + x] = pixel;
}

GLubyte LTexture::getPixel8(GLuint x, GLuint y)
{
	return mPixels8[y * mTextureWidth + x];
}


void LTexture::setPixel8(GLuint x, GLuint y, GLubyte pixel)
{
	mPixels8[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{	
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Set vertex data
		LVertexData2D vData[4];
		
		//Texture coordinates
		vData[0].texCoord.s = texLeft; vData[0].texCoord.t = texTop;
		vData[1].texCoord.s = texRight; vData[1].texCoord.t = texTop;
		vData[2].texCoord.s = texRight; vData[2].texCoord.t = texBottom;
		vData[3].texCoord.s = texLeft; vData[3].texCoord.t = texBottom;
		
		//Vertex positions
		vData[0].position.x = 0.0f; vData[0].position.y = 0.0f;
		vData[1].position.x = quadWidth; vData[1].position.y = 0.0f;
		vData[2].position.x = quadWidth; vData[2].position.y = quadHeight;
		vData[3].position.x = 0.0f; vData[3].position.y = quadHeight;
		
		//Set texture ID
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
			//Bind Vertex Buffer
			glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
			
			//Update vertex buffer data
			glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(LVertexData2D), vData);
			
			//Set texture coordinate data
			glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, texCoord));
			
			//Set vertex data
			glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, position));
			
			//Draw quad using vertex data and index date
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
			
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

void LTexture::blitPixels32(GLuint x, GLuint y, LTexture& destination)
{
	//Il existe des pixels à blit
	if(mPixels32 != NULL && destination.mPixels32 != NULL)
	{
		//Copie des pixels par ligne
		GLuint* dPixels = destination.mPixels32;
		GLuint* sPixels = mPixels32;
		
		for(unsigned int i = 0; i<mImageHeight; i++)
			memcpy(&dPixels[(i+y)*destination.mTextureWidth + x], &sPixels[i*mTextureWidth], mImageWidth * 4);
	}
}

void LTexture::blitPixels8(GLuint x, GLuint y, LTexture& destination)
{
	//On vérifie de bien avoir des pixels à blit
	if(mPixels8 != NULL && destination.mPixels8 != NULL)
	{
		//Copie des pixels par ligne
		GLubyte* dPixels = destination.mPixels8;
		GLubyte* sPixels = mPixels8;
		for(unsigned int i=0; i<mImageHeight; i++)
			memcpy(&dPixels[(i+y)*destination.mTextureWidth+x], &sPixels[i*mTextureWidth], mImageWidth);
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}

void LTexture::initVBO()
{
	//Si la texture est déjà chargée et que le VBO existe déjà
	if(mTextureID == 0 || mVBOID != 0)
		return;
		
	LVertexData2D vData[4];
	GLuint iData[4];
	
	//Set rendering indices
	iData[0] = 0;
	iData[1] = 1;
	iData[2] = 2;
	iData[3] = 3;
	
	//Création du VBO
	glGenBuffers(1, &mVBOID);
	glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(LVertexData2D), vData, GL_DYNAMIC_DRAW);
	
	//Création de l'IBO
	glGenBuffers(1, &mIBOID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), iData, GL_DYNAMIC_DRAW);
	
	//Détacher les buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void LTexture::freeVBO()
{
	if(mVBOID == 0)
		return;
	
	glDeleteBuffers(1, &mVBOID);
	glDeleteBuffers(1, &mIBOID);
	
}
