/*
Boris Merminod le 03/09/2019

	On va utiliser ici une fonction FreeType
bitmap pour gérer le chargement des pixels des caractères. Cette fonction est incluse via : 

	#include FT_BITMAP_H

	L'attribut statique mLibrary va contenir le
type de TTF font que l'on va utiliser pour notre police de caractère. La fonction initFreeType() va initialiser la librairie et se font pour qu'il ne soit pas nécessaire de le charger à chaque instanciation d'un objet de la classe LFont (A condition évidemment d'utiliser la même police d'écriture entre deux objets de la classe). 
	Une directive pré-processeur va néanmoins
conditionner l'initialisation de cette librairie. Si je comprend bien la bibliothèque FreeType nécessite de travailler en singlethread. Néanmoins l'utilisation de FreeGLUT est multithread et FreeType ne peut avoir qu'une seule instance de FT_Library par thread. Donc si je tente d'utiliser FT_Library sur plusieurs threads différent, l'application va planter. Si l'application est single thread utiliser FT_Library en static est possible 

	La loadFreeType() la fonction commence par initialiser
notre mLibrary pour notre programme multithread. Dans ce cas on va charger si je comprend bien une FT_Library par thread d'un programme FreeGLUT.
	Par la suite on va déclarer plusieurs variables. 
Comme pour le bitmap, un fichier ttf va se décomposer en un ensemble de cellules, et chaque cellule contient un caractère. Les cellules vont alors avoir leurs dimension en largeur et en hauteur soit respectivement cellW et cellH. La fonction comprend une table de type FT_GlyphMetrics et qui va représenter chacun de nos caractères. Le Y bearing est la distance entre le ligne de base et le pixel le plus haut du caractères. Le X bearing est la distance entre l'origine (ligne de base à gauche) et la première colonne de pixel à gauche du caractère. La variable maxbearing concerne le bearing en Y et contient la distance du caractères le plus grand de tous le ttf. La variable minHang contient la valeur du pixel du caractère étant le plus bas dans sa cellule.
	Le chargement du font commence par le chargement d'une
structure de type FT_Face via la fonction FT_NewFace() comme ceci : 

		FT_Face face = NULL; 
		error = FT_New_Face( mLibrary, path.c_str(), 0, &face );

grossièrement un font faces est un ensemble de glyph dont le rendu va être facilité, sachant que chaque font peut avoir plusieurs "faces" comme "normal, gras, italique, etc...". Le FT_Face est alors une table qui va contenir au minimum un élément (un font). Pour effectuer le rendu différent d'un font en utilisant cette table FT_Face, il suffit de vérifier l'existance d'une version différente du caractère dans la table FT_Face et ceci est faisable via la fonction FT_Open_Face(). La documentation FreeType est plus complète sur le sujet. Nous avons donc chargé notre FT_Face dans une table face à l'index 0.
	Une fois notre font face chargé, on va controller la
taille des sprites des caractères. Notre fonction prend en paramètre une variable GLuint pixelSize, qui va être utilisé par la fonction FT_Set_Pixel_Sizes() qui s'utilise comme ceci :

		error = FT_Set_Pixel_Sizes( face, 0, pixelSize );

Le but ici est de déterminer la taille maximale pour nos glyph. L'argument numéro deux de la fonction correspond à la taille maximale en largeur pour les glyphs. En lui passant le paramètre 0 elle ne va prendre en compte que le troisième paramètre correspondant à la taille maximale en hauteur de chaque glyph.
	Ensuite on va parcourir chaque glyph un à un soit 256
éléments. La fonction FT_Load_Char() va charger un glyph en fonction de la valeur ASCII donnée en argument. La fonction prend également un troisième argument FT_LOAD_RENDER ce qui lui indique de récupérer pour chaque caractères les datas du glyph ainsi que les données de rendu de pixels de ce glyph :

		for( int i = 0; i < 256; ++i ) { 
			//Load and render glyph error = FT_Load_Char( face, i, FT_LOAD_RENDER );
			...

	Si le glyph peut être correctement rendu, on va vouloir
récupérer et exploiter les datas du glyph. La structure face possède un membre glyph contenant l'information du glyph qui a été chargé via FT_Load_Char(). On va donc récupérer ici la métrique du glyph grâce à cette donnée associée au glyph comme ceci : 

		metrics[ i ] = face->glyph->metrics; 

Les pixels de chaque image de glyph vont devoir ensuite être stockée dans notre table de LTexture en utilisant la méthode copyPixels8(). Cette copie peut être faite à partir de l'élément bitmap du glyph contenant ainsi les données bitmap qui nous sont nécessaires. Ainsi bitmap.buffer contient un pointeur sur les données de pixels 8 bits, bitmap.width contient la largeur de l'image du glyph, bitmap.rows contient le nombre de lignes de pixels contenu par un glyph ce qui correspond également à la taille de l'image en hauteur.

		bitmaps[ i ].copyPixels8( face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows );

	Nous avons donc récupéré la métrique pour notre glyph,
il va falloir déterminer la taille du maximum bearing. Le membre horiBearingY est la distance entre la ligne de base et le pixel le plus haut du glyph lorsque le texte est rendu horizontalement (La bibliothèque FreeType peut effectuer le rendu du texte verticalement).

		//Calculate max bearing 
		if( metrics[ i ].horiBearingY / 64 > maxBearing ) 
		{ 
			maxBearing = metrics[ i ].horiBearingY / 64; 
		} 
		
		//Calculate max width 
		if( metrics[ i ].width / 64 > cellW ) 
		{ 
			cellW = metrics[ i ].width / 64; 
		} 
		
		//Calculate gylph hang 
		int glyphHang = ( metrics[ i ].horiBearingY - metrics[ i ].height ) / 64; 
		if( glyphHang < minHang ) 
		{ 
			minHang = glyphHang; 
		}

horiBearingY est une valeur divisée par 64. La raison est que l'unité de mesure utilisée pour la metrique des glyphes n'est pas le pixel mais un ensemble de points ou chaque unité de point représente 1/64 pixel. La valeur de la variable maxBearing est en pixel et c'est pourquoi il nous faut convertir la valeur de points en pixels. Un procédé similaire va permettre de déterminer la taille en largeur de la cellule. Enfin il ne nous reste plus qu'à déterminer la valeur du hang représentant la distance à laquelle le glyph descend en dessous de la ligne de base. Il s'agit en quelque sorte du point le plus bas du caractère soit le bearing minimum (le site lazyfoo représente un graph bien réussi expliquant la métrique pour un glyph).
	Le travaille de récupération des images des glyph et de
leurs métrique terminé, il ne nous reste plus qu'a créer une texture pour notre font. En premier on va calculer la taille maximale en hauteur de la cellule en prenant la valeur du glyph le plus haut et en lui soustrayant la valeur du glyph ayant le point le plus bas. Une fois que les dimensions des cellules sont déterminées, on va utiliser createPixels8() pour créer une texture assez large pour contenir toutes les images des glyphs : 

		//Create bitmap font 
		cellH = maxBearing - minHang; 
		createPixels8( cellW * 16, cellH * 16 ); 
		//Begin creating bitmap font 
		GLuint currentChar = 0; 
		LFRect nextClip = { 0.f, 0.f, cellW, cellH }; 
		//Blitting coordinates 
		int bX = 0; int bY = 0;

	Il nous reste alors à parcourir toutes les images des
glyph et à les coller à une LTexture de type LFont. Pour cela on va parcourir chaque cellule. Nous avons pu déterminer auparavant les dimensions de chaque gylph, et nous allons utiliser ses dimensions et faire correspondre les dimensions des sprites de chaque caractères aux dimensions de ces glyphes en pixels. La taille n'est cependant ajusté qu'en largeur, en hauteur chaque caractère doit posséder une hauteur similaire pour avoir une certaine Uniformité dans le rendu des caractères. 

		//Go through cell rows 
		for( unsigned int rows = 0; rows < 16; rows++ ) 
		{ 
			//Go through each cell column in the row 
			for( unsigned int cols = 0; cols < 16; cols++ ) { 
				//Set base offsets 
				bX = cellW * cols; 
				bY = cellH * rows; 
				//Initialize clip 
				nextClip.x = bX; 
				nextClip.y = bY; 
				nextClip.w = metrics[ currentChar ].width / 64; 
				nextClip.h = cellH;
				...

	Au moment du bilt des image de glyphe, un décalage en x
est appliqué correspondant à un décalage avec le bord gauche de la cellule. Le décalage en y lui correspond au point minimum du maxBearing du glyph en pixels. les images sont ensuite blit dans l'objet LFont courant puis un rectangle de clip est ajouté au sprite sheet et le processus se poursuit pour le reste des caractères :

				...
				//Blit character 
				bitmaps[ currentChar ].blitPixels8( bX, bY + maxBearing - metrics[ currentChar ].horiBearingY / 64, *this ); 
				//Go to the next character 
				mClips.push_back( nextClip ); 
				currentChar++; 
			} 
		 }

	Lorsque toutes les images des glyphes ont été blitté,
on va étendre les pixels en puissance de deux puis charger la texture et générer le VBO data.
	Ensuite le découpage de la texture de font est réalisé
puis la taille de l'espace est calculé. Lorsque l'emploi de notre FT_Face est terminé, on va le libérer à l'aide de FT_Done_Face().
	Quand nous avons terminé d'utiliser notre FT_Library,
dans notre applis multithread, on va appeler FT_Done_FreeType() pour fermer notre librairie.

	la fonction renderText() ne présente qu'un changement 
minime dans la manière de récupérer les sprites des caractères, rien de fou
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/23_freetype_fonts/index.php
*/

#include "LFont.h"
#include FT_BITMAP_H

//Librairie LFont
FT_Library LFont::mLibrary;

bool LFont::initFreeType()
{
	//Initialise FreeType pour une application single thread
	#ifndef __FREEGLUT_H__
		FT_Error error = FT_Init_FreeType(&mLibrary);
		if(error)
		{
			fprintf(stderr, "LFont::initFreeType : FreeType error => %X", error);
			return false;
		}
	#endif

	return true;
}

LFont::LFont()
{
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

LFont::~LFont()
{
	freeFont();
}

bool LFont::loadBitmap(std::string path)
{
	//Loading flag
	bool success = true;
	//Background pixel
	const GLubyte BLACK_PIXEL = 0x00;
	
	//Suppression du fond existant 
	freeFont();
	//Image pixels loaded
	if(loadPixelsFromFile8(path))
	{
		//Get cell dimension
		GLfloat cellW = imageWidth() / 16.f;
		GLfloat cellH = imageHeight() / 16.f;
		
		//Get letter bottom and top
		GLuint top = cellH;
		int bottom = 0;
		GLuint aBottom = 0;
		
		//Current pixel coordinates
		int pX = 0;
		int pY = 0;
		
		//Base cell offsets
		int bX = 0;
		int bY = 0;
		
		//Begin parsing bitmap font
		GLuint currentChar = 0;
		LFRect nextClip = {0.f, 0.f, cellW, cellH};
		
		//Go through celle rows
		for(unsigned int rows = 0; rows<16; rows++)
		{
			for(unsigned int cols = 0; cols <16; cols++)
			{
				//Begin cell parsing
				
				//Set Base offset
				bX = cellW * cols;
				bY = cellH * rows;
				
				//Initialize clip
				nextClip.x = cellW * cols;
				nextClip.y = cellH * rows;
				
				nextClip.w = cellW;
				nextClip.h = cellH;
				
				//Find left side of character
				for(int pCol = 0; pCol < cellW; pCol++)
				{
					for(int pRow = 0; pRow < cellH; pRow++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's x offset
							nextClip.x = pX;
							
							//Break the loops
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Right Side
				for(int pCol_w = cellW - 1; pCol_w >= 0; pCol_w--)
				{
					for(int pRow_w = 0; pRow_w < cellH; pRow_w++)
					{
						//Set pixel offset
						pX = bX + pCol_w;
						pY = bY + pRow_w;
						
						//Non_background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's width
							nextClip.w = (pX - nextClip.x) + 1;
							
							//Break the loops
							pCol_w = -1;
							pRow_w = cellH;
						}
					}
				}
				
				//Find top
				for(unsigned int pRow = 0; pRow < cellH; pRow++)
				{
					for(int pCol = 0; pCol < cellW; pCol++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non-background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//New Top found
							if(pRow < top)
								top = pRow;
							
							//Break the loop
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Find bottom
				for(int pRow_b = cellH - 1; pRow_b >= 0; pRow_b--)
				{
					for(int pCol_b = 0; pCol_b < cellW; pCol_b++)
					{
						//Set pixel offset
						pX = bX + pCol_b;
						pY = bY + pRow_b;
						
						//Non-background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set BaseLine
							if(currentChar == 'A')
								aBottom = pRow_b;
							
							if(pRow_b > bottom)
								bottom = pRow_b;
							
							//Break the loop
							pCol_b = cellW;
							pRow_b = -1;
						}
					}
				}
				
				//Go to next character
				mClips.push_back(nextClip);
				currentChar++;
			}
		}
		
		//Set top
		for(int t=0; t<256; t++)
		{
			mClips[t].y += top;
			mClips[t].h -= top;
		}
		
		//Create texture from manipulated pixels
		if(loadTextureFromPixels8())
		{
			//Build vertex buffer from sprite sheet data
			if(!generateDataBuffer(LSPRITE_ORIGIN_TOP_LEFT))
			{
				fprintf(stderr, "loadBitmap : Impossible de créer le vertex buffer à partir du bitmap font\n");
				success = false;
			}
		}
		else
		{
			fprintf(stderr, "loadBitmap : Impossible de créer une texture à partir des pixels du bitmap font\n");
			success = false;
		}
		
		//Set font texture wrap
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		
		//Set spacing variables
		mSpace = cellW / 2;
		mNewLine = aBottom - top;
		mLineHeight = bottom - top;
	}
	else
	{
		fprintf(stderr, "loadBitmap : Impossible de charger l'image bitmap font : %s\n", path.c_str());
		success = false;
	}
	
	return success;
}

bool LFont::loadFreeType(std::string path, GLuint pixelSize)
{
	//Flag d'erreur
	FT_Error error = 0;

	//Initialisation de FreeType pour des application multithread
	#ifdef __FREEGLUT_H__
		error = FT_Init_FreeType(&mLibrary);
		if(error)
		{
			fprintf(stderr, "LFont::loadFreeType : erreur => %X\n", error);
			return false;
		}
	#endif

	//Récupérer les dimension d'une cellule
	GLuint cellW= 0;
	GLuint cellH = 0;
	int maxBearing = 0;
	int minHang = 0;

	//Données des caractères
	LTexture bitmaps[256];
	FT_Glyph_Metrics metrics[256];

	//Load face
	FT_Face face = NULL;
	error = FT_New_Face(mLibrary, path.c_str(), 0, &face);
	if(!error)
	{
		//Set face size
		error = FT_Set_Pixel_Sizes(face, 0, pixelSize);
		if(!error)
		{
			//Go through extended ASCII to get glyph data
			for(int i = 0; i<256; i++)
			{
				//Load and render a glyph
				error = FT_Load_Char(face, i, FT_LOAD_RENDER);
				if(!error)
				{
					//Get metrics
					metrics[i] = face->glyph->metrics;

					//Copy glyph bitmap
					bitmaps[i].copyPixels8(face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows);

					//Calculate bearing max
					if(metrics[i].horiBearingY / 64 > maxBearing)
					{
						maxBearing = metrics[i].horiBearingY / 64;
					}

					//Calculate max width
					if(metrics[i].width/64 > cellW)
					{
						cellW = metrics[i].width / 64;
					}

					//Calculate glyph hang
					int glyphHang = (metrics[i].horiBearingY - metrics[i].height) / 64;
					if(glyphHang < minHang)
					{
						minHang = glyphHang;
					}
				}
				else
				{
					fprintf(stderr, " LFont::loadFreeType : Impossible de charger le glyph. FreeType error => %X\n", error);
					error = 0;
				}
				
			}

			//Création du font bitmap
			cellH = maxBearing - minHang;
			createPixels8(cellW * 16, cellH * 16);

			//Commence à construire le bitmap font
			GLuint currentChar = 0;
			LFRect nextClip = {0.f, 0.f, (float)cellW, (float) cellH};

			// Coordonnées de blitting
			int bX = 0;
			int bY = 0;

			for(unsigned int rows = 0; rows < 16; rows++)
			{
				for(unsigned int cols = 0; cols < 16; cols++)
				{
					//Set de l'offset de base
					bX = cellW * cols;
					bY = cellH * rows;

					//Initialisation du clip
					nextClip.x = bX;
					nextClip.y = bY;
					nextClip.w = metrics[currentChar].width / 64;
					nextClip.h = cellH;

					//Blit le caractère
					bitmaps[currentChar].blitPixels8(bX, bY + maxBearing - metrics[currentChar].horiBearingY/64, *this);

					//Passer au caractère suivant
					mClips.push_back(nextClip);
					currentChar++;
				}
			}

			//Fait de la texture une puissance de deux
			padPixels8();

			//Création de la texture
			if(loadTextureFromPixels8())
			{
				//Construit un vertex buffer depuis le sprite sheet data
				if(!generateDataBuffer(LSPRITE_ORIGIN_TOP_LEFT))
				{
					fprintf(stderr, " LFont::loadFreeType : Impossible de créer le vertex buffer depuis le bitmap font");
					error = 0xA2;
				}
			}
			else
			{
				fprintf(stderr, " LFont::loadFreeType : Impossible de créer une texture depuis le bitmap font généré \n");
				error = 0xA2;
			}

			//Set du font texture wrap
			glBindTexture(GL_TEXTURE_2D, getTextureID());
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

			//Set des variable de l'espacement
			mSpace = cellW/2;
			mNewLine = maxBearing;
			mLineHeight = cellH;
		}
		else
		{
			fprintf(stderr, " LFont::loadFreeType : Impossible de set la taille du font. FreeTypeError: %X\n", error);
		}

		//Free Face
		FT_Done_Face(face);
	}
	else
	{
		fprintf(stderr, " LFont::loadFreeType : Impossible de charger la font face . FreeType error => %X\n", error);
		return false;
	}

	//Ferme Freetype pour les applications multithread
	#ifdef __FREE_GLUT_H__
		FT_Done__FreeType(mLibrary);
	#endif

	return error == 0;
}

void LFont::freeFont()
{
	//Get rid of sprite sheet
	freeTexture();
	
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

void LFont::renderText(GLfloat x, GLfloat y, std::string text)
{
	//If there is a texture to render from
	if(getTextureID() != 0)
	{
		//Draw position
		GLfloat dX = x;
		GLfloat dY = y;
		
		//Move to draw position
		glTranslatef(x, y, 0.f);
		
		//Set texture
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		//Bind vertex data
		glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
		
		//Set texture coordinate data
		glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, texCoord));
		
		//Set vertex data
		glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, position));
		
		//Go through string
		for(unsigned int i=0; i<text.length(); i++)
		{
			//Space
			if(text[i] == ' ')
			{
				glTranslatef(mSpace, 0.f, 0.f);
				dX += mSpace;
			}
			//NewLine
			else if(text[i] == '\n')
			{
				glTranslatef(x - dX, mNewLine, 0.f);
				dY += mNewLine;
				dX += x - dX;
			}
			//character
			else
			{
				//Get ASCII
				GLuint ascii = (unsigned char) text[i];
				
				//Draw quad using vertex data and index data
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[ascii]);
				glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
				
				//Move over
				glTranslatef(mClips[ascii].w, 0.f, 0.f);
				dX += mClips[ascii].w;
			}
		}
		
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		
	}
}
