/*
	Boris Merminod le 14/05/2018
	
	Ecriture des méthodes de la classe LTexture
	
		Le constructeur va initialiser les attributs de notre classe, le destructeur lui va appeler la méthode freeTexture qui va libérer la mémoire allouée pour la gestion d'une instance de LTexture
		
		La méthode loadTextureFromPixels va convertir des données de pixels en une texture. Pour cela elle va prendre en paramètres un pointeur sur les données de pixels (sous la forme d'un tableau), et les dimensions de la texture. La méthode appelle freeTexture() en premier lieu pour éviter de charger deux fois des pixels en une même texture. On s'assure ainsi que la texture reste belle et bien vide. Ensuite on assigne à l'objet les dimensions voulue.
		On appelle ensuite la fonction glGenTextures(1, &mTextureID) dont le but est d'assigner une ID à une texture. elle va donc prendre un pointeur sur un GLUint et lui attacher une ID. Le premier paramètre de la fonction indique le nombre d'ID à générer.
		La fonction glBindTexture(GL_TEXTURE_2D, mTextureID) vient ensuite attacher cette ID à une texture. Celà va nous permettre d'effectuer par la suite des opérations sur cette nouvelle texture.
		Ensuite vient la fonction glTexImage2D() qui va assigner nos pixels à notre ID de texture pour générer une texture. La fonction prend plusieurs argument : 
	- GL_TEXTURE_2D : le type de texture à assigner au pixel ici c'est une texture en 2D
	- 0 : Un paramètre nommé mipmap level qui sera décrit dans un futur chapitre
	- GL_RGBA : Le format de stockage des pixels, ceci est pris par OpenGL comme une suggestion et non un ordre
	- width : La largeur de la texture
	- height : la hauteur de la texture
	- 0 : Le bord de la texture's width
	- GL_RGBA : Le format des données de pixels qui seront assignés
	- GL_UNSIGNED_BYTE : Le type de données pour les données de pixels qui seront assigné
	- pixels : Un pointeur sur les données de pixels qui seront assigné 
	Bon après tout ça notre pixel data se retrouvera bien au chaud dans la mémoire cache du GPU.
		Désormais nos pixels sont associées à une texture, il ne nous reste plus qu'à paramétrer des attributs de notre texture via glTexParameter(). Deux paramètres de cette fonction à souligner  GL_TEXTURE_MAG_FILTER et GL_TEXTURE_MIN_FILTER qui contrôle comment doit se comporter la texture lorsque la fenêtre est réduite ou agrandie. Un filtre GL_LINEAR va être utilisé et sera expliqué dans les chapitres suivants.
		Notre texture chargée, on va la détacher en quelque sorte de la mémoire, pour cela on va attacher via glBindTexture une texture NULL. Ceci est nécessaire pour libérer en fin d'utilisation la mémoire allouée par la texture.
		La fin de la fonction va tenter de récupérer une éventuelle erreur et de l'afficher à l'utilisateur.
		
		La méthode freeTexture va appeler la fonction glDeleteTextures qui va retrouver la texture lié à l'ID qui lui est passée en paramètre et la supprimer. Par la suite on assigne 0 à l'ID de la texture. 
		
		Le méthode render va effectuer le rendu de notre texture. Premièrement on check si la texture existe via son ID, si c'est le cas on va bouger à la position de rendu puis attacher notre texture à son ID.
		Maintenant que notre texture est lié, on va pouvoir texturer un carré. Lorsque l'on appelle glColor avant l'appelle de rendu d'un sommet, la couleur sera alors liée à ce sommet. Pour les textures c'est la même chose chaque sommet est associée à un code de coordonnées de texture via la fonction glTexCoord(). glTexCoord va attacher un point de notre texture à un sommet de notre forme géométrique, ainsi quand le carré est rendu la texture est cartographié sur cette forme.
		Dans les programmes précédents, le rendu est effectué sachant que l'origine de la forme géométrique est au centre de cette même forme. Dans notre méthode de rendu, l'origine se trouve en haut à gauche puis chaque sommet est rendu celui en haut à droite, puis en bas à droite et enfin en bas à gauche. Les coordonnées de textures ont encore une autre façon de fonctionner : les coordonnées sont représenté selon deux axes : s l'axe horizontal et t l'axe verticale. A s = 0 on est au bord gauche de la texture, à s = 1 au bord droit, à t = 0 on est au bord supérieur et à t = 1 au bord inférieur. En utilisant ces axes via la fonction glTexCoord on va mapper chaque coin de notre texture. 

		Il ne nous reste que les accesseurs servant à accéder au attributs membres.
		
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/05_texture_mapping_and_pixel_manipulation/index.php
*/

#include "LTexture.h"

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mTextureWidth = width;
	mTextureHeight = height;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}
	
	mTextureWidth = 0;
	mTextureHeight = 0;
}

void LTexture::render(GLfloat x, GLfloat y)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(0.f,0.f); glVertex2f(0.f,0.f);
			glTexCoord2f(1.f,0.f); glVertex2f(mTextureWidth,0.f);
			glTexCoord2f(1.f,1.f); glVertex2f(mTextureWidth,mTextureHeight);
			glTexCoord2f(0.f,1.f); glVertex2f(0.f,mTextureHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}
