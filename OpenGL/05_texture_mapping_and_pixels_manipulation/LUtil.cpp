/*
Boris merminod le 15/05/2018

	Dans notre LUtil.cpp file on va déclarer un objet global de la classe LTexture :
	LTexture gCheckerBoardTexture;
Nous allons donc créer une surface carré qui représente un damier

	Dans notre fonction initGL on initialise le mode texture via la fonction glEnable() et le paramètre GL_TEXTURE_2D. Sans cela les opérations de texturing seront ignoré purement et simplement

	Le but de se chapitre et d'effectuer le rendu texturé d'un damier de 128x128 pixels. Nous allons donc déclarer dans la fonction loadMedia une table checkerBoard avec cette taille.
	Notre table de checkerBoard est une table de type GLuint. Un GLuint a une taille de 32 bits soit 4x8 bits. Pour représenter une couleur on a besoin de 8 bits puisque chaque couleur RGB est comprise entre une plage de 0-255. Ainsi chaque GLuint de notre table peut être décomposée en une table de 4 x 8 bits afin de représenter la couleur du pixel qui lui est associé. Pour cela on va utiliser une boucle et un cast comme ceci :
	for( int i = 0; i < CHECKERBOARD_PIXEL_COUNT; ++i ) 
	{ 
		//Get the individual color components 
		GLubyte* colors = 	(GLubyte*)&checkerBoard[ i ];

Ce pointeur GLubyte* colors va devenir une table de 4 éléments de 8 bits chacun, pour cela on va caster chaque élément de checkerBoard en un GLubyte*. Pour rappelle 1 byte = 1 octet = 8bits. La table est sur 4 éléments car le dernier élément représente l'alpha la transparence du pixel. Ceci est la raison de l'utilisation du paramètre GL_RGBA dans la fonction glTexImage2D dans LTexture.cpp
	Dans la partie suivante le test i / 128 & 16 ^ i % 128 & 16 va permettre d'attribuer la couleur blanche au pixel s'il est vrai et rouge s'il est faux (opération par bit à revoir si besoin). Notre table de 4 éléments peut prendre une notation hexadécimal pour attribuer la valeur de la couleur 0xFF -> 255. L'alpha doit être à 255 pour être complètement opaque, ou à 0 pour être complètement transparent 
	Quand on a fait tout ça on peut appeler la méthode loadTetureFromPixels32 pour transformer notre checkerBoard en texture. Attention à toujours veiller à ce que la largeur et la hauteur d'une texture soit toujours une puissance de 2. Ainsi 64x64, 128x32 mais pas 256x200.
	Notre méthode de rendu va ensuite afficher la texture au centre de l'écran. 

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/05_texture_mapping_and_pixel_manipulation/index.php
*/

#include "LUtil.h"
#include "LTexture.h"

//Checkerboard texture
LTexture gCheckerBoardTexture;


bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Checkerboard pixels
	const int CHECKERBOARD_WIDTH =128;
	const int CHECKERBOARD_HEIGHT = 128;
	const int CHECKERBOARD_PIXEL_COUNT = CHECKERBOARD_WIDTH * CHECKERBOARD_HEIGHT;
	GLuint checkerBoard[CHECKERBOARD_PIXEL_COUNT];
	
	//Passage par tous les pixels
	for(int i = 0; i<CHECKERBOARD_PIXEL_COUNT;i++)
	{
		//Obtenir la composante couleur individuelle
		GLubyte* colors = (GLubyte*)&checkerBoard[i];
		
		// Si le 5ème bit du décalage x et y du pixel ne match pas
		if(i / 128 & 16 ^ i % 128 & 16)
		{
			//Alors on considère que la couleur est blanche
			colors[0] = 0xFF;
			colors[1] = 0xFF;
			colors[2] = 0xFF;
			colors[3] = 0xFF;
		}
		else
		{
			//Sinon on considère que la couleur du pixel est rouge
			colors[0] = 0xFF;
			colors[1] = 0x00;
			colors[2] = 0x00;
			colors[3] = 0xFF;
		}
	}
	
	//Chargement de la texture
	if(!gCheckerBoardTexture.loadTextureFromPixels32(checkerBoard, CHECKERBOARD_WIDTH, CHECKERBOARD_HEIGHT))
	{
		fprintf(stderr, "loadMedia : Impossible de charger la texture\n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Calcul du décalage centré
	GLfloat x = (SCREEN_WIDTH - gCheckerBoardTexture.textureWidth()) /
	2.f;
	GLfloat y = (SCREEN_HEIGHT - gCheckerBoardTexture.textureHeight()) /
	2.f;
	
	//Rendu de la texture checkerboard
	gCheckerBoardTexture.render(x, y);
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
