/*
	Boris Merminod le  

	On ajoute à la méthode render() un nouvel argument qui va spécifier de combien de degrées nous voulons effectuer la rotation de notre quad. La première partie de notre fonction de rendu ne change pas : l'attribution des paramètres de texture, les dimensions du quad, la gestion du rendu et du stretching. 
	Ensuite on va vouloir effectuer une rotation autour du point centrale de notre quad. Nous allons utiliser glRotate() pour effectuer la rotation, mais au préalable nous allons déplacer notre point de rotation au centre de l'image. 

 //Move to rendering point 
 glTranslatef( x + quadWidth / 2.f, y + quadHeight / 2.f, 0.f ); 
 
 //Rotate around rendering point 
 glRotatef( degrees, 0.f, 0.f, 1.f );

 La fonction glRotate() va effectuer sa rotation autour du point actuel de translation. Dans le code ci-dessus, on va faire un glTranslate(), pour bouger le point de translation vers le centre de l'image en donnant la position de l'image à l'écran plus la moitié des dimension du quad.
 Ensuite on effectue la roation via glRotate(). Le premier argument précise de combien de degrées on souhaite faire cette rotation. Les trois autres arguments sont les composantes x,y et z du vecteur autour duquel on souhaite tourner. Ici on tourne autour de l'axe z dans une rotation positive. glRotate() attend un vecteur dit normalisé, mais il devrait être capable de faire cette normalisation de lui-même peu importe le vecteur passé en paramètre.

 glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(-quadWidth/2.f,-quadHeight / 2.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth / 2.f, -quadHeight / 2.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth / 2.f,quadHeight / 2.f);
			glTexCoord2f(texLeft,texBottom); glVertex2f(-quadWidth / 2.f,quadHeight / 2.f);	
 glEnd();

Nous avons donc déplacer le point de translation au centre du quad que nous allons rendre. Du coup on détermine les sommet du quad à partir du centre de celui-ci ce qui entraîne une modification du code ci-dessus par rapport au versions précédente.
Néanmoins la gestion des coordonnées de notre texture reste la même. La texture va être cartographié à partir des sommets du quad. 

Code source tiré de : 
*/

#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels = NULL;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadPixelsFromFile(std::string path)
{
	//Libération de la mémoire allouée éventuelle pour d'autre texture
	freeTexture();

	//Flag de chargement de texture
	bool pixelsLoaded = false;

	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());

	//Si l'image s'est chargée comme il faut 
	if(success == IL_TRUE)
	{
		//Conversion de l'image vers le RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requise de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une mauvaise taille
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Place l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);

				//Redimensionne l'image
				iluEnlargeCanvas((int) texWidth, (int) texHeight, 1);
			}

			//Allocation de mémoire pour les données de texture
			GLuint size = texWidth * texHeight;
			mPixels = new GLuint[size];

			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;

			//Copier les pixels en mémoire
			memcpy(mPixels, ilGetData(), size * 4);
			pixelsLoaded = true;
		}

		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
	}

	//Report des erreurs
	if(!pixelsLoaded)
		fprintf(stderr, "Impossible de charger %s\n", path.c_str());
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromFileWithColorKey(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{
	//Charger les pixels
	if(!loadPixelsFromFile(path))
		return false;
	
	//Parcourir les pixels
	GLuint size = mTextureWidth * mTextureHeight;
	for(unsigned int i = 0; i < size; i++)
	{
		// Récupérer la couleur du pixel
		GLubyte* colors = (GLubyte*)&mPixels[i];

		//Si la couleur correspond
		if(colors[0] == r && colors[1] == g && colors[2] == b && (0 == a || colors[3] == a))
		{
			//Ajuster la transparence
			colors[0] = 255;
			colors[1] = 255;
			colors[2] = 255;
			colors[3] = 000;
		}
	}

	//Créer la texture
	return loadTextureFromPixels32();
}

bool LTexture::loadTextureFromPixels32()
{
	//Flag de succès de la fonction
	bool success = true;

	//Si l'attribut pixels est chargé
	if(mTextureID == 0 && mPixels != NULL)
	{
		//Générer une ID de texture
		glGenTextures(1, &mTextureID);

		//Attacher la textureID
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Mise en place des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur de chargement de %p pixels ! %s\n", mPixels, gluErrorString(error));
			success = false;
		}
		else 
		{
			//Suppression de l'attribut pixels
			delete [] mPixels;
			mPixels = NULL;
		}
	}
	// En cas d'erreur
	else 
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Structure mPixels non chargé, impossible de créer la texture !\n");

		//La texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La texture existe déjà \n");
		else if(mPixels == NULL)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La structure mPixels n'est pas initialisée\n");
		
		success = false;
	}

	return success;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels
	if(mPixels != NULL)
	{
		delete [] mPixels;
		mPixels = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		mPixels = new GLuint[size];

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if(mPixels != NULL && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Suppression de la structure pixels
		delete [] mPixels;
		mPixels = NULL;

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip, LFRect* stretch, GLfloat degrees)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}

		//Gestion du stretching
		if(stretch != NULL)
		{
			quadWidth = stretch->w;
			quadHeight = stretch->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x + quadWidth / 2.f, y + quadHeight / 2.f, 0.f);

		//Tourner autour d'un point de rendu
		glRotatef(degrees, 0.f, 0.f, 1.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(-quadWidth/2.f,-quadHeight / 2.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth / 2.f, -quadHeight / 2.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth / 2.f,quadHeight / 2.f);
			glTexCoord2f(texLeft,texBottom); glVertex2f(-quadWidth / 2.f,quadHeight / 2.f);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}
