/*
Boris merminod le 

On déclare en variables globales une instance de notre classe LTexture gRotatingTexture pour gérer le rendu de notre texture et une variable GLfloat gAngle qui va gérer la roation de notre texture

La fonction loadMedia() va s'occuper de charger notre texture comme à son habitude.

La fonction update() va travailler cette fois-ci. Nous souhaitons que notre image tourne à chaque seconde. Donc on va ajouter à l'angle de rotation 360 degrées / nombre_de_frame_par_seconde. L'angle de rotation ne pourra augmenter indéfiniement aussi on essai de le garder limité entre 0 et 360 degrées 

Puis vient notre fonction render(), travaillant à effectuer le rendu des textures. On pourrait se demander pourquoi ne pas effectuer les calculs de rotation par la fonction de rendu ? Par convention une fonction de rendue propre dans une boucle de jeu ne devrait jamais effectuer de caclul mettant à jour les paramètres des objets, le but de la fonction render() est d'effectuer le rendu tel quel des objets. ainsi sans update() pour l'influencer render() devrait toujours rendre la même chose

code source tiré de :
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Texture d'une flèche
LTexture gRotatingTexture;

//Angle de rotation
GLfloat gAngle = 0.f;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Initialisation de la clear color
	//glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gRotatingTexture.loadTextureFromFile("sprite/arrow.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{
	gAngle += 360.f / SCREEN_FPS;

	//Cap angle
	if(gAngle > 360.f)
		gAngle -= 360.f;
}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des textures
	gRotatingTexture.render((SCREEN_WIDTH - gRotatingTexture.imageWidth()) / 2.f,  ( SCREEN_HEIGHT - gRotatingTexture.imageHeight())/2.f, NULL, NULL, gAngle);
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
