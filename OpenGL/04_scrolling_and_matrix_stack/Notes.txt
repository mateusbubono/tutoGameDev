	Dans ce chapitre, on va effectuer le rendu d'une partie d'une scène en utilisant le champs de vue d'une caméra. Nous allons scroller dans la scène en déplaçant le champs de vue de la caméra. Pour cela on va utiliser des transformations de matrice modelview

LUtil.cpp :

	On commence par déclarer deux variable globales qui vont servir à définir la position de notre caméra dans l'espace : 
	GLfloat gCameraX = 0.f, gCameraY = 0.f;
	
	La fonction initGL est essentiellement la même que d'habitude, hormis son utilisation d'une fonction glPushMatrix(). Les matrices de projection et de modelview possèdent une pile qui leur est associée, il est donc possible de push une copie de la matrice courante pour la sauvegardée pour plus tard.
	Ainsi on va appliquer un mouvement de translation à la matrice de modelview pour effectuer un scrolling de l'environnement. Cependant cette fois on ne va pas appliquer les deux fonctions glLoadIdentity et glOrtho comme dans les tutos précédents, on va push une copie de la matrice modelview originale  en scrolling vers la position de la caméra que l'on va sauvegarder dans une pile pour le moment ou l'on aura besoin d'appliquer des transformations dessus. Il est considéré comme une mauvais habitude d'appliquer les opérations de transformations sur la matrice de projection car cela va interférer avec les calcul de fog et de lighting. La pile des matrices n'est pas infinie au delà d'une certaine limite elle déclenche une exception GL_STACK_OVERFLOW récupérée par glGetError().
	
	Dans la fonction handleKeys l'appuie sur une touche vont pouvoir effectuer des modification sur la position de la caméra. 
	Lorsque la position de la caméra est changée, il faut alors changer la matrice de défaut de notre caméra. Prmièrement on fait un pop de l'ancienne matrice au sommet de la pile via glPopMatrix(). 
	Ensuite on charge la matrice d'identité en matrice courante modelview. Ensuite on effectue la translation dans la matrice modelview en utilisant le décalage par la position de la caméra ainsi tout sera rendu par rapport à la position relative de la caméra.
	Il ne reste plus qu'à sauvegarder notre matrice dans la pile via un push pour la réutiliser plus tard.
	
	Dans la fonction render, on va éviter d'utiliser glLoadIdentity pour réinitialiser la matrice modelview, on va à la place enchaîner un glPopMatrix suivi d'un glPushMatrix ce qui va avoir le même effet. La matrice modelview va alors être rendue relativement à la position de la caméra, il ne reste plus qu'a effectuer le rendu des formes géométriques (carrés).
	Ici nous effectuons le rendu d'une scène deux fois plus grande que l'écran. La géométrie ne change à aucun moment de position, il n'y a que la caméra qui bouge. 
	Biensûre il est possible d'effecuter des zoom ou des rotations au lieu des rotations en utilisant les fonctions adéquate. Petit point important ici, le rendu est effectué sur l'ensemble de la scène même pour les élément hors du champs de vue de la caméra. On peut éviter cela ce qui gaspille du temps GPU en utilisant une technique dite Occlusion Test. Le but est de tester la collision entre l'objet à rendre et la box de la caméra.
