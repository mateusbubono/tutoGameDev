/*
Boris Merminod le 13/05/2018
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/04_scrolling_and_the_matrix_stack/index.php

*/

#include "LUtil.h"

/*
Pre-conditions :
	- freeGLUT est initialisé
Post-conditions :
	- Appeler la fonction main loop et l'initialisée pour qu'elle soient appelée en call back toute les 1000 / SCREEN_FPS ms.
Side Effects :
	-Initialisation de la fonction glutTimerFunc
*/

void runMainLoop(int val)
{
	//Frame Logic
	update();
	render();
	
	//Run frame one more time
	glutTimerFunc(1000 / SCREEN_FPS, runMainLoop, val);
}

int main(int argc, char * args[])
{
	//Initialisation de FreeGLUT
	glutInit(&argc, args);
	
	//Création du contexte OpenGL 2.1 : Si ça crash ici il suffit de commenter cette ligne et se référer au lien vers le site lazyfoo pour des explications
	glutInitContextVersion(2,1);
	
	//Création d'une fenêtre en double buffer
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("OpenGL");
	
	//Initialisation post création window/context 
	if(!initGL())
	{
		fprintf(stderr, "Unable to initialize graphics library !\n");
		return EXIT_FAILURE;
	}
	
	//Initialise le gestionnaire de clavier
	glutKeyboardFunc(handleKeys);
	
	//Initialise la fonction de rendu
	glutDisplayFunc(render);
	
	//Initialise main loop
	glutTimerFunc(1000 / SCREEN_FPS, runMainLoop, 0);
	
	//Démarrage du GLUT main loop
	glutMainLoop();
	
	return EXIT_SUCCESS;
	
}
