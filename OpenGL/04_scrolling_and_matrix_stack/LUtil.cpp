/*
Boris merminod le 13/05/2018

	On commence par déclarer deux variable globales qui vont servir à définir la position de notre caméra dans l'espace : 
	GLfloat gCameraX = 0.f, gCameraY = 0.f;
	
	La fonction initGL est essentiellement la même que d'habitude, hormis son utilisation d'une fonction glPushMatrix(). Les matrices de projection et de modelview possèdent une pile qui leur est associée, il est donc possible de push une copie de la matrice courante pour la sauvegardée pour plus tard.
	Ainsi on va appliquer un mouvement de translation à la matrice de modelview pour effectuer un scrolling de l'environnement. Cependant cette fois on ne va pas appliquer les deux fonctions glLoadIdentity et glOrtho comme dans les tutos précédents, on va push une copie de la matrice modelview originale  en scrolling vers la position de la caméra que l'on va sauvegarder dans une pile pour le moment ou l'on aura besoin d'appliquer des transformations dessus. Il est considéré comme une mauvais habitude d'appliquer les opérations de transformations sur la matrice de projection car cela va interférer avec les calcul de fog et de lighting. La pile des matrices n'est pas infinie au delà d'une certaine limite elle déclenche une exception GL_STACK_OVERFLOW récupérée par glGetError().
	
	Dans la fonction handleKeys l'appuie sur une touche vont pouvoir effectuer des modification sur la position de la caméra. 
	Lorsque la position de la caméra est changée, il faut alors changer la matrice de défaut de notre caméra. Prmièrement on fait un pop de l'ancienne matrice au sommet de la pile via glPopMatrix(). 
	Ensuite on charge la matrice d'identité en matrice courante modelview. Ensuite on effectue la translation dans la matrice modelview en utilisant le décalage par la position de la caméra ainsi tout sera rendu par rapport à la position relative de la caméra.
	Il ne reste plus qu'à sauvegarder notre matrice dans la pile via un push pour la réutiliser plus tard.
	
	Dans la fonction render, on va éviter d'utiliser glLoadIdentity pour réinitialiser la matrice modelview, on va à la place enchaîner un glPopMatrix suivi d'un glPushMatrix ce qui va avoir le même effet. La matrice modelview va alors être rendue relativement à la position de la caméra, il ne reste plus qu'a effectuer le rendu des formes géométriques (carrés).
	Ici nous effectuons le rendu d'une scène deux fois plus grande que l'écran. La géométrie ne change à aucun moment de position, il n'y a que la caméra qui bouge. 
	Biensûre il est possible d'effecuter des zoom ou des rotations au lieu des rotations en utilisant les fonctions adéquate. Petit point important ici, le rendu est effectué sur l'ensemble de la scène même pour les élément hors du champs de vue de la caméra. On peut éviter cela ce qui gaspille du temps GPU en utilisant une technique dite Occlusion Test. Le but est de tester la collision entre l'objet à rendre et la box de la caméra.

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/04_scrolling_and_the_matrix_stack/index.php
*/

#include "LUtil.h"

//La position de la caméra
GLfloat gCameraX = 0.f, gCameraY = 0.f;


bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Sauvegarde de la matrice modelview par défaut
	glPushMatrix();
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Pop de la matrice par défaut dans la matrice courante
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	
	//Sauvegarde de la matrice par défaut à nouveau
	glPushMatrix();
	
	//Bouger au centre de l'écran
	glTranslatef(SCREEN_WIDTH/2.f, SCREEN_HEIGHT / 2.f, 0.f);
	
	//Un carré rouge
	glBegin(GL_QUADS);
		glColor3f(1.f, 0.f, 0.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
	glEnd();
	
	//Bouger vers la droite de l'écran
	glTranslatef(SCREEN_WIDTH, 0.f, 0.f);
	
	//Un carré vert
	glBegin(GL_QUADS);
		glColor3f(0.f, 1.f, 0.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
	glEnd();
	
	//Bouger vers le bas de l'écran
	glTranslatef(0.f, SCREEN_HEIGHT, 0.f);
	
	//Un carré bleu
	glBegin(GL_QUADS);
		glColor3f(0.f, 0.f, 1.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
	glEnd();
	
	//Bouger vers la gauche de l'écran
	glTranslatef(-SCREEN_WIDTH, 0.f, 0.f);
	
	//Un carré jaune
	glBegin(GL_QUADS);
		glColor3f(1.f, 1.f, 0.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f);
		glVertex2f(SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
		glVertex2f(-SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f);
	glEnd();
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Changement de la position de la caméra si l'utilisateur utilise w/a/s/d
	if(key == 'w')
		gCameraY -= 16.f;
	else if(key == 's')
		gCameraY += 16.f;
	else if(key == 'a')
		gCameraX -= 16.f;
	else if(key == 'd')
		gCameraX += 16.f;
	
	//Prend la matrice hors de la pile et réinitialise là
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glLoadIdentity();
	
	//Bouger la caméra à la position donnée
	glTranslatef(-gCameraX, -gCameraY, 0.f);
	
	//Sauvegarde de la matrice par défaut de nouveau en prenant en compte le mouvement de la caméra
	glPushMatrix();
}
