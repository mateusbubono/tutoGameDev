/*
Boris merminod le 15/01/2019

	Au sommet de notre LUtil.cpp, on va commencer par définir quelques variables globales :
	
	//Les sommets d'un Quad
	LVertexPos2D gQuadVertices[4];

	//Indices des sommets
	GLuint gIndices[4];

	//Buffer des sommets
	GLuint gVertexBuffer = 0;

	//Index buffer
	GLuint gIndexBuffer = 0;

grosso modo c'est comme au chapitre précédent sauf qu'on ajoute cette fois une variable pour gérer des indices
	Dans le chapitre précédent, on utilise la fonction glDrawArrays(), pour dessiner notre forme géométrique à partir des sommets mémorisés. La particularité de cette fonction est qu'elle va dessiner dans l'ordre des indices de la table. Dans ce code on va utiliser une fonction qui peut tracer les sommets dans n'importe quel ordre. Avec cette fonction on peut même effectuer deux fois le rendu d'un même sommet. Ainsi dans cette exemple on a besoin d'une table d'indice pour savoir dans quel ordre on va tracer les sommets (C'est la raison d'être de gIndices).
	On dispose donc d'une table de sommets et d'une table d'indices. L'idée derrière ce chapitre est de les stocker dans le GPU à l'aide d'un buffer. Pour cela on va créer un Buffer Object qui sera lié à nos table via un ID. gVertexBuffer est donc l'ID pour notre table de sommets et gIndexBuffer est l'ID pour notre table des indices.
	
	Dans notre fonction loadMedia(), on va définir nos coordonnées de sommets comme au chapitre précédent, puis on va assigner des valeurs à notre table d'indices. Cette table sera lue dans l'ordre, chacune des valeur représentant un indice, il suffit de changer la valeur associée à ces indices pour tracer notre figure dans un ordre différent.
	Il y a un intérêt à faire ça, en 3D si je veux dessiner un cube, possédant 6 faces multiplié par 4 sommets, j'obtiens 24 sommets pour tracer mon cubes. Sauf que en réalité un cube ne possède que 8 coins différents. Si je veux optimiser mon tracé il me suffit d'indiquer les coordonnées des 8 coins de mon cube pour réussir à le tracer.
	Viens ensuite l'étape d'associer nos tables à un buffer. On va utiliser pour ça trois fonctions en commençant par notre table des sommets:
	- glGenBuffers() va créer le Buffer (on lui passe l'adresse de la variable qui contiendra l'ID en paramètre)
	- Ensuite on va attacher le Buffer via glBindBuffer() en lui indiquant en paramètre que je veux lui passer une table (GL_ARRAY_BUFFER) ainsi que l'ID du Buffer en second paramètre.
	- Enfin on va associer notre table à notre Buffer via glBufferData() en lui passant en paramètre, le fait qu'il s'agisse d'une table (GL_ARRAY_BUFFER), la taille en mémoire de cette table (taille * sizeof(type)), la table en question (son adresse), et un flag qui indique que les données seront envoyée au Buffer une fois mais dessinée plusieurs fois (GL_STATIC_DRAW).
	Pour faire la même chose avec notre table d'index, on va procédé de la même façon à l'aide des fonctions précédentes. On indiquera juste au système qu'il s'agit d'une table d'index (GL_ELEMENT_ARRAY_BUFFER).
	
	Dans la fonction render(), on va faire un clear de l'écran, puis on va activé le rendu du vertex array comme dans le chapitre précédent. Ensuite le rendu va être effectué via notre VBO (Vertex Buffer Object). Pour ça on va appeler glBindBuffer pour attacher notre Buffer, puis on va set nos vertex datas via glVertexPointer (Il va les récupérer directement du Buffer du coup). On souhaite que le rendu commence en prenant les données des sommets depuis le début du VBO, du coup on lui passe l'adresse à NULL dans ce but.
	Ensuite on va vouloir connaître les indices pour savoir dans quel ordre on effectue le rendu de notre figure. On va donc récupérer le buffer de notre IBO via glBindBuffer. Il ne nous reste plus qu'à dessiner le tout.
	Le dessin est effectué par glDrawElements(). La fonction prends en argument : le type de géométrie à tracer, le nombre d'éléments à tracé (en l'occurrence 4 pour un carré), en troisième argument on a le type de données pour nos indices d'IBO (GL_UNSIGNED_INT), le quatrième élément est l'adresse de notre table d'indices. Cette adresse est utilisée via notre IBO qui est actuellement Bind, on souhaite également lire cet IBO depuis le début, c'est pourquoi on utilise comme pour le VBO le NULL indiquant qu'on prend cette table depuis le début.
	De ce que je comprend, on pourrait ne pas bind notre IBO et passer à notre glDrawElements l'adresse de notre table d'index (du coup c'est comme si on travaillait sans Buffer ce qui signifie que les données de sommet sont envoyé au GPU à chaque appel de la fonction render). Il faut savoir que cette méthode de Vertex Array est obsolète dans les version de OpenGL au-dessus de 3.0+.
	Quand le dessin est fait on désactive notre vertex array et on met à jour l'écran.
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/17_vertex_buffer_objects/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LVertexPos2D.h"

//Les sommets d'un Quad
LVertexPos2D gQuadVertices[4];

//Indices des sommets
GLuint gIndices[4];

//Buffer des sommets
GLuint gVertexBuffer = 0;

//Index buffer
GLuint gIndexBuffer = 0;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Mise en place des sommet du carré
	gQuadVertices[0].x = SCREEN_WIDTH * 1.f/ 4.f;
	gQuadVertices[0].y = SCREEN_HEIGHT * 1.f/ 4.f;
	
	gQuadVertices[1].x = SCREEN_WIDTH * 3.f/ 4.f;
	gQuadVertices[1].y = SCREEN_HEIGHT * 1.f/ 4.f;
	
	gQuadVertices[2].x = SCREEN_WIDTH * 3.f/ 4.f;
	gQuadVertices[2].y = SCREEN_HEIGHT * 3.f/ 4.f;
	
	gQuadVertices[3].x = SCREEN_WIDTH * 1.f/ 4.f;
	gQuadVertices[3].y = SCREEN_HEIGHT * 3.f/ 4.f;
	
	//Mise en place des indices de rendu 
	gIndices[0] = 0;
	gIndices[1] = 1;
	gIndices[2] = 2;
	gIndices[3] = 3;
	
	//Création d'un Vertex Buffer Object
	glGenBuffers(1, &gVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, gVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER , 4 * sizeof(LVertexPos2D), gQuadVertices, GL_STATIC_DRAW);
	
	//Création d'un Indices Buffer Object
	glGenBuffers(1, &gIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), gIndices, GL_STATIC_DRAW);
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Activer le vertex array
	glEnableClientState(GL_VERTEX_ARRAY);
	
		//Set vertex data
		glBindBuffer(GL_ARRAY_BUFFER, gVertexBuffer);
		glVertexPointer(2, GL_FLOAT, 0, NULL);
		
		//Dessiner le carré en utilisé les données du vertex data
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIndexBuffer);
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

