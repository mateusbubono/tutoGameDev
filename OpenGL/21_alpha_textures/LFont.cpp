/*
Boris Merminod le 10/04/2019
	
	Ici, on va modifier la fonction loadBitmap, qui va ainsi réaliser une économie d'utilisation de la mémoire lors du chargement de bitmap fonts puisqu'elle va désormais travailler sur 8bits au lieu de 32. La fonction est trsè similaire à la précédente, au début avec un format sur 8bits, les pixels en luminance ont pour représentation de la couleur noire, un octet à 0. Pour charger nos pixels à partir d'un fichier on va changer notre loadPixelsFromFile vers un loadPixelsFromFile8. 
	Le parsing du bitmap font est ensuite sensiblement le même. Après le parsing, on va charger les textures via loadTexturesFromPixels8, générer le VBO, set le font texture wrap et set les variables d'espace. On va cependant éviter d'effectuer un blend sur les pixels qui sont déjà dans un format adapté pour cela.
	
	La fonction renderText ne change pas mais elle travaille désormais en conséquences des modifications sur des pixels à 8bits
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/21_alpha_textures/index.php
*/

#include "LFont.h"

LFont::LFont()
{
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

LFont::~LFont()
{
	freeFont();
}

bool LFont::loadBitmap(std::string path)
{
	//Loading flag
	bool success = true;
	//Background pixel
	const GLubyte BLACK_PIXEL = 0x00;
	
	//Suppression du fond existant 
	freeFont();
	//Image pixels loaded
	if(loadPixelsFromFile8(path))
	{
		//Get cell dimension
		GLfloat cellW = imageWidth() / 16.f;
		GLfloat cellH = imageHeight() / 16.f;
		
		//Get letter bottom and top
		GLuint top = cellH;
		int bottom = 0;
		GLuint aBottom = 0;
		
		//Current pixel coordinates
		int pX = 0;
		int pY = 0;
		
		//Base cell offsets
		int bX = 0;
		int bY = 0;
		
		//Begin parsing bitmap font
		GLuint currentChar = 0;
		LFRect nextClip = {0.f, 0.f, cellW, cellH};
		
		//Go through celle rows
		for(unsigned int rows = 0; rows<16; rows++)
		{
			for(unsigned int cols = 0; cols <16; cols++)
			{
				//Begin cell parsing
				
				//Set Base offset
				bX = cellW * cols;
				bY = cellH * rows;
				
				//Initialize clip
				nextClip.x = cellW * cols;
				nextClip.y = cellH * rows;
				
				nextClip.w = cellW;
				nextClip.h = cellH;
				
				//Find left side of character
				for(int pCol = 0; pCol < cellW; pCol++)
				{
					for(int pRow = 0; pRow < cellH; pRow++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's x offset
							nextClip.x = pX;
							
							//Break the loops
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Right Side
				for(int pCol_w = cellW - 1; pCol_w >= 0; pCol_w--)
				{
					for(int pRow_w = 0; pRow_w < cellH; pRow_w++)
					{
						//Set pixel offset
						pX = bX + pCol_w;
						pY = bY + pRow_w;
						
						//Non_background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's width
							nextClip.w = (pX - nextClip.x) + 1;
							
							//Break the loops
							pCol_w = -1;
							pRow_w = cellH;
						}
					}
				}
				
				//Find top
				for(unsigned int pRow = 0; pRow < cellH; pRow++)
				{
					for(int pCol = 0; pCol < cellW; pCol++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non-background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//New Top found
							if(pRow < top)
								top = pRow;
							
							//Break the loop
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Find bottom
				for(int pRow_b = cellH - 1; pRow_b >= 0; pRow_b--)
				{
					for(int pCol_b = 0; pCol_b < cellW; pCol_b++)
					{
						//Set pixel offset
						pX = bX + pCol_b;
						pY = bY + pRow_b;
						
						//Non-background pixel found
						if(getPixel8(pX, pY) != BLACK_PIXEL)
						{
							//Set BaseLine
							if(currentChar == 'A')
								aBottom = pRow_b;
							
							if(pRow_b > bottom)
								bottom = pRow_b;
							
							//Break the loop
							pCol_b = cellW;
							pRow_b = -1;
						}
					}
				}
				
				//Go to next character
				mClips.push_back(nextClip);
				currentChar++;
			}
		}
		
		//Set top
		for(int t=0; t<256; t++)
		{
			mClips[t].y += top;
			mClips[t].h -= top;
		}
		
		//Create texture from manipulated pixels
		if(loadTextureFromPixels8())
		{
			//Build vertex buffer from sprite sheet data
			if(!generateDataBuffer(LSPRITE_ORIGIN_TOP_LEFT))
			{
				fprintf(stderr, "loadBitmap : Impossible de créer le vertex buffer à partir du bitmap font\n");
				success = false;
			}
		}
		else
		{
			fprintf(stderr, "loadBitmap : Impossible de créer une texture à partir des pixels du bitmap font\n");
			success = false;
		}
		
		//Set font texture wrap
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		
		//Set spacing variables
		mSpace = cellW / 2;
		mNewLine = aBottom - top;
		mLineHeight = bottom - top;
	}
	else
	{
		fprintf(stderr, "loadBitmap : Impossible de charger l'image bitmap font : %s\n", path.c_str());
		success = false;
	}
	
	return success;
}

void LFont::freeFont()
{
	//Get rid of sprite sheet
	freeTexture();
	
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

void LFont::renderText(GLfloat x, GLfloat y, std::string text)
{
	//If there is a texture to render from
	if(getTextureID() != 0)
	{
		//Draw position
		GLfloat dX = x;
		GLfloat dY = y;
		
		//Move to draw position
		glTranslatef(x, y, 0.f);
		
		//Set texture
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		//Bind vertex data
		glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
		
		//Set texture coordinate data
		glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, texCoord));
		
		//Set vertex data
		glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, position));
		
		//Go through string
		for(unsigned int i=0; i<text.length(); i++)
		{
			//Space
			if(text[i] == ' ')
			{
				glTranslatef(mSpace, 0.f, 0.f);
				dX += mSpace;
			}
			//NewLine
			else if(text[i] == '\n')
			{
				glTranslatef(x - dX, mNewLine, 0.f);
				dY += mNewLine;
				dX += x - dX;
			}
			//character
			else
			{
				//Get ASCII
				GLuint ascii = (unsigned char) text[i];
				
				//Draw quad using vertex data and index data
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[ascii]);
				glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
				
				//Move over
				glTranslatef(mClips[ascii].w, 0.f, 0.f);
				dX += mClips[ascii].w;
			}
		}
		
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		
	}
}
