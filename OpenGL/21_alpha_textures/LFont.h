/*
Boris Merminod le 10/04/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/21_alpha_textures/index.php
*/

#include "LSpriteSheet.h"

class LFont: private LSpriteSheet
{
	public :
		LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Initialisation de spacing variables
		Side Effects :
		- None
		*/
		
		~LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer des Fonts
		Side Effects :
		- None
		*/
		
		bool loadBitmap(std::string path);
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Chargement de fonts bitmap
		- Retourne true s'il n'y a pas d'erreurs
		- Reporting des erreurs sur la console
		Side Effects :
		- None
		*/
		
		void freeFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer les textures et les sprites sheet du bitmap font
		Side Effects :
		- None
		*/
				
		void renderText(GLfloat x, GLfloat y, std::string text);
		/*
		Pré-conditions :
		- un font chargé
		Post conditions :
		- Rendu de textes
		Side Effects :
		- Binds la texture membre ainsi que les data buffers
		*/
		
	private :
		//Spacing variables
		GLfloat mSpace;
		GLfloat mLineHeight;
		GLfloat mNewLine;

};
