/*
Boris Merminod le 10/04/2019

	On commence ici par changer les anciennes fonction présente en leur ajoutant le suffixe 32 pour indiquer qu'elles travailles sur des pixels de 32 bits.
	
	Ensuite on va implémenter loadPixelsFromFile8 qui va fonctionner selon le même principe que son analogue en 32bits, hormis quelques différences clés. 
	Elle va allouer pour mPixels8 un tableau de GLubyte. Elle va aussi utiliser le format de pixel prédéfini vers GL_ALPHA. La copie de pixels rélaisé par memcpy se fait octet par octet ce qui permet d'éviter une multiplication par 4 ce que l'on fait pour des données RGBA :
	
	 memcpy( mPixels8, ilGetData(), size );

Ce qui change également c'est la manière dont DevIL va charger les données de pixels. désormais les pixels sont converti en luminance 

	success = ilConvertImage( IL_LUMINANCE, IL_UNSIGNED_BYTE );

Les pixels en luminance fonctionne sur un seul octet indiquant grosso modo leur intensité de brillance pour donner un effet lissé du texte.

	Il nous faut ensuite implémenter loadTextureFromPixels8. En créant une texture alpha, il nous faut au préalable indiquer que le format de pixel utilisé est GL_ALPHA au lieu de GL_RGBA. Les pixels sont ensuite envoyé comme ceci :
	
	 glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, mTextureWidth, mTextureHeight, 0, GL_ALPHA, GL_UNSIGNED_BYTE, mPixels8 );
	 
	 Dans la fonction freeTexture, on rajoute la suppression de la table mPixels8
	 
	 Dans la fonction lock on va s'assurer que la texture n'est pas déjà vérrouillée. ensuite on va proprement allouer la mémoire pour chaque pixel et obtenir les données de pixels depuis la texture.
	 
	glGetTexImage( GL_TEXTURE_2D, 0, mPixelFormat, GL_UNSIGNED_BYTE, mPixels32 );
	
Ensuite la fonction glGetTexImage va mettre en place le format de pixel pour l'utiliser proprement (par contre je n'ai pas compris pourquoi la table de pixels utilisée est forcément celle sur 32 bits

	Dans la fonction unlock, on va mettre à jour la texture. Pour ça on veut être sûr d'avoir des pixels que l'on peut mettre à jour avec une texture que l'on peut mettre à jour :
	
	 if( ( mPixels32 != NULL || mPixels8 != NULL ) && mTextureID != 0 )
	 
Quand on s'est assuré de ça on va attacher la texture et sélectionner le type de pixels que nous allons utilisé (32 ou 8 bits) grâce à un terner :

	void* pixels = ( mPixelFormat == GL_RGBA ) ? (void*)mPixels32 : (void*)mPixels8; glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, mPixelFormat, GL_UNSIGNED_BYTE, pixels );
	
Ainsi on sélectionne le type de pixel en fonction du format de pixels choisi. Ensuite on met à jour notre texture :

	glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, mPixelFormat, GL_UNSIGNED_BYTE, pixels );
	
puis on va libérer la mémoire allouée pour gérer les données de pixels et détacher la texture

	Les accesseurs et mutateurs vont chacun travailler sur une table de pixels différente soit sur 32 soit sur 8 bits

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/21_alpha_textures/index.php
*/

//Déclenche un bug d'une fonction OpenGL dans loadTextureFromPixels8
#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LVertexData2D.h"
//#include <il.h>

GLenum DEFAULT_TEXTURE_WRAP = GL_REPEAT;

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels32 = NULL;
	mPixels8 = NULL; 
	mPixelFormat = 0;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
	
	//Initialisation du VBO
	mVBOID = 0;
	mIBOID = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
	
	freeVBO();
}

bool LTexture::loadTextureFromFile32(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadPixelsFromFile32(std::string path)
{
	//Libération de la mémoire allouée éventuelle pour d'autre texture
	freeTexture();

	//Flag de chargement de texture
	bool pixelsLoaded = false;

	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());

	//Si l'image s'est chargée comme il faut 
	if(success == IL_TRUE)
	{
		//Conversion de l'image vers le RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requise de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une mauvaise taille
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Place l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);

				//Redimensionne l'image
				iluEnlargeCanvas((int) texWidth, (int) texHeight, 1);
			}

			//Allocation de mémoire pour les données de texture
			GLuint size = texWidth * texHeight;
			mPixels32 = new GLuint[size];

			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;

			//Copier les pixels en mémoire
			memcpy(mPixels32, ilGetData(), size * 4);
			pixelsLoaded = true;
		}

		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
	}

	//Report des erreurs
	if(!pixelsLoaded)
		fprintf(stderr, "Impossible de charger %s\n", path.c_str());
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromFileWithColorKey32(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{
	//Charger les pixels
	if(!loadPixelsFromFile32(path))
		return false;
	
	//Parcourir les pixels
	GLuint size = mTextureWidth * mTextureHeight;
	for(unsigned int i = 0; i < size; i++)
	{
		// Récupérer la couleur du pixel
		GLubyte* colors = (GLubyte*)&mPixels32[i];

		//Si la couleur correspond
		if(colors[0] == r && colors[1] == g && colors[2] == b && (0 == a || colors[3] == a))
		{
			//Ajuster la transparence
			colors[0] = 255;
			colors[1] = 255;
			colors[2] = 255;
			colors[3] = 000;
		}
	}

	//Créer la texture
	return loadTextureFromPixels32();
}

bool LTexture::loadTextureFromPixels32()
{
	//Flag de succès de la fonction
	bool success = true;

	//Si l'attribut pixels est chargé
	if(mTextureID == 0 && mPixels32 != NULL)
	{
		//Générer une ID de texture
		glGenTextures(1, &mTextureID);

		//Attacher la textureID
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels32);

		//Mise en place des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur de chargement de %p pixels ! %s\n", mPixels32, gluErrorString(error));
			success = false;
		}
		else 
		{
			//Suppression de l'attribut pixels
			delete [] mPixels32;
			mPixels32 = NULL;
			
			//Generate VBO
			initVBO();
		}
	}
	// En cas d'erreur
	else 
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Structure mPixels non chargé, impossible de créer la texture !\n");

		//La texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La texture existe déjà \n");
		else if(mPixels32 == NULL)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La structure mPixels n'est pas initialisée\n");
		
		success = false;
	}

	return success;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	//Générer le VBO
	initVBO();
	
	//Set pixels format
	mPixelFormat = GL_RGBA;
	
	return true;
}

bool LTexture::loadPixelsFromFile8(std::string path)
{
	//Libérer la précédente texture si besoin
	freeTexture();
	
	//booléen sur la réussite du chargement de la texture
	bool pixelsLoaded = false;
	
	//Générer l'image et lui attacher un ID
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est chargée avec succès
	if(success == IL_TRUE)
	{
		success = ilConvertImage(IL_LUMINANCE, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialise les dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
			
			//Calculer les dimensions optimales de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);
			
			//Si la texture n'est pas aux bonnes dimensions
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				
				//Redimensionner l'image
				iluEnlargeCanvas((int)texWidth, (int) texHeight, 1);
			}
			
			//Allocation de mémoire pour les texture data
			GLuint size = texWidth * texHeight;
			mPixels8 = new GLubyte[size];
			
			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;
			
			memcpy(mPixels8, ilGetData(), size);
			pixelsLoaded = true;
		}
		
		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
		
		//Set du format de pixel
		mPixelFormat = GL_ALPHA;
	}
	
	//Reporting des erreurs
	if(!pixelsLoaded)
	{
		fprintf(stderr, "loadPixelsFromFile8 : Impossible de charger %s\n", path.c_str());
	}
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromPixels8()
{
	//Flag de chargement
	bool success = true;
	
	//Pixels déjà chargé
	if(mTextureID == 0 && mPixels8 != NULL)
	{
		//Générer une Texture ID
		glGenTextures(1, &mTextureID);
		
		//Attacher la texture à cette ID
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, mTextureWidth, mTextureHeight, 0, GL_ALPHA, GL_UNSIGNED_BYTE, mPixels8);
		
		//Setting des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, DEFAULT_TEXTURE_WRAP);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, DEFAULT_TEXTURE_WRAP);
		
		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);
		
		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "loadTextureFromPixels8 : Erreur de chargement de texture à partir de %p pixels ! %s\n", mPixels8, gluErrorString(error));
			success = false;
		}
		else
		{
			//Libérer les pixels
			delete[] mPixels8;
			mPixels8 = NULL;
			
			//Générer le VBO
			initVBO();
			
			//Setting du format de pixel
			mPixelFormat = GL_ALPHA;
		}
	}
	else // erreur
	{
		fprintf(stderr, "loadTextureFromPixels8 : Impossible de charger la texture à partir des pixels! ");
		
		//Si la texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "Une texture est déjà chargée\n");
		else if(mPixels8 == NULL)
			fprintf(stderr, "Pas de pixels disponible pour créer une texture ! \n");
	}
	
	return success;
}


void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels 32 bits
	if(mPixels32 != NULL)
	{
		delete [] mPixels32;
		mPixels32 = NULL;
	}
	
	//Suppression des pixels de 8bits
	if(mPixels8 != NULL)
	{
		delete [] mPixels8;
		mPixels8 = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
	
	//Réinitialiser le format des pixels
	mPixelFormat = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels32 == NULL && mPixels8 == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		
		if(mPixelFormat == GL_RGBA)
		{
			mPixels32 = new GLuint[size];
		}
		else if(mPixelFormat == GL_ALPHA)
		{
			mPixels8 = new GLubyte[size];
		}

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, mPixelFormat, GL_UNSIGNED_BYTE, mPixels32);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if((mPixels32 != NULL || mPixels8 != NULL) && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		void * pixels = (mPixelFormat == GL_RGBA) ? (void*)mPixels32 : (void*)mPixels8;
		
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, mPixelFormat, GL_UNSIGNED_BYTE, pixels);

		//Suppression de la structure pixels
		if(mPixels32 != NULL)
		{
			delete [] mPixels32;
			mPixels32 = NULL;
		}
		if(mPixels8 != NULL)
		{
			delete[] mPixels8;
			mPixels8 = NULL;
		}

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels32;
}

GLubyte * LTexture::getPixelData8()
{
	return mPixels8;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels32[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels32[y * mTextureWidth + x] = pixel;
}

GLubyte LTexture::getPixel8(GLuint x, GLuint y)
{
	return mPixels8[y * mTextureWidth + x];
}


void LTexture::setPixel8(GLuint x, GLuint y, GLubyte pixel)
{
	mPixels8[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{	
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Set vertex data
		LVertexData2D vData[4];
		
		//Texture coordinates
		vData[0].texCoord.s = texLeft; vData[0].texCoord.t = texTop;
		vData[1].texCoord.s = texRight; vData[1].texCoord.t = texTop;
		vData[2].texCoord.s = texRight; vData[2].texCoord.t = texBottom;
		vData[3].texCoord.s = texLeft; vData[3].texCoord.t = texBottom;
		
		//Vertex positions
		vData[0].position.x = 0.0f; vData[0].position.y = 0.0f;
		vData[1].position.x = quadWidth; vData[1].position.y = 0.0f;
		vData[2].position.x = quadWidth; vData[2].position.y = quadHeight;
		vData[3].position.x = 0.0f; vData[3].position.y = quadHeight;
		
		//Set texture ID
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
			//Bind Vertex Buffer
			glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
			
			//Update vertex buffer data
			glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(LVertexData2D), vData);
			
			//Set texture coordinate data
			glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, texCoord));
			
			//Set vertex data
			glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, position));
			
			//Draw quad using vertex data and index date
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
			
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}

void LTexture::initVBO()
{
	//Si la texture est déjà chargée et que le VBO existe déjà
	if(mTextureID == 0 || mVBOID != 0)
		return;
		
	LVertexData2D vData[4];
	GLuint iData[4];
	
	//Set rendering indices
	iData[0] = 0;
	iData[1] = 1;
	iData[2] = 2;
	iData[3] = 3;
	
	//Création du VBO
	glGenBuffers(1, &mVBOID);
	glBindBuffer(GL_ARRAY_BUFFER, mVBOID);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(LVertexData2D), vData, GL_DYNAMIC_DRAW);
	
	//Création de l'IBO
	glGenBuffers(1, &mIBOID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBOID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), iData, GL_DYNAMIC_DRAW);
	
	//Détacher les buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void LTexture::freeVBO()
{
	if(mVBOID == 0)
		return;
	
	glDeleteBuffers(1, &mVBOID);
	glDeleteBuffers(1, &mIBOID);
	
}
