/*
	Boris Merminod le 10/04/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/21_alpha_textures/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
