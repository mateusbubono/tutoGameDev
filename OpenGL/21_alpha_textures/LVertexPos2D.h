/*
Boris Merminod le 10/04/2019


Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/21_alpha_textures/index.php
*/

#ifndef LVERTEX_POS_2D_H
#define LVERTEX_POS_2D_H

#include "LOpenGL.h"

struct LVertexPos2D
{
	GLfloat x;
	GLfloat y;
};

#endif
