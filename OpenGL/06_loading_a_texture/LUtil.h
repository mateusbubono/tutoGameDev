/*
	Boris Merminod le 17/05/2018
	
	On ajoute ici la fonction loadMedia qui va charger les pixels pour notre texture
	
	code source tiré de : http://lazyfoo.net/tutorials/OpenGL/06_loading_a_texture/index.php
*/

#ifndef LUTIL_H
#define LUTIL_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>

//Les constantes d'affichage
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

bool initGL();
/*
Pre Condition:
 -Un contexte OpenGL valide
Post Condition:
 - Initialisation des matrice et des couleurs 
 - Affichage des erreurs OpenGL à la console
 - Retourne false en cas d'erreur OpenGL
Side Effects:
 - La matrice de projection est initialisée en matrice d'identité
 - La matrice Modelview est initialisée en matrice d'identité
 - Le mode matrice est initialisée en modelview
 - La couleur est initialisée en noir 
*/

bool loadMedia();
/*
Pre Condition :
- Un contexte OpenGL valide
Post Condition :
- Chargement des médias à utiliser dans le programme
- Report des erreurs à la sortie stderr
- retourne true si le/les média(s) sont chargés avec succès
Side Effects :
- None
*/

void update();
/*
Pre Condition:
 -None
Post Condition:
 -Does per frame logic
Side Effects:
 -None
*/

void render();
/*
Pre Condition:
 - Un contexte OpenGL valide
 - Une matrice modelview active
Post Condition:
 - Rendu de la scène
Side Effects:
 -Vide le buffer de couleur
 -Swaps le front/back buffer
 - Translation de la matrice modelview au centre de l'écran par défaut
 - Change la couleur de rendu actuelle
 - Change le viewport actuel
*/

#endif
