/*
	Boris Merminod le 17/05/2018
	
	La fonction loadFromFile a été ajoutée ici, elle prend en paramètre un std::string vers le chemin d'accès pour ouvrir le fichier image. La fonction commence par initialisé un flag de chargement de texture.
	DevIL fonctionne selon le même principe que OpenGL comme une state machine. On va déclarer une ID pour notre image de type ILuint puis on va générer cette ID via ILGenImages. Enfin on va attacher l'ID à l'image courante via la fonction ILGenImages. 
	Le chargement de l'image vient ensuite vie ilLoadImage(). Le retour de cette fonction est du type ILboolean on va le comparer à IL_TRUE pour savoir si le chargement de l'image s'est bien passé. Si c'est le cas la fonction ilConvertImage va convertir l'image chargée au format RGBA, on s'assure que les pixels data seront bien selon se format.
	Par la suite on va passer les pixels data de notre image à la méthode loadTextureFromPixels32. La fonction (GLuint*)ilGetData() va permettre de récupérer les pixels data de notre image. (GLuint)ilGetInteger( IL_IMAGE_WIDTH ) permet de récupérer la largeur de l'image et (GLuint)ilGetInteger( IL_IMAGE_HEIGHT ) permet dans récupérer la hauteur. 
	L'image va ainsi se retrouvée chargée en mémoire de OpenGL. Elle va donc être supprimée de la mémoire de DevIL via la fonction ilDeleteImages(). Ensuite on va faire un report des erreurs avant de retourner notre success flag

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/06_loading_a_texture/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), (GLuint)ilGetInteger(IL_IMAGE_WIDTH), (GLuint)ilGetInteger(IL_IMAGE_HEIGHT));
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mTextureWidth = width;
	mTextureHeight = height;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}
	
	mTextureWidth = 0;
	mTextureHeight = 0;
}

void LTexture::render(GLfloat x, GLfloat y)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(0.f,0.f); glVertex2f(0.f,0.f);
			glTexCoord2f(1.f,0.f); glVertex2f(mTextureWidth,0.f);
			glTexCoord2f(1.f,1.f); glVertex2f(mTextureWidth,mTextureHeight);
			glTexCoord2f(0.f,1.f); glVertex2f(0.f,mTextureHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}
