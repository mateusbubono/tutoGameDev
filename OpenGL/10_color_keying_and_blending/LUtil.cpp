/*
Boris merminod le 20/07/2018

Pour pouvoir travailler sur le color keying et le blending, nous allons devoir effectuer quelques initialisations. Le tout va être réalisé dans la fonction initGL(). On commence par rendre le blending possible via glEnable(GL_BLEND). On va ensuite désactiver le Depth testing via glDisable(GL_DEPTH_TEST). Le Depth testing est une option qui est utile pour les application OpenGL en 3D ou l'on va effectuer le rendu de quelque chose puis le rendu d'une seconde chose qui est derrière. Ainsi si un objet est derrirère un autre, ces polygones ne seront pas rendu puisqu'un test de profondeur le depth testing des deux objets, sera effectué au préalable. D'après le tuto mélanger l'option Blending et Depth testing donne des résultat funky...
On va ensuite mettre en place les options de notre Blendmode grâce à la fonction glBlenFunc() :

glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

La fonction prend en argument : comment nos pixels source vont être "factored" et en second argument quels sont les pixels de destinations... Pour l'argument source, la valeur alpha de cette source va être utilisée. Pour la destination la valeur sera à 1. glBlendFunc() utilise une échelle de couleur de 0 à 1 au lieu de 0 à 255.
Si nous devons effectuer le rendu de quelque chose avec une valeur alpha à 1, alors 100% de la couleur source va être rendue transparente et par conséquent 1 - 1 = 0% de la couleur de destination va être rendue transparente. Cela est un peu compliquer il suffit juste de se dire qu'avec un alpha à 1 la couleur sera complètement opaque et à 0 elle sera transparente.

Lors du chargement de notre texture par la fonction loadMedia(), on va faire en sorte que la couleur de fond le cyan soit rendue transparente.

Dans notre fonction render() nous allons utiliser la fonction glColor4f() pour rendre la couleur blanche de notre texture à moitié transparente : 
glColor4f( 1.f, 1.f, 1.f, 0.5f );
Pour ça on utilise le 4ème paramètre l'alpha que l'on positionne à 0.5f soit 50% d'opacité.


code source tiré de : http://lazyfoo.net/tutorials/OpenGL/10_color_keying_and_blending/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Image circulaire
LTexture gCircleTexture;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Initialisation de la clear color
	//glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gCircleTexture.loadTextureFromFileWithColorKey("sprite/circle.png", 000, 255, 255))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des textures
	glColor4f(1.f, 1.f, 1.f, 0.5f);
	gCircleTexture.render((SCREEN_WIDTH - gCircleTexture.imageWidth())/2.f, (SCREEN_HEIGHT - gCircleTexture.imageHeight())/2.f );
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
