/*
	Boris Merminod le  20/07/2018

	On ajoute ici la fonction loadPixelsFromFile() dont le corps est très similaire à la fonction loadTextureFromFile(). On pourra juste constater un appel à freeTexture() au début de l'implémentation de la fonction.
	La différence avec loadTextureFromFile() est que nous n'avons pas besoin de créer une texture. A la place nous allons allouer de la mémoire pour copier les données des pixels dans l'attribut mPixels via l'utilisation de la fonction memcpy() :

	memcpy(mPixels, ilGetData(), size * 4);

	Le premier argument est la stucture qui va recevoir la copie des donnée. Le second est quelle données allons nous copier ? Le troisième est la taille des données en octet. Nous avons ici 4 octets par pixels que l'on multiplie par le nombre de pixels pour avoir la valeur de cet élément

	Par la suite nous avons une surcharge de la fonction loadTextureFromPixel32() avec quelques différences. La première est que la fonction va utiliser l'attribut mPixels de la classe LTexture pour créer la texture. La seconde est que la fonction ne fait pas d'appel à freeTexture(). La raison est que cela n'a pas de sens de se débarrasser des données de pixels avant de créer une texture à partir des données de pixels
	Cette version de loadTextureFromPixels32() peut cependant échouer s'il y a déjà une texture existante ou s'il n'y a pas de mémoire allouée pour mPixels

	Il nous reste la fonction loadTextureFromFileWithColorKey(). Une fois que les pixels de la texture sont récupéré il suffit de les parcourir et de rendre transparent ceux qui répondent à une condition voulue (par exemple si le pixel est magenta et que je veux rendre le magenta transparent). Une fois la transparence gérée on va créer la texture à partir des données de pixels modifiés.



Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/10_color_keying_and_blending/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	mPixels = NULL;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadPixelsFromFile(std::string path)
{
	//Libération de la mémoire allouée éventuelle pour d'autre texture
	freeTexture();

	//Flag de chargement de texture
	bool pixelsLoaded = false;

	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	//Chargement de l'image
	ILboolean success = ilLoadImage(path.c_str());

	//Si l'image s'est chargée comme il faut 
	if(success == IL_TRUE)
	{
		//Conversion de l'image vers le RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions de l'image
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requise de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une mauvaise taille
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Place l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);

				//Redimensionne l'image
				iluEnlargeCanvas((int) texWidth, (int) texHeight, 1);
			}

			//Allocation de mémoire pour les données de texture
			GLuint size = texWidth * texHeight;
			mPixels = new GLuint[size];

			//Récupérer les dimensions de l'image
			mImageWidth = imgWidth;
			mImageHeight = imgHeight;
			mTextureWidth = texWidth;
			mTextureHeight = texHeight;

			//Copier les pixels en mémoire
			memcpy(mPixels, ilGetData(), size * 4);
			pixelsLoaded = true;
		}

		//Suppression du fichier en mémoire
		ilDeleteImages(1, &imgID);
	}

	//Report des erreurs
	if(!pixelsLoaded)
		fprintf(stderr, "Impossible de charger %s\n", path.c_str());
	
	return pixelsLoaded;
}

bool LTexture::loadTextureFromFileWithColorKey(std::string path, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{
	//Charger les pixels
	if(!loadPixelsFromFile(path))
		return false;
	
	//Parcourir les pixels
	GLuint size = mTextureWidth * mTextureHeight;
	for(unsigned int i = 0; i < size; i++)
	{
		// Récupérer la couleur du pixel
		GLubyte* colors = (GLubyte*)&mPixels[i];

		//Si la couleur correspond
		if(colors[0] == r && colors[1] == g && colors[2] == b && (0 == a || colors[3] == a))
		{
			//Ajuster la transparence
			colors[0] = 255;
			colors[1] = 255;
			colors[2] = 255;
			colors[3] = 000;
		}
	}

	//Créer la texture
	return loadTextureFromPixels32();
}

bool LTexture::loadTextureFromPixels32()
{
	//Flag de succès de la fonction
	bool success = true;

	//Si l'attribut pixels est chargé
	if(mTextureID == 0 && mPixels != NULL)
	{
		//Générer une ID de texture
		glGenTextures(1, &mTextureID);

		//Attacher la textureID
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Générer la texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Mise en place des paramètres de la texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		//Check des erreurs
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur de chargement de %p pixels ! %s\n", mPixels, gluErrorString(error));
			success = false;
		}
		else 
		{
			//Suppression de l'attribut pixels
			delete [] mPixels;
			mPixels = NULL;
		}
	}
	// En cas d'erreur
	else 
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Structure mPixels non chargé, impossible de créer la texture !\n");

		//La texture existe déjà
		if(mTextureID != 0)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La texture existe déjà \n");
		else if(mPixels == NULL)
			fprintf(stderr, "LTexture::loadTextureFromPixels32 : La structure mPixels n'est pas initialisée\n");
		
		success = false;
	}

	return success;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}

	//Suppression des pixels
	if(mPixels != NULL)
	{
		delete [] mPixels;
		mPixels = NULL;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
}

bool LTexture::lock()
{
	// Si la texture n'est pas verrouillée et que cette même texture existe
	if(mPixels == NULL && mTextureID != 0)
	{
		//Allocation de mémoire pour les données de texture
		GLuint size = mTextureWidth * mTextureHeight;
		mPixels = new GLuint[size];

		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Récupérer les pixels
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

bool LTexture::unlock()
{
	//Si la texture est verrouillée et qu'elle existe
	if(mPixels != NULL && mTextureID != 0)
	{
		//Mise en place de la texture courante
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		//Mise à jour de la texture
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mTextureWidth, mTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, mPixels);

		//Suppression de la structure pixels
		delete [] mPixels;
		mPixels = NULL;

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint * LTexture::getPixelData32()
{
	return mPixels;
}

GLuint LTexture::getPixel32(GLuint x, GLuint y)
{
	return mPixels[y * mTextureWidth + x];
}

void LTexture::setPixel32(GLuint x, GLuint y, GLuint pixel)
{
	mPixels[y * mTextureWidth + x] = pixel;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(0.f,0.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth,0.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth,quadHeight);
			glTexCoord2f(texLeft,texBottom); glVertex2f(0.f,quadHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}
