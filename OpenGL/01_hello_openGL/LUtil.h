/*
	Boris Merminod le
	
	On va déclarer ici les constantes ainsi que quelques prototypes de fonctions. On précise pour chaque fonction en commentaires:
	- Les pré-conditions : Les conditions qui doivent être vraie avant d'exécuter la fonction
	- Les post-conditions : Les conditions qui doivent être vraie après l'éxécution de la fonction
	- Les Side Effects : Les changement qu'effectue la fonction en dehors de sa classe
	
	code source tiré de : http://lazyfoo.net/tutorials/OpenGL/01_hello_opengl/index2.php
*/

#ifndef LUTIL_H
#define LUTIL_H

#include "LOpenGL.h"
#include <stdio.h>

//Les constantes d'affichage
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

bool initGL();
/*
Pre Condition:
 -Un contexte OpenGL valide
Post Condition:
 - Initialisation des matrice et des couleurs 
 - Affichage des erreurs OpenGL à la console
 - Retourne false en cas d'erreur OpenGL
Side Effects:
 - La matrice de projection est initialisée en matrice d'identité
 - La matrice Modelview est initialisée en matrice d'identité
 - Le mode matrice est initialisée en modelview
 - La couleur est initialisée en noir 
*/

void update();
/*
Pre Condition:
 -None
Post Condition:
 -Does per frame logic
Side Effects:
 -None
*/

void render();
/*
Pre Condition:
 - Un contexte OpenGL valide
 - Une matrice modelview active
Post Condition:
 - Rendu de la scène
Side Effects:
 -Vide le buffer de couleur
 -Swaps le front/back buffer
*/

#endif
