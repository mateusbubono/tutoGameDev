/*
Boris merminod le 

Ce fichier contient trois fonctions : 

	initGL est une fonction d'initialisation qui a besoin qu'un contexte OpenGL soit actif. Un contexte OpenGL est quelque chose qui contient des informations OpenGL sur les textures, les shaders, les variable state machine, etc... Ce contexte sera crée avec l'initialisation de OpenGL dans la fonction main.
	La fonction va initialiser la matrice de projection et la matrice modelview. Ces deux matrices possèdent des données géométriques qui seront multipliés (entre elles je crois) avant que le rendu ne soit effectué. Ces deux matrices sont initialisées en matrice d'identité ainsi peut importe la géométrie qui est envoyée elle sera la même que celle qui sera rendue puisque la matrice d'identité donnera le même vecteur lors de la multiplication des matrices.
		La fonction glMatrixMode indique que l'on souhaite que GL_PROJECTION soit la matrice courante soit la matrice de projection. La fonction glLoadIdentity va charger la matrice d'identité dans la matrice courante. Le même principe est utilisé pour le texturing, on attache une texture courante et les opérations sont ensuites effectuées sur cette matrice courante. On appelle ça the OpenGL State Machine car une variable d'état est initialisé et les opérations sont réalisés sur l'état en cours. 
		Une fois que la matrice de projection est initialisée, on met en place en matrice courante la matrice modelview. Puis on l'initialise également en matrice d'identité.
		La fonction glClearColor permet de définir une couleur de clear, c'est à dire une couleur qui va remplir tout l'écran à chaque appel de la fonction glClear. Les arguments qu'elle prend sont rouge, vert, bleu et alpha. Les paramètres au lieu de donner une couleur entre 0 et 255 donne une couleur entre 0.0 et 1.0 (donc en float).
		Lorsque tout est initialisé on contrôle d'éventuelle erreur grâce à glGetError(). Cette fonction retourne un code erreur qui peut être traduit en string via la fonction gluErrorString.
		
	La fonction update est vide pour l'instant et sera utilisée plus tard

	La fonction render va effectuer le rendu de tout ce que l'on souhaite afficher. Elle commence par appeler la fonction glClear() avec la couleur définie grâce à glColorClear. Le flag GL_COLOR_BUFFER_BIT indique que le color buffer est simplement les pixels qui sont dessiné à l'écran.
	La fonction continue avec les instructions suivante :
	//Rendu d'un rectangle
	glBegin(GL_QUADS);
		glVertex2f(-0.5f, -0.5f);
		glVertex2f(0.5f, -0.5f);
		glVertex2f(0.5f, 0.5f);
		glVertex2f(-0.5f, 0.5f);
	glEnd();
On indique que l'on souhaite dessiner un rectangle à l'aide de glBegin(). Ensuite on indique les sommets à dessiner. L'ordre des sommets à une importance le premier et le deuxième indique le premier côté, le second et le deuxième indique le côté suivant, le troisième et le quatrième encore le suivant. Le dernier point est relié au premier pour terminer le rectangle. On peut tracer une figure géométrique dans le sens des aiguille du montre ou son inverse. La fonction glVertex2f prend en paramètre les coordonnées du sommet en x et y via des float. Si je voulais faire de la 3D il me suffirait d'appeler glVertex3f et de rajouter une coordonnée dans l'axe z. glEnd(); termine le rendu, à noté que je pourrai dessiner un second rectangle en indiquant simplement 8 sommets entre glBegin et glEnd
	La fonction se termine avec glSwapBuffers(). On utilise deux buffers pour effectuer le rendu, le premier est celui qui est visible par l'utilisateur et le second est dans la mémoir. Quand j'effectue un rendu j'utilise le second buffer puis je swap les buffers, ce qui rend les animations de rendu plus fluides.
 
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/01_hello_opengl/index2.php
*/

#include "LUtil.h"

bool initGL()
{
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu d'un rectangle
	glBegin(GL_QUADS);
		glVertex2f(-0.5f, -0.5f);
		glVertex2f(0.5f, -0.5f);
		glVertex2f(0.5f, 0.5f);
		glVertex2f(-0.5f, 0.5f);
	glEnd();
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
