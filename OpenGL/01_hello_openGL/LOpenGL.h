/*
	Boris Merminod le 04/03/2018
	
	Ce fichier contient l'ensemble des bibliothèque à inclure pour le fonctionnement de OpenGL
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/01_hello_opengl/index2.php
*/

#ifndef LOPENGL_H
#define LOPENGL_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

#endif
