/*
Boris Merminod le 05/03/2018

	Ce fichier contient une fonction main et une fonction de boucle principale qui va être appelé en callback. 
	
	La fonction main commence par initialiser freeGLUT via la fonction glutInit().La fonction glutInitContextVersion va charger le contexte openGL qui va dépendre de la version de celle-ci. Ici on indique la version 2.1 en paramètre.
	La fonction glutInitDisplayMode va initialiser un double buffer via le flag GLUT_DOUBLE. Ensuite la fonction glutInitWindowSize va prendre en compte les dimension de la fenêtre voulue. Enfin on construit notre fenêtre en utilisant glutCreateWindow qui prend en paramètre le titre à afficher de la fenêtre.
	Après avoir charger le contexte on va appeler la fonction initGL qui va échouer si un problème est survenu à l'initialisation de la bibliothèque.
	La fonction glutDisplayFunc va indiquer que notre fonction render sera le callback utilisé pour effectuer le rendu visuel. Ensuite la fonction glutTimerFunc va définir en callback une fonction de boucle principale runMainLoop qui va être appelé à un temps fixe de  1000 / SCREEN_FPS.Enfin la fonction glutMainLoop() va démarrer la boucle principale de freeGLUT. On dispose donc de deux boucles principale, une du système et une  définie par nos soins qui tournera en parallèle.
	
	La fonction runMainLoop, va appeler la fonction update et render à la suite. Puis elle va se rappeler elle-même en callback avant de se terminer via la fonction glutTimerFunc. C'est une sorte de boucle de callback récursif.
	
	Petite précision les coordonnées utilisé via openGL sont normalisés ce qui veut dire que dans la fenêtre le point 0,0 est au centre x = -1 représente le point 0 de la fenêtre, x = 1 représente le point à 640, y = -1 représente le point 0 de la fenêtre et le point y = 1 représente le point à 480
	
Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/01_hello_opengl/index2.php

*/

#include "LUtil.h"

/*
Pre-conditions :
	- freeGLUT est initialisé
Post-conditions :
	- Appeler la fonction main loop et l'initialisée pour qu'elle soient appelée en call back toute les 1000 / SCREEN_FPS ms.
Side Effects :
	-Initialisation de la fonction glutTimerFunc
*/

void runMainLoop(int val)
{
	//Frame Logic
	update();
	render();
	
	//Run frame one more time
	glutTimerFunc(1000 / SCREEN_FPS, runMainLoop, val);
}

int main(int argc, char * args[])
{
	//Initialisation de FreeGLUT
	glutInit(&argc, args);
	
	//Création du contexte OpenGL 2.1 : Si ça crash ici il suffit de commenter cette ligne et se référer au lien vers le site lazyfoo pour des explications
	glutInitContextVersion(2,1);
	
	//Création d'une fenêtre en double buffer
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("OpenGL");
	
	//Initialisation post création window/context 
	if(!initGL())
	{
		fprintf(stderr, "Unable to initialize graphics library !\n");
		return EXIT_FAILURE;
	}
	
	//Initialise la fonction de rendu
	glutDisplayFunc(render);
	
	//Initialise main loop
	glutTimerFunc(1000 / SCREEN_FPS, runMainLoop, 0);
	
	//Démarrage du GLUT main loop
	glutMainLoop();
	
	return EXIT_SUCCESS;
	
}
