/*
Boris merminod le 10/01/2019

	On commence ici par déclarer trois variables globales 

//Texture d'une flèche
LTexture gRotatingTexture;

//Angle de rotation
GLfloat gAngle = 0.f;

//Etat de la trnsformation
int gTransformationCombo = 0;

On a donc notre texture qui va subir des transformations, l'angle de rotation de la texture et l'état de transformation de la texture. Dans notre exemple, nous allons utiliser un ensembles de plusieurs matrices de transformations. La variable glTransformationCombo est là pour indiquer quelle transformations courante nous utilisons.

	La fonction loadMedia() va charger notre texture et la fonction update va mettre à jour l'angle de rotation.
	
	Dans notre fonction render(), on commence par clear notre Buffer via glClear, puis nous chargeons notre matrice d'identité via glLoadIdentity(). Ensuite viens le switch qui va nous aider à expérimenter plusieurs type de transformation :
	
	case 0 :
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glScalef(2.f, 2.f, 0.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			break;

Voici quelques explications :
- Quand je démarre ma texture se trouve dans le coin supérieur gauche de l'écran à l'origine, c'est normal puisque c'est ici que je l'ai chargée
- En même temps ma matrice de transformation se coordonne aussi à l'origine, ma texture est câlé par rapport à cette matrice
- Dans un premier temps je fais un glTranslatef vers le centre de l'écran, alors le coin supérieur gauche de ma texture se retrouve au centre de mon écran.
- Ensuite je fais un glRotatef dont l'angle va dépendre de glAngle. ma matrice va tourner celon une certaine valeur ainsi que ma texture calquée sur cette matrice
- glScalef va doubler la taille de ma matrice, ma texture est alors grossie
- On souhaite que la texture tourne par rapport à son centre c'est pourquoi j'utilise un second glTranslatef pour câler la matrice sur le centre de ma texture
- Le liens LazyFoo explique cette opération de transformation en image très clairement

Il faut bien comprendre que les transformations successive sont ajoutée les unes au autres : D'abord je me déplace au centre, ensuite je tourne, ensuite je grossi et ensuite je me câle au centre de la texture

	case 1 :
		glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
		glRotatef(gAngle, 0.f, 0.f, 1.f);
		glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
		glScalef(2.f, 2.f, 0.f);
		break;

Dans cette configuration le scale est appelé à la fin ce qui va décaler le centre final de rotation de l'image... (Le site à l'air de dire que la rotation aurait dûe être annulée...)

	case 2 :
		glScalef(2.f, 2.f, 0.f);
		glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
		glRotatef(gAngle, 0.f, 0.f, 1.f);
		glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
	
En réalisant le scale au début les opérations suivantes vont faire que la texture va tourner autour de son bord supérieur inférieur droit
	
	case 3 :
		glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
		glRotatef(gAngle, 0.f, 0.f, 1.f);
		glScalef(2.f, 2.f, 0.f);
		break;

Ici on retire le redécalage au centre de l'image, en conséquence l'image va tourner autour de son bord supérieur gauche


		case 4 :
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glScalef(2.f, 2.f, 0.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			break;

Ici la rotation est faite en permier et sans le recalage finale, du coup l'image va tourner autours du coin supérieur gauche de l'écran

Lorsque toutes les opérations de transformations ont été réalisée, on appelle la méthode de rendu de notre texture

	La fonction handleKeys permet de contrôler le passage d'une animation à l'autre en appuyant sur 'q' et en incrémentant la variable gTransformationCombo

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/13_matrix_transformations/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Texture d'une flèche
LTexture gRotatingTexture;

//Angle de rotation
GLfloat gAngle = 0.f;

//Etat de la trnsformation
int gTransformationCombo = 0;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Initialisation de la clear color
	//glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gRotatingTexture.loadTextureFromFile("sprite/texture.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{
	gAngle += 360.f / SCREEN_FPS;

	//Cap angle
	if(gAngle > 360.f)
		gAngle -= 360.f;
}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Reset des transformations
	glLoadIdentity();
	
	//Rendu des transformation dans la scène courante
	switch(gTransformationCombo)
	{
		case 0 :
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glScalef(2.f, 2.f, 0.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			break;
		case 1 :
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			glScalef(2.f, 2.f, 0.f);
			break;
		case 2 :
			glScalef(2.f, 2.f, 0.f);
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			break;
		case 3 :
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glScalef(2.f, 2.f, 0.f);
			break;
		case 4 :
			glRotatef(gAngle, 0.f, 0.f, 1.f);
			glTranslatef(SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f);
			glScalef(2.f, 2.f, 0.f);
			glTranslatef(gRotatingTexture.imageWidth() / -2.f, gRotatingTexture.imageHeight() / -2.f, 0.f);
			break;
			
	}
	
	//Rendu des textures
	gRotatingTexture.render(0.f, 0.f);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Si q est pressé
	if(key == 'q')
	{
		//Reset la rotation
		gAngle = 0.f;
		
		//Gestion des cycles de combinaisons
		gTransformationCombo++;
		if(gTransformationCombo > 4)
			gTransformationCombo = 0;
	}
}


