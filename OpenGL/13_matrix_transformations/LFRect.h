/*
	Boris Merminod le 10/01/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/13_matrix_transformations/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
