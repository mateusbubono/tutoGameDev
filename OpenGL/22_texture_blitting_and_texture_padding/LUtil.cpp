/*
Boris merminod le 23/04/2019

	On va commencer par se déclarer trois textures :

//Textures à charger
LTexture gLeft;
LTexture gRight;

//Combinaison des deux texture générée
LTexture gCombined;

gLeft et gRight sont nos textures chargées depuis deux fichier .png différents. Entuite pour gCombined il s'agit de la version blit de gLeft avec gRight.

	Dans la fonction loadMedia(), on va charger les deux images comme étant un esemble de pixels puis on va allouer un ensemble de pixels assez large pour contenir nos deux images à blit.
	Puis on va blit par la suite nos deux textures, dans cet ensemble de pixels blanc et vide. Ensuite on va faire un pad de notre image blittée, puis on va créer à la fin la texture pour de bon.	
	
	gLeft.blitPixels32(0,0, gCombined);
	gRight.blitPixels32(gLeft.imageWidth(),0, gCombined);

Dans notre fonction render on va juste effectuer le rendu de notre texture combinée
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/22_texture_blitting_and_texture_padding/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LFont.h"

//Textures à charger
LTexture gLeft;
LTexture gRight;

//Combinaison des deux texture générée
LTexture gCombined;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture Left
	if(!gLeft.loadPixelsFromFile32("sprite/left.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger la texture left.png\n");
		return false;
	}
	
	//Chargement de la txture right
	if(!gRight.loadPixelsFromFile32("sprite/right.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger la texture right.png\n");
		return false;
	}
	
	//Création de pixels blancs et vide
	gCombined.createPixels32(gLeft.imageWidth() + gRight.imageWidth(), gLeft.imageHeight());
	
	//Blit des images
	gLeft.blitPixels32(0,0, gCombined);
	gRight.blitPixels32(gLeft.imageWidth(),0, gCombined);
	
	//Pad de la texture créée
	gCombined.padPixels32();
	gCombined.loadTextureFromPixels32();
	
	//Suppression des texture gLeft et gRight
	gLeft.freeTexture();
	gRight.freeTexture();
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	
	//Render red text
	//glColor3f(0.f, 1.f, 0.f);
	gCombined.render((SCREEN_WIDTH - gCombined.imageWidth())/2.f, (SCREEN_HEIGHT - gCombined.imageHeight())/2.f);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

