/*
Boris Merminod le 23/04/2019


Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/22_texture_blitting_and_texture_padding/index.php
*/

#ifndef LVERTEX_POS_2D_H
#define LVERTEX_POS_2D_H

#include "LOpenGL.h"

struct LVertexPos2D
{
	GLfloat x;
	GLfloat y;
};

#endif
