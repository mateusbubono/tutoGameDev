/*
	Boris Merminod le 23/04/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/22_texture_blitting_and_texture_padding/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
