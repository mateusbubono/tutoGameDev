/*
Boris merminod le 20/07/2018

	Dans le fichier LUtil.cpp, on va déclarer 3 variables globales. gStretchedTexture, est la texture que nous allons manipuler de la taille 160x120. gStretchRect est le rectangle représentant la dimension du stretch. Enfin gFiltering va controler un filtre de rendu de la texture.
	
	Les fonction loadMedia et render() vont respectivement charger notre texture et effectuer le stretch à l'écran
	
	Une fonction handleKeys() est ajoutée dans le but de gérer des filtre de rendu de notre texture. Ainsi si j'appuie sur 'q' le filtre de rendu va basculer via la fonction glTexParameteri() vers le filtre GL_LINEAR ou GL_NEAREST. Grosso modo le filtre GL_LINEAR donne un rendu de texture plus flou mais plus lisse, alors que le filtre GL_NEAREST rend la texture plus pixelisée mais plus nette.

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/11_stretching_and_filters/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//Image à redimensionner
LTexture gStretchedTexture;

//Dimensions du Stretch
LFRect gStretchRect = {0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT};

//Filre de texture
GLenum gFiltering = GL_LINEAR;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Initialisation de la clear color
	//glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gStretchedTexture.loadTextureFromFile("sprite/mini_opengl.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des textures
	gStretchedTexture.render(0.f, 0.f, NULL, &gStretchRect);
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}

void handleKeys(unsigned char key, int x, int y)
{
	if(key == 'q')
	{
		//Récupérer la texture pour la modifier
		glBindTexture(GL_TEXTURE_2D, gStretchedTexture.getTextureID());

		//Basculement du filtre linear/nearest
		if(gFiltering != GL_LINEAR)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			gFiltering = GL_LINEAR;
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			gFiltering = GL_NEAREST;
		}

		//Détacher la texture
		glBindTexture(GL_TEXTURE_2D, 0);
	}

}
