/*
	Boris Merminod le 20/07/2018
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/11_stretching_and_filters/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
