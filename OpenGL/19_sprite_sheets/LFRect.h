/*
	Boris Merminod le 20/02/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
