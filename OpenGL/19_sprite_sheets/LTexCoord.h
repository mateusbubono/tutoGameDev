/*

Boris Merminod le  20/02/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php

*/

#ifndef LTEX_COORD_H
#define LTEX_COORD_H

#include "LOpenGL.h"

struct LTexCoord
{
	GLfloat s;
	GLfloat t;
};

#endif
