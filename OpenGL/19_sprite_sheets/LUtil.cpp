/*
Boris merminod le 20/02/2019

	On commence par inclure ici l'en-tête de notre classe LSpriteSheet.
	On ajoute une variable globale qui sera une instance de notre classe LSpriteSheet : gArrowSprites.
	
	Dans la fonction loadMedia(), on va charger notre texture via une méthode de la classe LTexture(). Puis on va appliquer sur chaque flèche de la texture un rectangle de clip pour obtenir un sprite unique par clip. Enfin on va appeler la méthode generateDataBuffer() pour créer nos data buffers à l'aide des rectangles de clips
	
	Enfin dans la fonction render on va réaliser le rendu de chaque clip à chaque coin de l'écran
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LSpriteSheet.h"

//Sprite sheet
LSpriteSheet gArrowSprites;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	if(!gArrowSprites.loadTextureFromFile("sprite/arrows.png"))
	{
		fprintf(stderr,"loadMedia : Impossible de charger la texture OpenGL\n");
		return false;
	}
	
	//Set Clips
	LFRect clip = {0.f, 0.f, 128.f, 128.f};
	
	//Coin supérieur gauche
	clip.x = 0.f;
	clip.y = 0.f;
	gArrowSprites.addClipSprite(clip);
	
	//Coin supérieur droit
	clip.x = 128.f;
	clip.y = 0.f;
	gArrowSprites.addClipSprite(clip);
	
	//Coin inférieur gauche
	clip.x = 0.f;
	clip.y = 128.f;
	gArrowSprites.addClipSprite(clip);
	
	//Coin inférieur droit
	clip.x = 128.f;
	clip.y = 128.f;
	gArrowSprites.addClipSprite(clip);
	
	//Générer un VBO
	if(!gArrowSprites.generateDataBuffer())
	{
		fprintf(stderr, "loadMedia : Unable to clip sprite sheet\n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu de la flèche supérieure gauche
	glLoadIdentity();
	glTranslatef(64.f, 64.f, 0.f);
	gArrowSprites.renderSprite(0);
	
	//Rendu de la flèche supérieure droite
	glLoadIdentity();
	glTranslatef(SCREEN_WIDTH - 64.f, 64.f, 0.f);
	gArrowSprites.renderSprite(1);
	
	//Rendu de la flèche inférieure gauche
	glLoadIdentity();
	glTranslatef(64.f, SCREEN_HEIGHT - 64.f, 0.f);
	gArrowSprites.renderSprite(2);
	
	//Rendu de la flèche inférieure droite
	glLoadIdentity();
	glTranslatef(SCREEN_WIDTH-64.f, SCREEN_HEIGHT - 64.f, 0.f);
	gArrowSprites.renderSprite(3);
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

