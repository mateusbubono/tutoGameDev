/*
Boris Merminod le 20/02/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php
*/

#ifndef LVERTEX_DATA_2D_H
#define LVERTEX_DATA_2D_H

#include "LVertexPos2D.h"
#include "LTexCoord.h"

struct LVertexData2D
{
	LVertexPos2D position;
	LTexCoord texCoord;
};

#endif
