/*

Boris Merminod le 20/02/2019

	Voici notre classe fille de LTexture : LSpriteSheet. Un sprite sheet (morceau de sprite) est donc une texture avec un mode d'utilisation spécifique.
	La classe possède un constructeur et un destructeur (comme d'habitude). Vient ensuite une méthode addClipSprite, qui va ajouter un rectangle de clip représentant un sprite et qui va être ajouté à une table membre mClips. L'accesseur getClip permet de récupérer un clip en particulier dans cette table mClips.
	Une fois que l'on a set tous les clips de notre sprite dans notre mClips array, on va appeler la fonction generateDataBuffer qui va utiliser les clips pour générer notre VBO et notre IBO. 
	Quand on a terminé avec l'utilisation de nos clip, on appelle la méthode freeSheet pour libérer la mémoire allouée pour les gérer.
	pour effectuer le rendu de nos clips, on va utiliser la fonction renderSprite()
	
	Les attributs de la classe vont être mClips que j'ai déjà présenté plus haut, mVertexDataBuffer qui va contenir les vertex data pour tous les sprites représenté par chaque clip, mIndexBuffers représentant notre table IBO. Ainsi on dispose d'une grande table VBO et IBO qui va servir à représenter chaque sprite individuel découpé par nos clips

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php

*/

#ifndef LSPRITE_SHEET_H
#define LSPRITE_SHEET_H

#include "LTexture.h"
#include "LVertexData2D.h"
#include <vector>

class LSpriteSheet : public LTexture
{
	public :
		LSpriteSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Initialisation de l'ID du Buffer
		Side Effects :
		- None
		*/
		
		~LSpriteSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour le sprite sheet data
		Side Effects :
		- None
		*/
		
		int addClipSprite(LFRect& newClip);
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Ajoute un rectangle de clipping dans une table de clip
		- Retourne l'index du rectangle de clippig dans la table de clip
		Side Effects :
		- None
		*/
		
		LFRect getClip(int index);
		/*
		Pré-conditions :
		- Un index valide
		Post conditions :
		- Retourne un rectangle de clipping depuis la table de clip à un index donné
		Side Effects :
		- None
		*/
		
		bool generateDataBuffer();
		/*
		Pré-conditions : 
		- Une texture de base LTexture chargée
		- Des rectangles de clipping dans une table de clips
		Post conditions :
		- Génère un VBO et un IBO pour effectuer le rendu de sprites
		- Retourne true en cas de succès
		- Reporting des erreurs sur stderr
		Side Effects :
		- Les buffers membre sont bound
		*/
		
		void freeSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer le VBO, l'IBO et la table des clips
		Side Effects :
		- None
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer le sprite sheet et l'objet LTexture
		Side Effects :
		- None
		*/
		
		void renderSprite(int index);
		/*
		Pré-conditions :
		- Une texture LTexture chargée
		- Un VBO fonctionnel
		Post conditions :
		- Rendu d'un sprite à un index donné
		Side Effects :
		- Texture LTexture de base est bound
		- Le buffer membre est bound
		*/
		
	protected :
		//Sprite clips
		std::vector<LFRect>mClips;
		
		//VBO data
		GLuint mVertexDataBuffer;
		GLuint* mIndexBuffers;		
};

#endif
