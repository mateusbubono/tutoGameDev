/*

Boris Merminod le 20/02/2019

	Le constructeur va initialiser les attributs membres. Le destructeur va appeleler la méthode freeSheet(). Comme le destructeur de la classe mère LTexture est virtual, alors le destructeur de la classe LTexture va être appelé par défaut après le destructeur de la classe LSpriteSheet.
	
	La méthode addClipSprite() va simplement ajouter un rectangle de clipping dans une structure STL de type vector. La fonction retourne l'index exacte du dernier élément inséré dans le vector.
	La méthode getClip() est un accesseur qui va récupérer un clip à un index précis dans la structure de type vector. L'auteur du cours justifie le fait de ne pas tester les limite de taille de la table pour éviter d'ajouter un calcul pesant au programme. On est donc moins sécuritaire mais plus performant en contre partie.
	
	Lorsque tous les clips on été construit, on va appeler la fonction generateDataBuffer, pour créer notre VBO et notre IBO. notre VBO va se composer d'une table à une dimension avec 4 sommets par sprites. Donc si j'ai 4 sprites, mon VBO sera composé de 16 éléments.
	Notre IBO lui se compose d'un élément par sprite donc 4 sprites correspond à 4 éléments.
	Par la suite on va générer nos buffers via glGenBuffers(). On réupère ensuite la largeur et la hauteur de notre texture, puis on déclare une table spriteIndices de 4 éléments que l'on va utiliser pour notre IBO (pour déterminer dans quel ordre sont dessinés les sommets).
	Quand on a fait tout ça, on va rentrer dans une boucle qui va itéré sur la taille de notre IBO, c'est à dire 4 images. Pour chaque image, le VBO contient 4 sommets nous allons donc nous servir du numéro du sprite pour connaître les sommets qui lui correspondent dans le VBO, comme ceci :  
	
	for(int i = 0; i<totalSprites; ++i)
	{
		//Initialisation des indices
		spriteIndices[0] = i * 4 + 0;
		spriteIndices[1] = i * 4 + 1;
		spriteIndices[2] = i * 4 + 2;
		spriteIndices[3] = i * 4 + 3;

En plus clair le premier clip aura comme numéro de sommet le 0, 1, 2, 3. Le second aura comme numéro de sommet le 4, 5, 6, 7, et cela ce poursuit pour atteindre les 4*4 sommets qui doivent être représentés.
	Après avoir déterminer les sommets pour un clip de l'IBO, on va set pour chaque sommet la position en x, la position en y ainsi que les coordonnées s et t des textures. On part du principe que l'origine se trouve au centre du clip donc au centre du morceau de sprite représenté.
	A la fin de la boucle on va set les données de l'IBO pour le sprite courant via glBindBuffer et glBufferData
	En sortie de la boucle une fois qu'on a fait un set de l'IBO pour chaque sommet, on va faire un set du VBO via un nouveau glBindBuffer puis un glBufferData.
	
	Dans la fonction freeSheet(), on va simplement libérer la mémoire allouée pour gérer nos sprites sheets en supprimant les buffers mais en gardant la textures au cas ou l'on souhaiterai la réutiliser.
	
	freeTexture() est une fonction qui est surchargée de LTexture. Elle va appeler freeSheet puis appeler à la suite freeTexture de la classe LTexture pour libérer la mémoire allouée à la gestion des texture
	
	Le rendu va être effectué par la fonction renderSprite(). On va pour cela récupérer le VBO avec les coordonnées des sommets et des textures, puis effectuer le rendu par sprite en utilisant l'IBO.
	

Code Source tiré de : http://lazyfoo.net/tutorials/OpenGL/19_sprite_sheets/index.php

*/

#include "LSpriteSheet.h"

LSpriteSheet::LSpriteSheet()
{
	//Initialisation du vertex buffer data
	mVertexDataBuffer = 0;
	mIndexBuffers = NULL;
}

LSpriteSheet::~LSpriteSheet()
{
	//Cleat sprite sheet data
	freeSheet();
}

int LSpriteSheet::addClipSprite(LFRect& newClip)
{
	//Ajouter un clip puis retourner l'index
	mClips.push_back(newClip);
	return mClips.size() - 1;
}

LFRect LSpriteSheet::getClip(int index)
{
	return mClips[index];
}

/*bool LSpriteSheet::generateDataBuffer()
{
	//If there is a texture loaded and clips to make vertex data from
	if( getTextureID() != 0 && mClips.size() > 0 )
	{
		//Allocate vertex buffer data
		int totalSprites = mClips.size();
		LVertexData2D* vertexData = new LVertexData2D[ totalSprites * 4 ];
        mIndexBuffers = new GLuint[ totalSprites ];

		//Allocate vertex data buffer name
		glGenBuffers( 1, &mVertexDataBuffer );

		//Allocate index buffers names
		glGenBuffers( totalSprites, mIndexBuffers );

		//Go through clips
		GLfloat tW = textureWidth();
		GLfloat tH = textureHeight();
		GLuint spriteIndices[ 4 ] = { 0, 0, 0, 0 };

		for( int i = 0; i < totalSprites; ++i )
		{
			//Initialize indices
			spriteIndices[ 0 ] = i * 4 + 0;
			spriteIndices[ 1 ] = i * 4 + 1;
			spriteIndices[ 2 ] = i * 4 + 2;
			spriteIndices[ 3 ] = i * 4 + 3;

			//Top left
			vertexData[ spriteIndices[ 0 ] ].position.x = -mClips[ i ].w / 2.f;
			vertexData[ spriteIndices[ 0 ] ].position.y = -mClips[ i ].h / 2.f;

			vertexData[ spriteIndices[ 0 ] ].texCoord.s =  (mClips[ i ].x) / tW;
			vertexData[ spriteIndices[ 0 ] ].texCoord.t =  (mClips[ i ].y) / tH;

			//Top right
			vertexData[ spriteIndices[ 1 ] ].position.x =  mClips[ i ].w / 2.f;
			vertexData[ spriteIndices[ 1 ] ].position.y = -mClips[ i ].h / 2.f;

			vertexData[ spriteIndices[ 1 ] ].texCoord.s =  (mClips[ i ].x + mClips[ i ].w) / tW;
			vertexData[ spriteIndices[ 1 ] ].texCoord.t =  (mClips[ i ].y) / tH;

			//Bottom right
			vertexData[ spriteIndices[ 2 ] ].position.x =  mClips[ i ].w / 2.f;
			vertexData[ spriteIndices[ 2 ] ].position.y =  mClips[ i ].h / 2.f;

			vertexData[ spriteIndices[ 2 ] ].texCoord.s =  (mClips[ i ].x + mClips[ i ].w) / tW;
			vertexData[ spriteIndices[ 2 ] ].texCoord.t =  (mClips[ i ].y + mClips[ i ].h) / tH;

			//Bottom left
			vertexData[ spriteIndices[ 3 ] ].position.x = -mClips[ i ].w / 2.f;
			vertexData[ spriteIndices[ 3 ] ].position.y =  mClips[ i ].h / 2.f;

			vertexData[ spriteIndices[ 3 ] ].texCoord.s =  (mClips[ i ].x) / tW;
			vertexData[ spriteIndices[ 3 ] ].texCoord.t =  (mClips[ i ].y + mClips[ i ].h) / tH;

			//Bind sprite index buffer data
			glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[ i ] );
			glBufferData( GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), spriteIndices, GL_STATIC_DRAW );
		}

		//Bind vertex data
		glBindBuffer( GL_ARRAY_BUFFER, mVertexDataBuffer );
		glBufferData( GL_ARRAY_BUFFER, totalSprites * 4 * sizeof(LVertexData2D), vertexData, GL_STATIC_DRAW );

		//Deallocate vertex data
		delete[] vertexData;
	}
	//Error
	else
	{
	    if( getTextureID() == 0 )
	    {
            printf( "No texture to render with!\n" );
	    }

        if( mClips.size() <= 0 )
	    {
            printf( "No clips to generate vertex data from!\n" );
	    }

		return false;
	}

	return true;
}*/


bool LSpriteSheet::generateDataBuffer()
{
	//Si une texture est chargée avec des clips pour créer des vertex data
	if(getTextureID() != 0 && mClips.size() > 0)
	{
		//Allocation d'un vertex buffer data
		int totalSprites = mClips.size();
		LVertexData2D* vertexData = new LVertexData2D[totalSprites * 4];
		mIndexBuffers = new GLuint[totalSprites];
		
		//Allocation d'un data buffer
		glGenBuffers(1, &mVertexDataBuffer);
		
		//Allocation de plusieurs index buffers
		glGenBuffers(totalSprites, mIndexBuffers);
		
		//Parcourir les clips
		GLfloat tW = textureWidth();
		GLfloat tH = textureHeight();
		GLuint spriteIndices[4] = {0,0,0,0};
		
		for(int i = 0; i<totalSprites; ++i)
		{
			//Initialisation des indices
			spriteIndices[0] = i * 4 + 0;
			spriteIndices[1] = i * 4 + 1;
			spriteIndices[2] = i * 4 + 2;
			spriteIndices[3] = i * 4 + 3;
			
			//En haut à gauche
			vertexData[spriteIndices[0]].position.x = -mClips[i].w / 2.f;
			vertexData[spriteIndices[0]].position.y = -mClips[i].h / 2.f;
			vertexData[spriteIndices[0]].texCoord.s = (mClips[i].x) / tW;
			vertexData[spriteIndices[0]].texCoord.t = (mClips[i].y) / tH;
			//En haut à droite
			vertexData[spriteIndices[1]].position.x = mClips[i].w / 2.f;
			vertexData[spriteIndices[1]].position.y = -mClips[i].h / 2.f;
			vertexData[spriteIndices[1]].texCoord.s = (mClips[i].x + mClips[i].w) / tW;
			vertexData[spriteIndices[1]].texCoord.t = (mClips[i].y) / tH;
			
			//En bas à droite
			vertexData[spriteIndices[2]].position.x = mClips[i].w / 2.f;
			vertexData[spriteIndices[2]].position.y = mClips[i].h / 2.f;
			vertexData[spriteIndices[2]].texCoord.s = (mClips[i].x + mClips[i].w) / tW;
			vertexData[spriteIndices[2]].texCoord.t = (mClips[i].y + mClips[i].h) / tH;
			
			//En bas à gauche
			vertexData[spriteIndices[3]].position.x = -mClips[i].w / 2.f;
			vertexData[spriteIndices[3]].position.y = mClips[i].h / 2.f;
			vertexData[spriteIndices[3]].texCoord.s = (mClips[i].x) / tW;
			vertexData[spriteIndices[3]].texCoord.t = (mClips[i].y + mClips[i].h) / tH;
			
			//Bind sprite index buffer data
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[i]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4*sizeof(GLuint), spriteIndices, GL_STATIC_DRAW);
			
		}
		
		//Bind vertex data
		glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
		glBufferData(GL_ARRAY_BUFFER, totalSprites * 4 * sizeof(LVertexData2D), vertexData, GL_STATIC_DRAW);
		//Libérer la mémoie allouée pour le vertex data
		delete[] vertexData;
	}
	else //Erreur
	{
		if(getTextureID() == 0)
			fprintf(stderr, "LSpriteSheet::generateDataBuffer : Pas de texture chargée pour le rendu\n");
		
		if(mClips.size() <= 0)
			fprintf(stderr, "LSpriteSheet::generateDataBuffer : Pas de clips pour générer un vertex data\n");
		
		return false;
	}
	
	return true;
}

void LSpriteSheet::freeSheet()
{
	//Clear le vertex buffer
	if(mVertexDataBuffer != 0)
	{
		glDeleteBuffers(1, &mVertexDataBuffer);
		mVertexDataBuffer = 0;
	}
	
	//Clear l'index buffer
	if(mIndexBuffers != NULL)
	{
		glDeleteBuffers(mClips.size(), mIndexBuffers);
		delete[] mIndexBuffers;
		mIndexBuffers = NULL;
	}
	
	//Clear clips
	mClips.clear();
}

void LSpriteSheet::freeTexture()
{
	//Get rid of sprite sheet data
	freeSheet();
	
	//Free texture
	LTexture::freeTexture();
}

void LSpriteSheet::renderSprite(int index)
{
	//Sprite sheet data exist
	if(mVertexDataBuffer != 0)
	{
		//Set texture
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
			//Bind vertex data
			glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
			
			//Set tecture coordinate data
			glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, texCoord));
			
			//Set vertex data
			glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, position));
			
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[index]);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
		
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
			
	}
}
