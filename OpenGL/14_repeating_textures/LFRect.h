/*
	Boris Merminod le 10/01/2019
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/14_repeating_textures/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
