/*
Boris merminod le 10/01/2019

	On commence ici par déclarer quelques variables globales. En premier la texture qui va nous servir de tuile (on dit en anglais to tile a texture). Ensuite il y a des offsets qui vont nous aider à dérouler la texture. Enfin on a une variable indiquant comment nous allons wrap notre texture
	
	//L'image qui va être répetée
	LTexture gRepeatingTexture;

	//Texture offset
	GLfloat gTexX = 0.f, gTexY = 0.f;

	//Texture wrap type
	int gTextureWrapType = 0;

	Notre fontion loadMedia() va charger notre texture et... c'est tout
	
	La fonction update() va mettre à jour les valeurs de nos variable d'offset pour dérouler notre texture. Le tout est limité par un seuil qui une fois dépasser va réinitialiser les coordonnées de notre texture ce qui nous donne plus clairement un scrolling en diagonale en boucle infinie
	
	La fonction render va faire le calcul suivant : 
	
	GLfloat textureRight = (GLfloat)SCREEN_WIDTH / (GLfloat) gRepeatingTexture.textureWidth();
	GLfloat textureBottom = (GLfloat)SCREEN_HEIGHT / (GLfloat)gRepeatingTexture.textureHeight();
	
Si on se dit que ma texture fait 128 pixels et que mon écran fasse 256 pixels de large, en divisant 256/128 j'obtiens 2.0, grâce à se résultat, on va mapper deux fois la texture sur l'axe x/s. Se calcul va aussi être  réalisé pour l'axe y/t.
	Après ce calcul, on va attacher notre texture via glBindingTexture, puis on va switcher vers la texture matrix via glMatrixMode(). La texture matrix peut être utilisée pour effectuer des transformations sue les coordonnées des textures, un peu comme quand on utilise la matrice modelview pour transformer des coordonnées de sommets. Cette matrice va être utilisée comme matrice d'identité via glLoadIdentity()
	Après avoir initialisée la matrice de texture, on va utiliser glTranslatef pour se positionner par rapport à la mise à jour de la valeur de l'offset. ici la valeur 1.0 d'un paramètre ne signifie pas un pixel, mais 100% de la texture, ce calcul est obtenu en divisant l'offset par la largeur en x, puis la hauteur en y. Enfin on va effectuer le rendu du quad avec les coordonnées transformée des textures :
	
	glTranslatef(gTexX / gRepeatingTexture.textureWidth(), gTexY / gRepeatingTexture.textureHeight(), 0.f);
	
	//Rendu
	glBegin(GL_QUADS);
		glTexCoord2f(0.f, 0.f); glVertex2f(0.f, 0.f);
		glTexCoord2f(textureRight, 0.f); glVertex2f(SCREEN_WIDTH, 0.f);
		glTexCoord2f(textureRight, textureBottom); glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);
		glTexCoord2f(0.f, textureBottom); glVertex2f(0.f, SCREEN_HEIGHT);
	glEnd();

	Il nous reste la fonction handleKeys(). en pressant sur 'q' on va changer via glTexParameteri le type de texture wrapping de GL_REPEAT à GL_CLAMP. Dans le cas de GL_REPEAT, en rencontrant une coordonnée de texture entre 0 et 1 le système va continuer son mapping de texture et le répéter. Dans le cas de GL_CLAMP, le mapping de texture va s'arrêter au-delà de 0 et 1 
	
	
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/14_repeating_textures/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//L'image qui va être répetée
LTexture gRepeatingTexture;

//Texture offset
GLfloat gTexX = 0.f, gTexY = 0.f;

//Texture wrap type
int gTextureWrapType = 0;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gRepeatingTexture.loadTextureFromFile("sprite/texture2.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

	//Déroule la texture
	gTexX++;
	gTexY++;
	
	//Cap Scrolling
	if(gTexX >= gRepeatingTexture.textureWidth())
		gTexX = 0.f;
	
	if(gTexY >= gRepeatingTexture.textureHeight())
		gTexY = 0.f;
}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Calculate texture maxima
	GLfloat textureRight = (GLfloat)SCREEN_WIDTH / (GLfloat) gRepeatingTexture.textureWidth();
	GLfloat textureBottom = (GLfloat)SCREEN_HEIGHT / (GLfloat)gRepeatingTexture.textureHeight();
	
	//Utiliser la répetition de la texture
	glBindTexture(GL_TEXTURE_2D, gRepeatingTexture.getTextureID());
	
	glMatrixMode(GL_TEXTURE);
	
	//Reset des transformations
	glLoadIdentity();
	
	glTranslatef(gTexX / gRepeatingTexture.textureWidth(), gTexY / gRepeatingTexture.textureHeight(), 0.f);
	
	//Rendu
	glBegin(GL_QUADS);
		glTexCoord2f(0.f, 0.f); glVertex2f(0.f, 0.f);
		glTexCoord2f(textureRight, 0.f); glVertex2f(SCREEN_WIDTH, 0.f);
		glTexCoord2f(textureRight, textureBottom); glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);
		glTexCoord2f(0.f, textureBottom); glVertex2f(0.f, SCREEN_HEIGHT);
	glEnd();
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Si q est pressé
	if(key == 'q')
	{
		//Cycle de répeition de textures
		gTextureWrapType++;
		if(gTextureWrapType >= 2)
			gTextureWrapType = 0;
		
		//Set la répetition de textures
		glBindTexture(GL_TEXTURE_2D, gRepeatingTexture.getTextureID());
		switch(gTextureWrapType)
		{
			case 0 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				printf("%d : GL_REPEAT\n", gTextureWrapType);
				break;
			case 1 :
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				printf("%d : GL_REPEAT\n", gTextureWrapType);
				break;
		}
	}
}


