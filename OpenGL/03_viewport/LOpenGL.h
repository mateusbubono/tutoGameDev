/*
	Boris Merminod le 30/04/2018
	
	Ce fichier contient l'ensemble des bibliothèques à inclure pour le fonctionnement de OpenGL
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/03_the_viewport/index.php
*/

#ifndef LOPENGL_H
#define LOPENGL_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

#endif
