/*
Boris merminod le 30/04/2018

	La variable globale gViewportMode contient les différents mode de viewport qui seront utilisé par le programme. La variable contiendra le viewport à réaliser
	La fonction initGL contient une fonction glViewport() indiquant quelle partie de l'écran nous allons vouloir voir en rendu. Elle prend 4 paramètres les coordonnées x et y du viewport et les width et height du viewport. La fonction initGL va alors demander d'effectuer le rendu de l'écran en entier.
	La fonction render contient les différents mode d'exploitation du viewport. Les première ligne sont similaire à la définition de initGL, elle définisse un viewport sur tout l'écran et effectue le rendu d'un rectangle rouge sur tout l'écran. Le second mode effectue un rendu sur le centre de l'écran et va effectuer le rendu d'un rectangle vert dans le viewport. les dimensions de l'écran pour toute forme dont le rendu est effectué, sont ramené au dimension définie par le viewport. Dans un troisième exemple le rendu est effectué sur le centre et la partie supérieure de l'écran, un rectangle bleu est rendu dans cette zone. Le quatrième exemple va créer 4 viewport différent et effectuer un rendu 'un rectangle par viewport. Le dernier exemple effectue un rendu comme s'il s'agissait d'un radar
	La fonction handleKeys va modifier la variable gViewportMode ce qui permet de moduler le viewport

 
code source tiré de : http://lazyfoo.net/tutorials/OpenGL/03_the_viewport/index.php
*/

#include "LUtil.h"

//Viewport mode
int gViewportMode = VIEWPORT_MODE_FULL;


bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Reset la matrice modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Bouger au centre de l'écran
	glTranslatef(SCREEN_WIDTH/2.f, SCREEN_HEIGHT / 2.f, 0.f);
	
	//Full View
	if(gViewportMode == VIEWPORT_MODE_FULL)
	{
		//Remplir l'écran
		glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		//Rectangle rouge
		glBegin(GL_QUADS);
			glColor3f(1.f,0.f,0.f);
			glVertex2f(-SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
			glVertex2f(-SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
		glEnd();
	}
	//Viewport at center of screen
	else if(gViewportMode == VIEWPORT_MODE_HALF_CENTER)
	{
		//Centrage du viewport
		glViewport(SCREEN_WIDTH/4.f, SCREEN_HEIGHT/4.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		
		//Rectangle vert
		glBegin(GL_QUADS);
			glColor3f(0.f,1.f,0.f);
			glVertex2f(-SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
			glVertex2f(-SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
		glEnd();
		
	}
	//Viewport centered at the top
	else if(gViewportMode == VIEWPORT_MODE_HALF_TOP)
	{
		//Viewport at the top
		glViewport(SCREEN_WIDTH/4.f, SCREEN_HEIGHT/2.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		
		//Rectangle bleu
		glBegin(GL_QUADS);
			glColor3f(0.f,0.f,1.f);
			glVertex2f(-SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, -SCREEN_HEIGHT/2.f);
			glVertex2f(SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
			glVertex2f(-SCREEN_WIDTH /2.f, SCREEN_HEIGHT/2.f);
		glEnd();
	}
	//Four viewport
	else if(gViewportMode == VIEWPORT_MODE_QUAD)
	{
		//Rectangle rouge en bas à gauche
		glViewport(0.f,0.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		glBegin(GL_QUADS);
			glColor3f(1.f,0.f,0.f);
			glVertex2f(-SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
			glVertex2f(-SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
		glEnd();
		
		//Rectangle vert en bas à droite
		glViewport(SCREEN_WIDTH/2.f,0.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		glBegin(GL_QUADS);
			glColor3f(0.f,1.f,0.f);
			glVertex2f(-SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
			glVertex2f(-SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
		glEnd();
		
		//Rectangle bleu en haut à gauche
		glViewport(0.f,SCREEN_HEIGHT/2.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		glBegin(GL_QUADS);
			glColor3f(0.f,0.f,1.f);
			glVertex2f(-SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
			glVertex2f(-SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
		glEnd();
		
		//Rectangle jaune en haut à droite 
		glViewport(SCREEN_WIDTH/2.f,SCREEN_HEIGHT/2.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		glBegin(GL_QUADS);
			glColor3f(1.f,1.f,0.f);
			glVertex2f(-SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, -SCREEN_HEIGHT/4.f);
			glVertex2f(SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
			glVertex2f(-SCREEN_WIDTH /4.f, SCREEN_HEIGHT/4.f);
		glEnd();
	}
	else if(gViewportMode == VIEWPORT_MODE_RADAR)
	{
		//Rectangle Full size
		glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
		glBegin(GL_QUADS);
			glColor3f(1.f, 1.f, 1.f);
			glVertex2f(-SCREEN_WIDTH /8.f, -SCREEN_HEIGHT/8.f);
			glVertex2f(SCREEN_WIDTH /8.f, -SCREEN_HEIGHT/8.f);
			glVertex2f(SCREEN_WIDTH /8.f, SCREEN_HEIGHT/8.f);
			glVertex2f(-SCREEN_WIDTH /8.f, SCREEN_HEIGHT/8.f);
			glColor3f(0.f, 0.f, 0.f);
			glVertex2f(-SCREEN_WIDTH /16.f, -SCREEN_HEIGHT/16.f);
			glVertex2f(SCREEN_WIDTH /16.f, -SCREEN_HEIGHT/16.f);
			glVertex2f(SCREEN_WIDTH /16.f, SCREEN_HEIGHT/16.f);
			glVertex2f(-SCREEN_WIDTH /16.f, SCREEN_HEIGHT/16.f);
		glEnd();
		
		//Rectangle radar
		glViewport(SCREEN_WIDTH/2.f,SCREEN_HEIGHT/2.f,SCREEN_WIDTH/2.f, SCREEN_HEIGHT/2.f);
		glBegin(GL_QUADS);
			glColor3f(1.f, 1.f, 1.f);
			glVertex2f(-SCREEN_WIDTH /8.f, -SCREEN_HEIGHT/8.f);
			glVertex2f(SCREEN_WIDTH /8.f, -SCREEN_HEIGHT/8.f);
			glVertex2f(SCREEN_WIDTH /8.f, SCREEN_HEIGHT/8.f);
			glVertex2f(-SCREEN_WIDTH /8.f, SCREEN_HEIGHT/8.f);
			glColor3f(0.f, 0.f, 0.f);
			glVertex2f(-SCREEN_WIDTH /16.f, -SCREEN_HEIGHT/16.f);
			glVertex2f(SCREEN_WIDTH /16.f, -SCREEN_HEIGHT/16.f);
			glVertex2f(SCREEN_WIDTH /16.f, SCREEN_HEIGHT/16.f);
			glVertex2f(-SCREEN_WIDTH /16.f, SCREEN_HEIGHT/16.f);
		glEnd();
			
	}
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}

void handleKeys(unsigned char key, int x, int y)
{
	//Si on appuie sur q
	if(key == 'q')
	{
		gViewportMode++;
		if(gViewportMode > VIEWPORT_MODE_RADAR)
			gViewportMode = VIEWPORT_MODE_FULL;
	}
}
