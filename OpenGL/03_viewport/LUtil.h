/*
	Boris Merminod le 30/04/2018
	
	On défini ici une énumération qui décrit l'ensemble des Viewport qui seront utilisé par ce programme
	
	code source tiré de : http://lazyfoo.net/tutorials/OpenGL/03_the_viewport/index.php
*/

#ifndef LUTIL_H
#define LUTIL_H

#include "LOpenGL.h"
#include <stdio.h>

//Les constantes d'affichage
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

//Viewport mode
enum ViewPortMode
{
	VIEWPORT_MODE_FULL,
	VIEWPORT_MODE_HALF_CENTER,
	VIEWPORT_MODE_HALF_TOP,
	VIEWPORT_MODE_QUAD,
	VIEWPORT_MODE_RADAR,
};

bool initGL();
/*
Pre Condition:
 -Un contexte OpenGL valide
Post Condition:
 - Initialisation des matrice et des couleurs 
 - Affichage des erreurs OpenGL à la console
 - Retourne false en cas d'erreur OpenGL
Side Effects:
 - La matrice de projection est initialisée en matrice d'identité
 - La matrice Modelview est initialisée en matrice d'identité
 - Le mode matrice est initialisée en modelview
 - La couleur est initialisée en noir 
*/

void update();
/*
Pre Condition:
 -None
Post Condition:
 -Does per frame logic
Side Effects:
 -None
*/

void render();
/*
Pre Condition:
 - Un contexte OpenGL valide
 - Une matrice modelview active
Post Condition:
 - Rendu de la scène
Side Effects:
 -Vide le buffer de couleur
 -Swaps le front/back buffer
 - Translation de la matrice modelview au centre de l'écran par défaut
 - Change la couleur de rendu actuelle
 - Change le viewport actuel
*/

void handleKeys(unsigned char key, int x, int y);
/*
Pré conditions :
 -None
Post conditions :
 - Changement des différent mode de point de vue lorsque l'utilisateur appuie sur q
Side Effects :
- Si l'utilisateur appuie sur e, le mode de matrice passe en matrice de projection
*/

#endif
