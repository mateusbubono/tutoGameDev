/*
	Boris Merminod le 10/07/2018
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/08_non_power_of_2_textures/index.php

*/


#ifndef LFRECT_H
#define LFRECT_H

#include "LOpenGL.h"

struct LFRect
{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif
