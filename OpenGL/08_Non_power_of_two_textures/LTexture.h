/*
	Boris Merminod le 10/07/2018

	Plusieurs modifications sont apportées à la classe LTexture. Tout d'abord la fonction loadTextureFromPixels32() se voit modifier et ajouter des paramètres supplémentaires. Il n'est pas possible de charger en OpenGL des texture qui ne sont pas des puissances de deux. Donc si ma texture n'est pas une puissance de deux, (par exemple 510 x 120) on va agrandir l'image en lui rajoutant les pixels manquant (par exemple 512 x 128), puis on va utiliser le clipping pour retirer ce dont on a pas besoin. Pour faire ça il faut connaître la taille originale de l'image ainsi que sa taille agrandie.
 
	Il nous faut donc des variables qui vont contenir les dimensions de l'image ainsi que les dimensions de l'image agrandie. Pour déterminer la taille agrandie des images, on utilise la méthode powerOfTwo() qui va calculer la valeur adéquat en fonction de la taille originale qui lui est passée en paramètre. Par exemple en lui passant la valeur 60 elle va retourner la valeur 64
	
	Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/08_non_power_of_2_textures/index.php
*/

#ifndef LTEXTURE_H
#define LTEXTURE_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "LFRect.h"

class LTexture
{
	public :
		LTexture();
		/*
		Pré-conditions :
		-None
		Post-conditions :
		- Initialise les attributs membres
		Side effects :
		- None
		*/
		
		~LTexture();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Libère la mémoire allouée pour gérer un objet de texture
		Side effects:
		- None
		*/
		
		bool loadTextureFromFile(std::string path);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Librairie DevIL initialisée
		Post-conditions :
		- Création d'une texture à partir d'un fichier
		- Report des erreur à la sortie stderr
		Side Effects :
		- Association d'une texture NULL 
		*/
		
		bool loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Création d'une texture à partir de pixels donnés
		- Reporte les erreurs à la console si la texture n'a pas pu être créée
		Side Effects: 
		- Attache une texture NULL
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		Post-conditions :
		- Suppression des texutres si elles existes
		- Mise en place d'un texture ID à 0
		Side Effects :
		- None
		*/
		
		void render(GLfloat x, GLfloat y, LFRect* clip = NULL);
		/*
		Pré-conditions :
		- Un contexte OpenGL valide
		- Une matrice modelview active
		Post-conditions :
		- opération de translation à une position données puis rendu de carré texturés
		- Si le paramètre clip est NULL alors la totalité de la texture est rendue
		Side  effects :
		- Associe à la texture membre une ID
		*/
		
		GLuint getTextureID();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne le nom et l'ID de la texture
		Side effects :
		- None
		*/
		
		GLuint textureWidth();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- retourne la largeur (width) de la texture
		Side effects :
		- None
		*/
		
		GLuint textureHeight();
		/*
		Pré-conditions :
		- None
		Post-conditions :
		- Retourne la hauteur de la texture (height)
		Side effects :
		- None
		*/

		GLuint imageWidth();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image width
		Side Effects :
		- None
		*/

		GLuint imageHeight();
		/*
		Pre Condition :
		- None
		Post Condition :
		- Retour d'une unpadded image height
		Side Effects :
		- None
		*/
		
	private :
		GLuint powerOfTwo(GLuint num);
		/*
		Pré-conditions :
		- None
		Post Conditions :
		- Retourne la puissance de deux entières la plus proche (et plus grande) que la valeur passée en paramètres
		Side Effects :
		- None
		*/
	
		//Nom de la texture
		GLuint mTextureID;
		
		//Dimensions de la texture
		GLuint mTextureWidth;
		GLuint mTextureHeight;	
		
		//Dimension Unpadded d'une image
		GLuint mImageWidth;
		GLuint mImageHeight;
};

#endif
