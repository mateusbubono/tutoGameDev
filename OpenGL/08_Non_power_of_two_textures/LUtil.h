/*
	Boris Merminod le 10/07/2018
	
	code source tiré de : http://lazyfoo.net/tutorials/OpenGL/08_non_power_of_2_textures/index.php
*/

#ifndef LUTIL_H
#define LUTIL_H

#include "LOpenGL.h"
#include <stdio.h>
#include <iostream>

//Les constantes d'affichage
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

bool initGL();
/*
Pre Condition:
 -Un contexte OpenGL valide
Post Condition:
 - Initialisation des matrice et des couleurs 
 - Affichage des erreurs OpenGL à la console
 - Retourne false en cas d'erreur OpenGL
 - Initialisation de la bibliothèque DevIL
 - Report vers la console des erreurs OpenGL
 - Report vers la console des erreurs de DevIL
Side Effects:
 - La matrice de projection est initialisée en matrice d'identité
 - La matrice Modelview est initialisée en matrice d'identité
 - Le mode matrice est initialisée en modelview
 - La couleur est initialisée en noir 
 - Mise en place d'un viewport remplissant l'espace de rendu
 - Le texturing est activé
 - Mise en place d'une clear color de DevIL vers le blanc transparent
*/

bool loadMedia();
/*
Pre Condition :
- Un contexte OpenGL valide
Post Condition :
- Chargement des médias à utiliser dans le programme
- Report des erreurs à la sortie stderr
- retourne true si le/les média(s) sont chargés avec succès
Side Effects :
- None
*/

void update();
/*
Pre Condition:
 -None
Post Condition:
 -Does per frame logic
Side Effects:
 -None
*/

void render();
/*
Pre Condition:
 - Un contexte OpenGL valide
 - Une matrice modelview active
Post Condition:
 - Rendu de la scène
Side Effects:
 -Vide le buffer de couleur
 -Swaps le front/back buffer
 - Translation de la matrice modelview au centre de l'écran par défaut
 - Change la couleur de rendu actuelle
 - Change le viewport actuel
*/

#endif
