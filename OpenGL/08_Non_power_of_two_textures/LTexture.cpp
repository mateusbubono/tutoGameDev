/*
	Boris Merminod le 10/07/2018

	En haut du fichier LTexture.cpp, on inclu la bibliothèque ilu.h pour utiliser quelques utilitaires DevIL supplémentaire.

	La fonction loadTextureFromFile() va charger et convertir notre image comme d'habitude. On récupère également la largeur et la hauteur  de l'image en pixel.
	Dès que l'image est chargée, il nous faut calculer sa taille plus grande et en puissance de deux. C'est le rôle de la fonction powerOfTwo(). On va ensuite checker si le résultat retourné est égale au dimensions actuelle de l'image (du coup cela signifie que notre image est déja une puissance de deux donc pas besoin de redimensionner). Lorsque l'image a besoin d'être redimensionner, on va commencer par mettre en place l'origine de celle-ci vers le coin supérieur gauche. C'est le rôle de la fonction iluImageParameter(). Lorsque l'image est agrandie, on va lui ajouter des pixels vers le bas et la droite, ce qui va élargir le canevas requis pour la texture via l'utilisation de la fonction iluEnlargeCanvas(). 

	La texture générée par loadTextureFromFile(), va être envoyée à la fonction loadTextureFromPixels32() avec ses dimensions initiales, puis agrandie. Prendre en compte ses deux dimensions va permettre d'effectuer le clipping des pixels rajouté directement via la fonction render et ainsi utiliser l'image aux dimensions d'origine.

	La fonction render() va permettre d'effectuer le rendu souhaité de la texture en utilisant les dimensions originales et agrandie de la texture. Il suffit alors de clip la texture dans ses dimensions originales en calculant le rapport entre la taille originale de l'image et sa taille agrandie

	Le destructeur va réinitialiser les deux dimensions utilisées pour la texture : la taille originale et la taille agrandie

	La fonction powerOfTwo() possède un code particulier qui n'est pas développer dans ce tuto. Mais la méthode fonctionne pour agrandir l'image selon une puissance de deux



Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/08_non_power_of_2_textures/index.php
*/

#include "LTexture.h"
#include <IL/il.h>
#include <IL/ilu.h>
//#include <il.h>

LTexture::LTexture()
{
	//Initialiation de l'ID de la texture
	mTextureID = 0;
	
	//Initialisation des dimensions de l'image
	mImageWidth = 0;
	mImageHeight = 0;
	
	//Initialisation des dimension de la texture
	mTextureWidth = 0;
	mTextureHeight = 0;
}

LTexture::~LTexture()
{
	//Libère la mémoire allouée par un objet de texture
	freeTexture();
}

bool LTexture::loadTextureFromFile(std::string path)
{
	//Flag du succès de fonctionnement de la fonction
	bool textureLoaded = false;
	
	//Générer une ID pour l'image
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);
	
	//Charger l'image
	ILboolean success = ilLoadImage(path.c_str());
	
	//L'image est-elle chargée
	if(success == IL_TRUE)
	{
		//Convertion de l'image au format RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success == IL_TRUE)
		{
			//Initialisation des dimensions
			GLuint imgWidth = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			GLuint imgHeight = (GLuint) ilGetInteger(IL_IMAGE_HEIGHT);

			//Calcul des dimensions requises de la texture
			GLuint texWidth = powerOfTwo(imgWidth);
			GLuint texHeight = powerOfTwo(imgHeight);

			//Si la texture est d'une taille non adéquat
			if(imgWidth != texWidth || imgHeight != texHeight)
			{
				//Placer l'image en haut à gauche
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				//Redimmensionner l'image
				iluEnlargeCanvas( (int) texWidth, (int) texHeight, 1);
			}
			
			//Créer une textre depuis une image de pixels dans un fichier
			textureLoaded = loadTextureFromPixels32((GLuint *)ilGetData(), imgWidth, imgHeight, texWidth, texHeight);
		}
	}
	
	//Suppression du fichier en mémoire
	ilDeleteImages(1, &imgID);
	
	//Report des erreurs
	if(!textureLoaded)
		fprintf(stderr, "loadTextureFromFile : Impossible de charger %s\n", path.c_str());
	
	return textureLoaded;
}

bool LTexture::loadTextureFromPixels32(GLuint* pixels, GLuint imgWidth, GLuint imgHeight, GLuint texWidth, GLuint texHeight)
{
	//free la texture si elle existe
	freeTexture();
	
	//Obtenir les dimensions de la texture
	mImageWidth = imgWidth;
	mImageHeight = imgHeight;
	mTextureWidth = texWidth;
	mTextureHeight = texHeight;
	
	//Générer une ID pour la texture
	glGenTextures(1, &mTextureID);
	
	//Attacher l'ID à la texture
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	//Générer une texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTextureWidth, mTextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	//Mise en place des paramètre de la texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	//Détacher la texture
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//Vérifie les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "LTexture::loadTextureFromPixels32 : Erreur pendant le chargement d'une texture depuis %p pixels ! %s\n", pixels, gluErrorString(error));
		return false;
	}
	
	return true;
}

void LTexture::freeTexture()
{
	//Suppression de la texture
	if(mTextureID != 0)
	{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}
	
	mImageWidth = 0;
	mImageHeight = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
}

void LTexture::render(GLfloat x, GLfloat y, LFRect* clip)
{
	//La texture doit exister
	if(mTextureID != 0)
	{
		//Suppression de la transformation précédente
		glLoadIdentity();
		
		//Coordonnées de la texture
		GLfloat texTop = 0.f;
		GLfloat texBottom = (GLfloat)mImageHeight / (GLfloat)mTextureHeight;
		GLfloat texLeft = 0.f;
		GLfloat texRight = (GLfloat)mImageWidth / (GLfloat)mTextureWidth;
		
		//Coordonnées des sommets
		GLfloat quadWidth = mImageWidth;
		GLfloat quadHeight = mImageHeight;
		
		//Gestion du clipping
		if(clip != NULL)
		{
			//Coordonnées de la texture
			texLeft = clip->x / mTextureWidth;
			texRight = (clip->x + clip->w)/ mTextureWidth;
			texTop = clip->y / mTextureHeight;
			texBottom = (clip->y + clip->h) / mTextureHeight;
			//Coordonnées des sommets
			quadWidth = clip->w;
			quadHeight = clip->h;
		}
		
		//On bouge à la zone de rendu
		glTranslatef(x, y, 0.f);
		
		//Mise en place de l'ID de la texture
		glBindTexture(GL_TEXTURE_2D, mTextureID);
		
		//Rendu d'un carré texturé
		glBegin(GL_QUADS);
			glTexCoord2f(texLeft, texTop); glVertex2f(0.f,0.f);
			glTexCoord2f(texRight,texTop); glVertex2f(quadWidth,0.f);
			glTexCoord2f(texRight,texBottom); glVertex2f(quadWidth,quadHeight);
			glTexCoord2f(texLeft,texBottom); glVertex2f(0.f,quadHeight);	
		glEnd();
	}
}

GLuint LTexture::getTextureID()
{
	return mTextureID;
}

GLuint LTexture::textureWidth()
{
	return mTextureWidth;
}

GLuint LTexture::textureHeight()
{
	return mTextureHeight;
}

GLuint LTexture::imageWidth()
{
	return mImageWidth;
}

GLuint LTexture::imageHeight()
{
	return mImageHeight;
}

GLuint LTexture::powerOfTwo(GLuint num)
{
	if(num == 0)
		return num;
	num--;
	num |= (num >> 1); //Or first 2 bits
	num |= (num >> 2); //Or next 2 bits
	num |= (num >> 4); //Or next 4 bits
	num |= (num >> 8); //Or next 8 bits
	num |= (num >> 16); //Or next 16 bits
	num++;
	return num;
}
