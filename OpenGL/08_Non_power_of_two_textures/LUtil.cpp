/*
Boris merminod le 10/07/2018

La fonction initGL() se voit ajouter la fonction iluInit() qui va permettre d'utiliser iluImageParameter() et iluEnlargeCanvas().

La fonction loadMedia() va charger notre image et render() va effectuer le rendu à l'écran

Notre système peut maintenant charger des textures de dimensions quelconque, l'inconvénient est que pour une texture de 520x235, on va créer une texture de dimension agrandie à 1024x256 ce qui prend de la place pour rien. Cet inconvénient est une raison pour laquelle il est bien d'utiliser plusieurs images dans une texture, le tout permet d'utiliser plus efficacement la mémoire GPU

code source tiré de : http://lazyfoo.net/tutorials/OpenGL/08_non_power_of_2_textures/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"

//texture Non-power-of-two
LTexture gNon2NTexture;

bool initGL()
{

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);
	
	//Initialisation de la clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	//Chargement de la texture
	if(!gNon2NTexture.loadTextureFromFile("sprite/opengl.png"))
	{
		fprintf(stderr, "loadMedia : Impossible de charger une texture \n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Rendu des textures
	gNon2NTexture.render((SCREEN_WIDTH - gNon2NTexture.imageWidth())/2.f, (SCREEN_HEIGHT - gNon2NTexture.imageHeight())/2.f );
	
	//Mise à jour de l'écran
	glutSwapBuffers();
		
}
