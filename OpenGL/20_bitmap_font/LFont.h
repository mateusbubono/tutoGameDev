/*
Boris Merminod le 23/04/2019

	On ajoute ici une classe LFont qui va héritée de LSpriteSheet. 
	La classe se compose d'un constructeur et destructeur normal, ensuite vient la fonction loadBitmap() qui va charger une image puis en effectuer un parsing en plusieurs sprites pour chaque lettre du font qui seront convertie en textures.
	La fonction freeFont() va libérer la mémoire allouée pour gérer le bitmap font.
	La fonction renderText() va effectuer le rendu d'un texte à partir du bitmap font.
	La classe possèdent 3 attributs. mSpace indique de combien de pixel en x il faut bouger quand un ' ' est rencontré. mLineHeight représente la distance entre la lettre la plus haute en pixel et la lettre la plus basse en pixels. mNewLine représente de combien de pixel on va se déplacer en y quand on rencontre un '\n'

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php
*/

#include "LSpriteSheet.h"

class LFont: private LSpriteSheet
{
	public :
		LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Initialisation de spacing variables
		Side Effects :
		- None
		*/
		
		~LFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer des Fonts
		Side Effects :
		- None
		*/
		
		bool loadBitmap(std::string path);
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Chargement de fonts bitmap
		- Retourne true s'il n'y a pas d'erreurs
		- Reporting des erreurs sur la console
		Side Effects :
		- None
		*/
		
		void freeFont();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer les textures et les sprites sheet du bitmap font
		Side Effects :
		- None
		*/
				
		void renderText(GLfloat x, GLfloat y, std::string text);
		/*
		Pré-conditions :
		- un font chargé
		Post conditions :
		- Rendu de textes
		Side Effects :
		- Binds la texture membre ainsi que les data buffers
		*/
		
	private :
		//Spacing variables
		GLfloat mSpace;
		GLfloat mLineHeight;
		GLfloat mNewLine;

};
