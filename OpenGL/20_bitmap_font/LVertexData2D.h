/*
Boris Merminod le 23/04/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php
*/

#ifndef LVERTEX_DATA_2D_H
#define LVERTEX_DATA_2D_H

#include "LVertexPos2D.h"
#include "LTexCoord.h"

struct LVertexData2D
{
	LVertexPos2D position;
	LTexCoord texCoord;
};

#endif
