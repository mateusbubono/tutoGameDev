/*

Boris Merminod le 23/04/2019

	Dans la fonction generateDataBuffer(), on va générer notre VBO et notre IBO comme avant en rajoutant la capacité de réaliser le parsing des rectangles de clipping. On ajoute cette fois des variables additionnelles pour set les sommets supérieure, inférieurs, gauche et droits.
	Pour chaque sprite on va setter les indices puis on va calculer le décalage en y du haut vers le bas, et le décalage en x de la gauche vers la droite pour notre vertex data. Cela va dépendre de l'origine de départ choisie.
	Ensuite on va setter les coordonnées de nos sommets et des textures ainsi que les indices par sprites comme celà a été réalisé dans le chapitre précédent. Ce qui change c'est comment sont placés les positions des sommets dont nous allons effectuer le rendu.
	Pour finir on va générer notre VBO et reporter les erreurs si elles doivent survenir


Code Source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php

*/

#include "LSpriteSheet.h"

LSpriteSheet::LSpriteSheet()
{
	//Initialisation du vertex buffer data
	mVertexDataBuffer = 0;
	mIndexBuffers = NULL;
}

LSpriteSheet::~LSpriteSheet()
{
	//Cleat sprite sheet data
	freeSheet();
}

int LSpriteSheet::addClipSprite(LFRect& newClip)
{
	//Ajouter un clip puis retourner l'index
	mClips.push_back(newClip);
	return mClips.size() - 1;
}

LFRect LSpriteSheet::getClip(int index)
{
	return mClips[index];
}


bool LSpriteSheet::generateDataBuffer(LSpriteOrigin origin)
{
	//Si une texture est chargée avec des clips pour créer des vertex data
	if(getTextureID() != 0 && mClips.size() > 0)
	{
		//Allocation d'un vertex buffer data
		int totalSprites = mClips.size();
		LVertexData2D* vertexData = new LVertexData2D[totalSprites * 4];
		mIndexBuffers = new GLuint[totalSprites];
		
		//Allocation d'un data buffer
		glGenBuffers(1, &mVertexDataBuffer);
		
		//Allocation de plusieurs index buffers
		glGenBuffers(totalSprites, mIndexBuffers);
		
		//Parcourir les clips
		GLfloat tW = textureWidth();
		GLfloat tH = textureHeight();
		GLuint spriteIndices[4] = {0,0,0,0};
		
		//Variables Origin
		GLfloat vTop = 0.f;
		GLfloat vBottom = 0.f;
		GLfloat vLeft = 0.f;
		GLfloat vRight = 0.f;
		
		for(int i = 0; i<totalSprites; ++i)
		{
			//Initialisation des indices
			spriteIndices[0] = i * 4 + 0;
			spriteIndices[1] = i * 4 + 1;
			spriteIndices[2] = i * 4 + 2;
			spriteIndices[3] = i * 4 + 3;
			
			//Set origin
			switch(origin)
			{
				case LSPRITE_ORIGIN_TOP_LEFT :
					vTop = 0.f;
					vBottom = mClips[i].h;
					vLeft = 0.f;
					vRight = mClips[i].w;
					break;
				case LSPRITE_ORIGIN_TOP_RIGHT :
					vTop = 0.f;
					vBottom = mClips[i].h;
					vLeft = -mClips[i].w;
					vRight = 0.f;
					break;
				case LSPRITE_ORIGIN_BOTTOM_LEFT :
					vTop = -mClips[i].h;
					vBottom = 0.f;
					vLeft = 0.f;
					vRight = mClips[i].w;
					break;
				case LSPRITE_ORIGIN_BOTTOM_RIGHT :
					vTop = -mClips[i].h;
					vBottom = 0.f;
					vLeft = -mClips[i].w;
					vRight = 0.f;
					break;
				default :
					vTop = -mClips[i].h / 2.f;
					vBottom = mClips[i].h / 2.f;
					vLeft = -mClips[i].w / 2.f;
					vRight = mClips[i].w /2.f;
					break;
			}
			
			//En haut à gauche
			vertexData[spriteIndices[0]].position.x = vLeft;
			vertexData[spriteIndices[0]].position.y = vTop;
			vertexData[spriteIndices[0]].texCoord.s = (mClips[i].x) / tW;
			vertexData[spriteIndices[0]].texCoord.t = (mClips[i].y) / tH;
			//En haut à droite
			vertexData[spriteIndices[1]].position.x = vRight;
			vertexData[spriteIndices[1]].position.y = vTop;
			vertexData[spriteIndices[1]].texCoord.s = (mClips[i].x + mClips[i].w) / tW;
			vertexData[spriteIndices[1]].texCoord.t = (mClips[i].y) / tH;
			
			//En bas à droite
			vertexData[spriteIndices[2]].position.x = vRight;
			vertexData[spriteIndices[2]].position.y = vBottom;
			vertexData[spriteIndices[2]].texCoord.s = (mClips[i].x + mClips[i].w) / tW;
			vertexData[spriteIndices[2]].texCoord.t = (mClips[i].y + mClips[i].h) / tH;
			
			//En bas à gauche
			vertexData[spriteIndices[3]].position.x = vLeft;
			vertexData[spriteIndices[3]].position.y = vBottom;
			vertexData[spriteIndices[3]].texCoord.s = (mClips[i].x) / tW;
			vertexData[spriteIndices[3]].texCoord.t = (mClips[i].y + mClips[i].h) / tH;
			
			//Bind sprite index buffer data
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[i]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4*sizeof(GLuint), spriteIndices, GL_STATIC_DRAW);
			
		}
		
		//Bind vertex data
		glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
		glBufferData(GL_ARRAY_BUFFER, totalSprites * 4 * sizeof(LVertexData2D), vertexData, GL_STATIC_DRAW);
		//Libérer la mémoie allouée pour le vertex data
		delete[] vertexData;
	}
	else //Erreur
	{
		if(getTextureID() == 0)
			fprintf(stderr, "LSpriteSheet::generateDataBuffer : Pas de texture chargée pour le rendu\n");
		
		if(mClips.size() <= 0)
			fprintf(stderr, "LSpriteSheet::generateDataBuffer : Pas de clips pour générer un vertex data\n");
		
		return false;
	}
	
	return true;
}

void LSpriteSheet::freeSheet()
{
	//Clear le vertex buffer
	if(mVertexDataBuffer != 0)
	{
		glDeleteBuffers(1, &mVertexDataBuffer);
		mVertexDataBuffer = 0;
	}
	
	//Clear l'index buffer
	if(mIndexBuffers != NULL)
	{
		glDeleteBuffers(mClips.size(), mIndexBuffers);
		delete[] mIndexBuffers;
		mIndexBuffers = NULL;
	}
	
	//Clear clips
	mClips.clear();
}

void LSpriteSheet::freeTexture()
{
	//Get rid of sprite sheet data
	freeSheet();
	
	//Free texture
	LTexture::freeTexture();
}

void LSpriteSheet::renderSprite(int index)
{
	//Sprite sheet data exist
	if(mVertexDataBuffer != 0)
	{
		//Set texture
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
			//Bind vertex data
			glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
			
			//Set tecture coordinate data
			glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, texCoord));
			
			//Set vertex data
			glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*)offsetof(LVertexData2D, position));
			
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[index]);
			glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
		
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
			
	}
}
