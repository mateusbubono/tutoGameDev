/*
Boris merminod le 23/04/2019

	La fonction loadMedia() va simplement servir à charger notre bitmap font.
	
	La fonction render() va effectuer le rendu de notre texte en appelant la méthode adéquat de la classe LFont
	
code source tiré de :  http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php
*/

#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LFont.h"

//Bitmap font
LFont gFont;

bool initGL()
{

	//Initialisation de GLEW
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK)
	{
		fprintf(stderr, "initGL : Echec d'initialisation de GLEW ! %s\n", glewGetErrorString(glewError));
		return false;
	}
	
	//S'assurer que la version de OpenGL est bien la 2.1
	if(!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "initGL : OpenGL2.1 n'est pas supporté !\n");
		return false;
	}

	//Initialisation du viewport
	glViewport(0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	//Initialisation de la matrice de projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	
	//Initialisation de la matrice Modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glClearColor(0.f, 0.f, 0.f, 1.f);
	
	//Active le texturing
	glEnable(GL_TEXTURE_2D);

	//Activation du blending
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//Checker les erreurs
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de OpenGL ! %s\n", gluErrorString(error));
		return false;
	}
	
	//Initialisation de DevIL
	ilInit();
	iluInit();
	ilClearColour(255, 255, 255, 000);
	
	//Check des erreurs
	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR)
	{
		fprintf(stderr, "initGL : Erreur d'initialisation de DevIL ! %s\n", iluErrorString(ilError));
		return false;
	}
	
	return true;
}

bool loadMedia()
{
	if(!gFont.loadBitmap("sprite/lazy_font.png"))
	{
		fprintf(stderr,"loadMedia : Impossible de charger la texture OpenGL\n");
		return false;
	}
	
	return true;
}

void update()
{

}

void render()
{
	//Clear le color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	
	//Render red text
	glColor3f(1.f, 0.f, 0.f);
	gFont.renderText(0.f, 0.f, "The quick brown fox jumps\nover the lazy dog");
	
	//Mise à jour de l'écran
	glutSwapBuffers();		
}

