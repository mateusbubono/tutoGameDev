/*
Boris Merminod le 23/04/2019

	Je passe un peu sur le constructeur et le destructeur qui ne font rien de fou.
	
	La fonction loadBitmap() a elle un grand intérêt. La fonction a besoin d'un path vers un fichier .bmp contenant du noir, du blanc et des nuances de gris. Le noir est ici la couleur de fond du .bmp si bien qu'un autre type de font ne fonctionnerait pas avec notre fonction. On commence donc par faire un freeFont() pour s'assurer qu'il n'y ait pas de font déjà chargé. Ensuite on va charger via loadPixelsFromFile notre font dans des pixels dont nous allons effectuer le parsing.
	Notre parser fonctionne selon l'idée que nos caractères sont séparés selon une grille de 16x16. Notre font nouvellement chargé va donc être divisé en width et en height par 16 ce qui nous donnera les dimensions de chaque case en pixels. Notre fonction utilise plusieurs variables :
- top, bottom et aBottom sont utilisés pour gérer l'espacement entre les caractères
- Les coordonnées en pixels sont représentées par pX et pY 
- Les coordonnées en pixels relative à chaque case de la grille sont représentés par bX et bY
- currentChar : garde une trace du code ASCII utilisé pour la case courante de la grille qui se prend le parsing
- nextClip : est le rectangle de clipping pour le sprite de notre caractère courant qui se prend le parsing
	Notre font va ensuite être parcouru ligne par ligne et pour chaque ligne, colonne par colonne. le parcour commence par le calcul du décalage de base pour chaque cellule ce qui va représenter les variable bX et bY initialisé à leur valeur "zéro". Le rectangle de clip du prochain clip est lui initialisé comme faisant la taille entière d'une cellule.
	Une cellule est parcouru ligne par ligne et pour chaque ligne on parcour les pixels colonne par colonne. Chaque pixel est alors analysé jusqu'à ce que l'on tombe sur un pixel dont la valeur RGBA ne correspond pas à ce qui a été défini en couleur de fond. Lors de cette détéction on choppe ainsi l'offset en x de notre caractère représentant son côté gauche.
	En parcourant notre cellule ligne par ligne et colonne par colonne mais en partant par la droite, selon la même méthode, on fini par trouver la largeur de notre caractère.
	Au sujet de la hauteur des caractères, on fonctionne un peu différement. Dans chacune de nos case, il y a un gap entre le sommet de la case et le haut du caractère. Pour que notre rectangle de clip soit le mieux adapté pour le plus de caractère possible, on va rechercher le caractère qui à la hauteur la plus grande et s'en servir comme hauteur par défaut pour l'ensemble des caractères de notre font. Cette hauteur doit être mesurée des deux côté, ainsi on va rechercher le point le plus haut pour le caractère ayant la hauteur la plus élevée, mais aussi le point le plus bas pour le caractère ayant la base la plus basse.
	La base du A est prise à part pour une raison que je n'ai pas bien comprise (je crois que c'est l'habitude du programmeur que je suis en train d'étudier).
	Le parsing d'une cellule va se terminer par l'ajout du sprite du caractère dans notre clip sprites. Ensuite on va incrémenter la valeur de currentChar qui contient le code ASCII du prochain caractère que l'on va parser. Et on v poursuivre ceci pour les 256 caractères de notre font.
	Lorsque l'opération de parsing est totalement terminée, on va passer en revue les rectangle de clip pour attribuer le point le plus haut et la taille en y du plus grand caractère du font sur les 256 caractères représentés.
	L'utilisation d'OpenGL nous permet de travailler sur les caractères de notre font. On pourrait par exemple lisser chaque sprite pour donner un effet moins plat à notre police d'écriture. Ce que l'on peut ainsi faire c'est utiliser la valeur brightness de chaque pixel comme égale à sa valeur alpha. Ainsi plus le pixel est sombre plus il ressortira fortement par rapport au fond. Comme notre bitmap font est une échelle de gris, il nous suffit juste de prendre la valeur du component Red comme  réprensatatif du brightness. Ensuite pour chaque pixel on va setter la valeur RGB vers le blanc et la seul chose qui shading sera sa transparence.
	Une fois le parsing terminé ainsi que le calcul des rectangle de clip, on va générer la texture ainsi que le VBO data.
	La texture wrap de notre texture va être paramétré sur GL_CLAMP_TO_BORDER, pour éviter que les bord des sprites ne soit découper d'une mauvaise façon.
	Ensuite on va calculer la valeur de l'espace. De base la valeur est fixée sur la moitié de la largeur d'une cellule. La valeur du retour charriot est aussi calculé par la distance entre le point de pixel du caractère le plus haut et la ligne de base de la cellule (je crois). Enfin la hauteur de ligne est calculée comme étant l'espace entre le pixel le plus haut du caractère le plus haut et le pixel le plus bas du caractère le plus bas.
	
	La fonction freeFont() va appeler la méthode freeTexture définie dans la classe mère LSpriteSheet
	
	Vient ensuite la fonction renderText(), qui ressemble à une version un peu spéciale de la fonction renderSprite de la classe LSpriteSheet. La fonction va initialiser les position à dessiner, effectuer un translate vers ces position pour effectuer le rendu des différent point, attacher ensuite la texture puis set les VBO data.
	L'avantage d'utiliser LFont est que l'on va attacher la texture une fois, puis effectuer le rendu plusieurs fois à partir de cette attache. Le Binding de texture dans OpenGL est une opération très coûteuse, il est donc important de bien tirer parti d'une texture attachée pour éviter des échanges permanent avec la VRAM.
	La fonction va parcourir le string qui lui est passé en paramètre pour effectuer le rendu du caractère adéquat correspondant à chaque caractère. Si un espace est trouvé, on va faire une translation pour montrer l'espace et décaler le prochain point dont il faudra effectuer le rendu. Le retour charriot réinitialise la position de rendu au début d'une ligne et effectuer un espace vers le bas en y. Pareil le prochain point de rendu est décalé.
	Quand on tombe sur un caractère, on va récupérer son code ASCII puis attacher le sprite du caractère en question via l'IBO ce qui nous permettra d'en effectuer le rendu. Puis on va faire une translation vers le prochain point de rendu. Le tout ce répète jusqu'à ce qu'il n'y ait plus rien à afficher
	


code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php
*/

#include "LFont.h"

LFont::LFont()
{
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

LFont::~LFont()
{
	freeFont();
}

bool LFont::loadBitmap(std::string path)
{
	//Loading flag
	bool success = true;
	//Background pixel
	const GLuint BLACK_PIXEL = 0xFF000000;
	
	//Suppression du fond existant 
	freeFont();
	
	//Image pixels loaded
	if(loadPixelsFromFile(path))
	{
		//Get cell dimension
		GLfloat cellW = imageWidth() / 16.f;
		GLfloat cellH = imageHeight() / 16.f;
		
		//Get letter bottom and top
		GLuint top = cellH;
		int bottom = 0;
		GLuint aBottom = 0;
		
		//Current pixel coordinates
		int pX = 0;
		int pY = 0;
		
		//Base cell offsets
		int bX = 0;
		int bY = 0;
		
		//Begin parsing bitmap font
		GLuint currentChar = 0;
		LFRect nextClip = {0.f, 0.f, cellW, cellH};
		
		//Go through celle rows
		for(unsigned int rows = 0; rows<16; rows++)
		{
			for(unsigned int cols = 0; cols <16; cols++)
			{
				//Begin cell parsing
				
				//Set Base offset
				bX = cellW * cols;
				bY = cellH * rows;
				
				//Initialize clip
				nextClip.x = cellW * cols;
				nextClip.y = cellH * rows;
				
				nextClip.w = cellW;
				nextClip.h = cellH;
				
				//Find left side of character
				for(int pCol = 0; pCol < cellW; pCol++)
				{
					for(int pRow = 0; pRow < cellH; pRow++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non background pixel found
						if(getPixel32(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's x offset
							nextClip.x = pX;
							
							//Break the loops
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Right Side
				for(int pCol_w = cellW - 1; pCol_w >= 0; pCol_w--)
				{
					for(int pRow_w = 0; pRow_w < cellH; pRow_w++)
					{
						//Set pixel offset
						pX = bX + pCol_w;
						pY = bY + pRow_w;
						
						//Non_background pixel found
						if(getPixel32(pX, pY) != BLACK_PIXEL)
						{
							//Set sprite's width
							nextClip.w = (pX - nextClip.x) + 1;
							
							//Break the loops
							pCol_w = -1;
							pRow_w = cellH;
						}
					}
				}
				
				//Find top
				for(unsigned int pRow = 0; pRow < cellH; pRow++)
				{
					for(int pCol = 0; pCol < cellW; pCol++)
					{
						//Set pixel offset
						pX = bX + pCol;
						pY = bY + pRow;
						
						//Non-background pixel found
						if(getPixel32(pX, pY) != BLACK_PIXEL)
						{
							//New Top found
							if(pRow < top)
								top = pRow;
							
							//Break the loop
							pCol = cellW;
							pRow = cellH;
						}
					}
				}
				
				//Find bottom
				for(int pRow_b = cellH - 1; pRow_b >= 0; pRow_b--)
				{
					for(int pCol_b = 0; pCol_b < cellW; pCol_b++)
					{
						//Set pixel offset
						pX = bX + pCol_b;
						pY = bY + pRow_b;
						
						//Non-background pixel found
						if(getPixel32(pX, pY) != BLACK_PIXEL)
						{
							//Set BaseLine
							if(currentChar == 'A')
								aBottom = pRow_b;
							
							if(pRow_b > bottom)
								bottom = pRow_b;
							
							//Break the loop
							pCol_b = cellW;
							pRow_b = -1;
						}
					}
				}
				
				//Go to next character
				mClips.push_back(nextClip);
				currentChar++;
			}
		}
		
		//Set top
		for(int t=0; t<256; t++)
		{
			mClips[t].y += top;
			mClips[t].h -= top;
		}
		
		//Blend
		const int RED_BYTE = 1;
		const int GREEN_BYTE = 1;
		const int BLUE_BYTE = 2;
		const int ALPHA_BYTE = 3;
		
		//Go through pixels
		const int PIXEL_COUNT = textureWidth() * textureHeight();
		GLuint * pixels = getPixelData32();
		for(int i = 0; i<PIXEL_COUNT; i++)
		{
			//Get individual color components
			GLubyte * colors = (GLubyte*)&pixels[i];
			
			//White pixel shaded by transparency
			colors[ALPHA_BYTE] = colors[RED_BYTE];
			colors[RED_BYTE] = 0xFF;
			colors[GREEN_BYTE] = 0XFF;
			colors[BLUE_BYTE] = 0xFF;
		}
		
		//Create texture from manipulated pixels
		if(loadTextureFromPixels32())
		{
			//Build vertex buffer from sprite sheet data
			if(!generateDataBuffer(LSPRITE_ORIGIN_TOP_LEFT))
			{
				fprintf(stderr, "loadBitmap : Impossible de créer le vertex buffer à partir du bitmap font\n");
				success = false;
			}
		}
		else
		{
			fprintf(stderr, "loadBitmap : Impossible de créer une texture à partir des pixels du bitmap font\n");
			success = false;
		}
		
		//Set font texture wrap
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		
		//Set spacing variables
		mSpace = cellW / 2;
		mNewLine = aBottom - top;
		mLineHeight = bottom - top;
	}
	else
	{
		fprintf(stderr, "loadBitmap : Impossible de charger l'image bitmap font : %s\n", path.c_str());
		success = false;
	}
	
	return success;
}

void LFont::freeFont()
{
	//Get rid of sprite sheet
	freeTexture();
	
	mSpace = 0.f;
	mLineHeight = 0.f;
	mNewLine = 0.f;
}

void LFont::renderText(GLfloat x, GLfloat y, std::string text)
{
	//If there is a texture to render from
	if(getTextureID() != 0)
	{
		//Draw position
		GLfloat dX = x;
		GLfloat dY = y;
		
		//Move to draw position
		glTranslatef(x, y, 0.f);
		
		//Set texture
		glBindTexture(GL_TEXTURE_2D, getTextureID());
		
		//Enable vertex and texture coordinate arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		//Bind vertex data
		glBindBuffer(GL_ARRAY_BUFFER, mVertexDataBuffer);
		
		//Set texture coordinate data
		glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, texCoord));
		
		//Set vertex data
		glVertexPointer(2, GL_FLOAT, sizeof(LVertexData2D), (GLvoid*) offsetof(LVertexData2D, position));
		
		//Go through string
		for(unsigned int i=0; i<text.length(); i++)
		{
			//Space
			if(text[i] == ' ')
			{
				glTranslatef(mSpace, 0.f, 0.f);
				dX += mSpace;
			}
			//NewLine
			else if(text[i] == '\n')
			{
				glTranslatef(x - dX, mNewLine, 0.f);
				dY += mNewLine;
				dX += x - dX;
			}
			//character
			else
			{
				//Get ASCII
				GLuint ascii = (unsigned char) text[i];
				
				//Draw quad using vertex data and index data
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffers[ascii]);
				glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);
				
				//Move over
				glTranslatef(mClips[ascii].w, 0.f, 0.f);
				dX += mClips[ascii].w;
			}
		}
		
		//Disable vertex and texture coordinate arrays
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		
	}
}
