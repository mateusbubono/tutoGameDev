/*

Boris Merminod le 23/04/2019

	On va ajouter à la classe LSpriteSheet la possibilité de changer l'origine de rendu des sprites. Dans le chapitre précédent, ce rendu était effectué depuis le centre du sprite. Ici le rendu sera géré depuis le coin supérieur gauche des sprites
	
	On va ensuite changer le prototype de la fonction generateBuffer :
	
bool generateDataBuffer(LSpriteOrigin origin = LSPRITE_ORIGIN_CENTER);

On indique comme ça que l'origine de rendu par défaut est le centre. Cela va affecter la façon des les vertex datas seront générés

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php

*/

#ifndef LSPRITE_SHEET_H
#define LSPRITE_SHEET_H

#include "LTexture.h"
#include "LVertexData2D.h"
#include <vector>

//Sprite drawning origin
enum LSpriteOrigin
{
	LSPRITE_ORIGIN_CENTER,
	LSPRITE_ORIGIN_TOP_LEFT,
	LSPRITE_ORIGIN_BOTTOM_LEFT,
	LSPRITE_ORIGIN_TOP_RIGHT,
	LSPRITE_ORIGIN_BOTTOM_RIGHT
};

class LSpriteSheet : public LTexture
{
	public :
		LSpriteSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Initialisation de l'ID du Buffer
		Side Effects :
		- None
		*/
		
		~LSpriteSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour le sprite sheet data
		Side Effects :
		- None
		*/
		
		int addClipSprite(LFRect& newClip);
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Ajoute un rectangle de clipping dans une table de clip
		- Retourne l'index du rectangle de clippig dans la table de clip
		Side Effects :
		- None
		*/
		
		LFRect getClip(int index);
		/*
		Pré-conditions :
		- Un index valide
		Post conditions :
		- Retourne un rectangle de clipping depuis la table de clip à un index donné
		Side Effects :
		- None
		*/
		
		bool generateDataBuffer(LSpriteOrigin origin = LSPRITE_ORIGIN_CENTER);
		/*
		Pré-conditions : 
		- Une texture de base LTexture chargée
		- Des rectangles de clipping dans une table de clips
		Post conditions :
		- Génère un VBO et un IBO pour effectuer le rendu de sprites
		- Retourne true en cas de succès
		- Reporting des erreurs sur stderr
		Side Effects :
		- Les buffers membre sont bound
		*/
		
		void freeSheet();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer le VBO, l'IBO et la table des clips
		Side Effects :
		- None
		*/
		
		void freeTexture();
		/*
		Pré-conditions :
		- None
		Post conditions :
		- Libère la mémoire allouée pour gérer le sprite sheet et l'objet LTexture
		Side Effects :
		- None
		*/
		
		void renderSprite(int index);
		/*
		Pré-conditions :
		- Une texture LTexture chargée
		- Un VBO fonctionnel
		Post conditions :
		- Rendu d'un sprite à un index donné
		Side Effects :
		- Texture LTexture de base est bound
		- Le buffer membre est bound
		*/
		
	protected :
		//Sprite clips
		std::vector<LFRect>mClips;
		
		//VBO data
		GLuint mVertexDataBuffer;
		GLuint* mIndexBuffers;		
};

#endif
