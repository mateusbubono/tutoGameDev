/*

Boris Merminod le 23/04/2019

Code source tiré de : http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php

*/

#ifndef LTEX_COORD_H
#define LTEX_COORD_H

#include "LOpenGL.h"

struct LTexCoord
{
	GLfloat s;
	GLfloat t;
};

#endif
