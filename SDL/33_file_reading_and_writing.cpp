/*
Boris Merminod 25/09/2017 

Introduction à la lecture et à l'écriture de fichier à l'aide de la SDL. L'idée ici est que l'on va sauvegarder dans un fichier .bin, un tableau d'entier signé de type Sint32, comme ceci :

Sint32 gData[TOTAL_DATA];

La fonction loadMedia va alors contenir gérer un stream pour l'écriture et la lecture des données :

SDL_RWops* file

On peut ouvrir un flux via SDL_RWFromFile qui prend en argument le path du fichier et le mode de lecture ou d'écriture :
- "r+b" : Lecture d'un format binaire
- "w+b" : Ecriture d'un format binaire
La fonction loadMedia tente donc d'ouvrir un fichier nums.bin en lecture, si elle échoue elle va alors tenter de créer le fichier donc de réaliser une ouverture au format w+b. Si l'ouverture s'est bien passé elle va écrire des données par défaut dans le fichier en initialisant chaque élément de gData à 0 puis en écrivant dans le fichier via la fonction SDL_RWwrite, qui prend en paramètre le stream dans lequel écrire, l'adresse de l'élément à écrire sa taille déterminé par un sizeof(Sint32) et le nombre d'objet à écrire placé à 1 en général. Lorsque l'écriture est terminée au ferme le fichier via SDL_RWclose() qui ferme le stream utilisé.
Si le fichier nums.bin existe alors la fonction loadMedia va le lire via SDL_RWread qui prend en paramètre, le stream de lecture, l'adresse de la variable à assigner, la taille via un sizeof(Sint32) et le nombre d'objet à lire en principe 1. A la fin de la lecture on ferme le stream avec SDL_RWclose().

Lorsque le fichier a bien été lu, on va convertir chaque élément de gData en une texture, comme ceci :

gDataTextures[ 0 ].loadFromRenderedText( std::to_string( (_Longlong)gData[ 0 ] ), highlightColor ); 
for( int i = 1; i < TOTAL_DATA; ++i ) 
	gDataTextures[ i ].loadFromRenderedText( std::to_string( (_Longlong)gData[ i ] ), textColor );

Petite remarque la conversion en string de gData ce fait via un cast de la valeur de gData vers le type _Longlong. Sur mon système le compilateur m'a refusé ce cast, sans doute une particularité de la norme C++11 que j'utilise, voilà pourquoi je l'ai retiré à chaque appel de std::to_string

La fonction close réalise une sauvegarde de gData après les modification subit lors de l'exécution du programme

La fonction main va permettre d'afficher dans une fenêtre les valeurs sauvegardée dans le fichier et de les modifier. Lorsque les valeurs sont celles du fichier sauvegardé, la couleur du/des chiffre(s) apparaît en bleu, le chiffre sur lequel se trouve le "curseur" est en surbrillance grâce à une couleur rouge. Lorsque le curseur est passé sur un chiffre celui-ci est considéré comme modifié et devient noir. Ce procédé est décrit par le code de la fonction main.

Code source tiré de : http://lazyfoo.net/tutorials/SDL/33_file_reading_and_writing/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
//#include <string.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Entier du nombre total de données
const int TOTAL_DATA = 10;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};


// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gPromptTextTexture;
LTexture gDataTextures[TOTAL_DATA];

TTF_Font * gFont = NULL;

//Data points
Sint32 gData[TOTAL_DATA];


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}


// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "init : Impossible d'initialiser SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Rendu textuel de couleur
	SDL_Color textColor = {0,0,0xFF};
	SDL_Color highlightColor = {0xFF, 0,0,0xFF};
	
	//Chargement des sprites et textures	
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}

	//Rendu du prompt
	if(gPromptTextTexture.loadFromRenderedText("Enter Data : ", textColor) == false)
	{
		fprintf(stderr, "loadMedia : Echec de rendu du text de prompt\n");
		return false;
	}
	
	//Ouverture du fichier pour une lecture au format binaire
	SDL_RWops * file = SDL_RWFromFile("sprite/nums.bin", "r+b");
	
	//Si le fichier n'existe pas
	if(file == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible d'ouvrir le fichier ! SDL Error : %s\n", SDL_GetError());
		file = SDL_RWFromFile("sprite/nums.bin", "w+b");
		if(file == NULL)
		{
			fprintf(stderr, "loadMedia : Impossible de créer le fichier ! SDL Error : %s\n", SDL_GetError());
			return false;
		}
		
		//Initialisation des données
		printf("Création d'un nouveau fichier\n");
		for(int i = 0; i<TOTAL_DATA; ++i)
		{
			gData[i] = 0;
			SDL_RWwrite(file, &gData[i], sizeof(Sint32), 1);
		}
		
		//Fermeture du fichier
		SDL_RWclose(file);	
	}
	else //Si le fichier existe
	{
		//Chargement des données
		printf("Lecture du fichier...\n");
		for(int i = 0; i<TOTAL_DATA;++i)
			SDL_RWread(file, &gData[i], sizeof(Sint32), 1);
		
		//Fermeture du fichier
		SDL_RWclose(file);
	}
	
	//Initialisation des données de textures
	gDataTextures[0].loadFromRenderedText(std::to_string( gData[0]), highlightColor);
	for(int i = 1; i <TOTAL_DATA; ++i)
		gDataTextures[i].loadFromRenderedText(std::to_string(gData[i]), textColor);
		
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{

	//Ouverture des données pour l'écriture
	SDL_RWops* file = SDL_RWFromFile("sprite/nums.bin", "w+b");
	if(file !=NULL)
	{
		//Sauvegarde des données
		for(int i=0; i<TOTAL_DATA; ++i)
			SDL_RWwrite(file, &gData[i], sizeof(Sint32), 1);
		
		//Fermeture du fichier
		SDL_RWclose(file);
	}
	else
	{
		fprintf(stderr, "close : Echec de sauvegarde de fichier ! %s\n", SDL_GetError());
	}
	
	//Libération de la mémoire pour les textures
	gPromptTextTexture.free();
	for(int i=0; i<TOTAL_DATA; ++i)
		gDataTextures[i].free();
	
	//Fermeture du font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}


// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//Couleur du texte noir
	SDL_Color textColor = {0,0,0,0xFF};
	SDL_Color highlightColor = {0xFF,0,0,0xFF};
	
	//Current input point
	int currentData = 0;

	while(!quit)
	{
		//Flag de rendu du texte
		bool renderText = false;
		
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			else if(e.type == SDL_KEYDOWN)
			{
				switch(e.key.keysym.sym)
				{
					//Données d'entrées précédent
					case SDLK_UP: 
						//Renouveller le rendu des points d'entrées précédent
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]), textColor);
						--currentData;
						if(currentData<0)
							currentData = TOTAL_DATA-1;
						//Renouveller le rendu du point d'entrée courant
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]), highlightColor);
						break;
					//Données d'entrée suivants
					case SDLK_DOWN :
					//Renouveller le rendu des point d'entrée suivant
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]), textColor);
						++currentData;
						if(currentData == TOTAL_DATA)
							currentData = 0;
					
						//Renouveller le rendu du point d'entrée courant
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]),highlightColor);
						break;
					//Décrémente le point d'entrée
					case SDLK_LEFT :
						--gData[currentData];
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]), highlightColor);
						break;
					//Décrémente le point d'entrée
					case SDLK_RIGHT :
						++gData[currentData];
						gDataTextures[currentData].loadFromRenderedText(std::to_string(gData[currentData]), highlightColor);
						break;
						
						
				}
			
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu des textures
		gPromptTextTexture.render((SCREEN_WIDTH - gPromptTextTexture.getWidth()) / 2, 0);
		
		for(int i = 0; i<TOTAL_DATA; ++i)
			gDataTextures[i].render((SCREEN_WIDTH - gDataTextures[i].getWidth())/2, gPromptTextTexture.getHeight() + gDataTextures[0].getHeight() * i);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

