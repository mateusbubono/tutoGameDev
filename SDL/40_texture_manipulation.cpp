/*
Boris Merminod le 22/11/2017

Introduction à la manipulation de texture. Pour réaliser des effets graphiques, on va souvent avoir besoin de manipuler l'image au niveau des pixels. Dans cet exemple les pixels d'une image vont être modifié en blanc comme la couleur du background.

	La classe Texture se voit ajouter des fonctions de verrouillage et déverrouillage de pixels. Le vérrouillage va permettre de manipuler les pixels qui seront ensuite déverrouillé une fois la manipulation terminée. On a alors une fonction pour obtenir la ligne de pixels et une fonction pour obtenir le pitch, représentant la largeur de la texture en mémoire. Si la largeur d'une image est de 100 pixels, le pitch sera alors de 128 c'est à dire la puissance de 2 juste au-dessus.
	La méthode loadFromFile commence par charger une surface, puis elle va la convertir par rapport au format d'affichage via SDL_ConvertSurfaceFormat. Ensuite elle va créer à partir de cette surface convertie une texture via la fonction SDL_CreateTexture. L'utilisation du flag SDL_TEXTUREACCESS_STREAMING, va permettre l'accès à la modification des pixels
	Une fois les opérations de chargement faites, on va locker les textures via SDL_LockTexture avec en argument la texture, la région à manipuler (NULL s'il s'agit de toute la texture), un pointeur sur le nombre de pixels et un pointeur sur le pitch. Ensuite on copie via memcpy ces pixels avec en argument, la destination de la copie, la source de la copie, la taille en octet de la copie qui correspond à la hauteur de la texture * le pitch. Enfin on unlock la texture après manipulation des pixels
	
	Dans la fonction loadMedia la texture est vérouillée juste après sont chargement pour pouvoir manipuler ces pixels. Ensuite on va passer en revue ces pixels pour les rendre transparent en manipulant leur colorKey. 
		Pour cela on commence par allouer un format de pixel en utilisant la fonction SDL_GetWindowPiexelFormat et SDL_AllocFormat. Ensuite on récupère nos pixels via l'accesseur de notre classe de Texture qui retourne un pointeur sur void que l'on va pouvoir caster en format 32 bits qui est le format de pixel dont on va avoir besoin dans notre exemple. Ensuite on va avoir besoin du nombre de pixels. Pour cela on a le pitch qui est la longueur de la texture en octet. Le pitch nous donne 4 octets par pixels ainsi il suffit de diviser le pitch par 4 pour obtenir la largeur en pixels de la texture. On le multiplie ensuite par la hauteur de la texture pour obtenir le nombre de pixels.
		Par la suite dans une boucle on va rendre tous les pixels possédant le colorKey cyan en transparent. Puis une fois que c'est fait on dévérrouille la texture

Code source tiré de : http://lazyfoo.net/tutorials/SDL/40_texture_manipulation/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
		
		//Pixel manipulators
		bool lockTexture();
		bool unlockTexture();
		void * getPixels();
		int getPitch();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		void * mPixels;
		int mPitch;
		
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gFooTexture;
TTF_Font *gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Conversion de la surface au format d'affichage
	SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_GetWindowPixelFormat(gWindow), NULL);
	if(formattedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de convertir la surface en format d'affichage : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une texture vide streamable
	newTexture = SDL_CreateTexture(gRenderer, SDL_GetWindowPixelFormat(gWindow), SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de créer la texture ! SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Verrouillage de la texture pour la manipulation
	SDL_LockTexture(newTexture, NULL, &mPixels, &mPitch);
	
	//Copie de la surface de pixels copiée/formattée
	memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	//Déverrouillage de la texture pour mise à jour
	SDL_UnlockTexture(newTexture);
	mPixels = NULL;
	
	//Dimensions des images
	mWidth = formattedSurface->w;
	mHeight = formattedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(formattedSurface); 
	SDL_FreeSurface(loadedSurface); 
	
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Accesseur attribut mPixels
void * LTexture::getPixels()
{
	return mPixels;
}

//Accesseur de l'attribut mPitch
int LTexture::getPitch()
{
	return mPitch;
}

bool LTexture::lockTexture()
{
	//La texture est déjà vérrouillée
	if(mPixels != NULL)
	{
		fprintf(stderr, "lockTexture : la texture est déjà vérrouillée\n");
		return false;
	}
	//Vérouillage de la texture
	if(SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
	{
		fprintf(stderr, "lockTexture : Impossible de vérouiller la texture ! %s\n", SDL_GetError());
		return false;
	}
	
	return true;
	
}

bool LTexture::unlockTexture()
{
	if(mPixels == NULL)
	{
		fprintf(stderr, "unlockTexture : La texture n'est pas vérouillée\n");
		return false;
	}
	
	//Déverrouillage de la texture
	SDL_UnlockTexture(mTexture);
	mPixels = NULL;
	mPitch = 0;
	
	return true;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	if(!gFooTexture.loadFromFile("sprite/foo.png"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de texture\n");
		return false;
	}
	
	if(!gFooTexture.lockTexture())
	{
		fprintf(stderr, "loadMedia : Impossible de vérouiller la texture\n");
		return false;
	}
	
	//Allocation d'un format depuis la fenêtre
	Uint32 format = SDL_GetWindowPixelFormat(gWindow);
	SDL_PixelFormat* mappingFormat = SDL_AllocFormat(format);
	
	//Obtenir les donnée des pixels
	Uint32* pixels = (Uint32*)gFooTexture.getPixels();
	int pixelCount = (gFooTexture.getPitch()/4) * gFooTexture.getHeight();
	
	//Map colors
	Uint32 colorKey = SDL_MapRGB(mappingFormat, 0, 0xFF, 0xFF);
	Uint32 transparent = SDL_MapRGBA(mappingFormat, 0xFF, 0xFF, 0xFF, 0x00);
	
	//Color key pixels
	for(int i = 0; i<pixelCount; i++)
		if(pixels[i] == colorKey)
			pixels[i] = transparent;
	
	gFooTexture.unlockTexture();
	
	//Libération du format
	SDL_FreeFormat(mappingFormat);
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	
	//Libération de la mémoire pour les textures
	gFooTexture.free();
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de la figure
		gFooTexture.render((SCREEN_WIDTH - gFooTexture.getWidth())/2, (SCREEN_HEIGHT - gFooTexture.getHeight())/2);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

