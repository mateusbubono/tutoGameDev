/*
Boris Merminod le 

	Introduction au Texture Streaming. Le principe est de permettre d'effectuer le rendu de donnée de pixels depuis une autre source que le bitmap comme depuis une webcam par exemple. Le texture stream permet d'effectuer le rendu depuis n'importe quelle image source.
	
	La classe LTexture se voit étoffée de deux méthodes. La méthode createBlank va créer une texture vide dans laquelle on va copier les données du streaming.
	La classe DataStream va permettre d'exploiter via un buffer les pixels en provenance d'un flux vidéo comme une web cam, un décodeur vidéo, une API etc...
	
	La méthode createBlank de la classe LTexture va créer une texture RGBA 32bit avec un stream access. Le but ici est de s'assurer que les pixels des textures que l'on va effectuer le rendu soit au format des pixels en provenance des streams.
	La méthode copyPixels de LTexture va ensuite copier les pixels du stream vers les pixels de la texture.
	
	Dans la fonction main la stream texture est verrouillée puis copiée dans la texture dont on va effectuer le rendu puis on déverrouille la texture streamé.
	
	Concrètement ce qui se passe dans le détail : On va charger une LTexture vide au dimensions d'une image à streamer, c'est le rôle de la méthode createBlank de la classe LTexture. On va charger dans la classe DataStream les images à Streamer sous la forme de structure SDL_Surface. Ces images ne seront donc pas convertie en texture dès le chargement mais dynamiquement lors du rendu des texture. Au moment du rendu la méthode getBuffer va retourner une des images chargée par DataStream selon le frame en cours. La méthode copyPixels de LTexture va alors copier cette image dans la texture qui sera ensuite rendu via le renderer. C'est le principe du Texture Streaming 

Code source tiré de : http://lazyfoo.net/tutorials/SDL/42_texture_streaming/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Création d'une texture vide
		bool createBlank(int width, int height);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
		
		//Pixel manipulators
		bool lockTexture();
		bool unlockTexture();
		void * getPixels();
		void copyPixels(void * pixels);
		int getPitch();
		Uint32 getPixel32(unsigned int x, unsigned int y);
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		void * mPixels;
		int mPitch;
		
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

class DataStream
{
	public :
		//Constructeur
		DataStream();
		
		//Chargement des données intiales
		bool loadMedia();
		
		//Libération de la mémoire
		void free();
		
		//Obtenir les données de la frame en cours
		void * getBuffer();
	
	private:
		//Données internes
		SDL_Surface * mImages[4];
		int mCurrentImage;
		int mDelayFrames;
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gStreamingTexture;
DataStream gDataStream;
TTF_Font *gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Conversion de la surface au format d'affichage
	SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
	if(formattedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de convertir la surface en format d'affichage : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une texture vide streamable
	newTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de créer la texture ! SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Permettre le blending
	SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
	
	//Verrouillage de la texture pour la manipulation
	SDL_LockTexture(newTexture, &formattedSurface->clip_rect, &mPixels, &mPitch);
	
	//Copie de la surface de pixels copiée/formattée
	memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	//Dimensions des images
	mWidth = formattedSurface->w;
	mHeight = formattedSurface->h;
	
	//Obtenir les données du pixel dans un format modifiable
	Uint32* pixels = (Uint32*)mPixels;
	int pixelCount = (mPitch/4) * mHeight;
	
	//Map color
	Uint32 colorKey = SDL_MapRGB(formattedSurface->format, 0, 0xFF, 0xFF);
	Uint32 transparent = SDL_MapRGBA(formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00);
	
	//Color key pixel
	for(int i=0; i<pixelCount; i++)
	{
		if(pixels[i] == colorKey)
		{
			pixels[i] = transparent;
		}
	}
	
	//Déverrouillage de la texture pour mise à jour
	SDL_UnlockTexture(newTexture);
	mPixels = NULL;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(formattedSurface); 
	SDL_FreeSurface(loadedSurface); 
	
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

bool LTexture::createBlank(int width, int height)
{
	//Création d'une texture non initialisée
	mTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, width, height);
	if(mTexture == NULL)
	{
		fprintf(stderr, "LTexture::createBlank : Impossible de créer la texture vide ! SDL Error : %s\n", SDL_GetError());
	}
	else
	{
		mWidth = width;
		mHeight = height;
	}
	
	return mTexture != NULL;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Accesseur attribut mPixels
void * LTexture::getPixels()
{
	return mPixels;
}

void LTexture::copyPixels(void * pixels)
{
	//Copie des pixels verrouillés
	memcpy(mPixels, pixels, mPitch * mHeight);
}

//Accesseur mPixels au format Uint32
Uint32 LTexture::getPixel32(unsigned int x, unsigned int y)
{
	//Convert the pixels to 32 bit
	Uint32 * pixels = (Uint32*)mPixels;
	
	//Get the pixel requested
	return pixels[(y * (mPitch/4)) + x];
}

//Accesseur de l'attribut mPitch
int LTexture::getPitch()
{
	return mPitch;
}

bool LTexture::lockTexture()
{
	//La texture est déjà vérrouillée
	if(mPixels != NULL)
	{
		fprintf(stderr, "lockTexture : la texture est déjà vérrouillée\n");
		return false;
	}
	//Vérouillage de la texture
	if(SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
	{
		fprintf(stderr, "lockTexture : Impossible de vérouiller la texture ! %s\n", SDL_GetError());
		return false;
	}
	
	return true;
	
}

bool LTexture::unlockTexture()
{
	if(mPixels == NULL)
	{
		fprintf(stderr, "unlockTexture : La texture n'est pas vérouillée\n");
		return false;
	}
	
	//Déverrouillage de la texture
	SDL_UnlockTexture(mTexture);
	mPixels = NULL;
	mPitch = 0;
	
	return true;
}

// ~~~~~ Méthodes de la classe DataStream ~~~~~

//Constructeur
DataStream::DataStream()
{
	for(int i = 0; i<4; i++)
		mImages[i] = NULL;
		
	mCurrentImage = 0;
	mDelayFrames = 4;
}

//Chargement des images
bool DataStream::loadMedia()
{
	for(int i = 0; i<4; i++)
	{
		char path[64] = "";
		sprintf(path, "sprite/foo_walk_%d.png", i);
		SDL_Surface * loadedSurface = IMG_Load(path);
		if(loadedSurface == NULL)
		{
			fprintf(stderr, "DataStream::loadMedia : Impossible de lire %s ! SDL_image error : %s\n", path, IMG_GetError());
			return false;
		}
		
		mImages[i] = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
		SDL_FreeSurface(loadedSurface);
	}
	
	return true;
}

//Libération de la mémoire allouée
void DataStream::free()
{
	for(int i = 0; i<4; i++)
	{
		if(mImages[i] == NULL)
			continue;
		SDL_FreeSurface(mImages[i]);
	}
}

void * DataStream::getBuffer()
{
	mDelayFrames--;
	if(mDelayFrames == 0)
	{
		mCurrentImage++;
		mDelayFrames = 4;
	}
	
	if(mCurrentImage == 4)
		mCurrentImage = 0;
		
	return mImages[mCurrentImage]->pixels;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Seed random
	srand(SDL_GetTicks());
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	//Chargement d'une texture vide
	if(!gStreamingTexture.createBlank(64,205))
	{
		fprintf(stderr, "loadMedia : Echec de création d'une streaming texture\n");
		return false;
	}
	
	//Chargement du data stream
	if(!gDataStream.loadMedia())
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une data stream\n");
		return false;
	}
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	
	//Libération de la mémoire pour les textures
	gStreamingTexture.free();
	gDataStream.free();
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de la figure
		gStreamingTexture.lockTexture();
		gStreamingTexture.copyPixels(gDataStream.getBuffer());
		gStreamingTexture.unlockTexture();
		
		//Render frame
		gStreamingTexture.render((SCREEN_WIDTH - gStreamingTexture.getWidth()) / 2, (SCREEN_HEIGHT - gStreamingTexture.getHeight()) / 2 );
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

