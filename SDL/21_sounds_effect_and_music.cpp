/*
Boris Merminod 18/09/2017

Introduction à la gestion du son par SDL_mixer. SDL_mixer permet de gérer des musiques et des effets sonores. Les effets sonores sont de 4 types : les scratch (sons très très court) , les low (sont court), les medium (sons moyens), les high (sons longs). La musique est géré via une structure du type Mix_Music et les effets sonores avec une structure du type Mix_Chunk.

Dans la fonction init la fonction SDL_Init doit initialiser le mode audio via SDL_INIT_AUDIO, puis on utilise la fonction Mix_OpenAudio pour initialiser la bibliothèque de SDL_Mixer. Cette fonction prend 4 paramètres : la fréquence de sortie, la fréquence d'échantillonage, la chaîne de sortie (1 = mono, 2 = stéréo) et la taille en octets de l'échantillon de sortie.

Dans la fonction loadMedia, la fonction Mix_LoadMUS va permettre le chargement d'une musique, la fonction Mix_LoadWAV permet de charger les effets sonores au format .wav

Dans la fonction close on libère la mémoire utilisée par les effets sonores et la musiques à l'aide de Mix_FreeMusic et de Mix_FreeChunk

Dans la fonction main on peut observer comment sont joué les effets sonores et la musique. Pour les effets sonore il s'agit de la fonction Mix_PlayChannel qui prend trois paramètres : La chaîne sur laquelle jouer, à -1 la fonction prend la première chaîne libre, la structure Mix_Chunk qui contient l'effet sonore à jouer et le nombre de fois qu'il faut répéter l'effet sonore.
Au niveau de la musique la fonction Mix_PlayingMusic permet de savoir si une musique est jouée actuellement, la fonction Mix_PausedMusic elle permet de savoir si une musique est en pause. A partir de la, la fonction Mix_PlayMusic va lancer une musique d'une structure Mix_Music, sont second paramètre permet de savoir s'il faut boucler cette musique (à -1). La fonction Mix_PauseMusic permet de mettre la musique en pause, et la fonction Mix_ResumeMusic permet de reprendre une musique jouée en pause. Enfin la fonction Mix_HaltMusic permet d'arrêter la musique en cours


Code source tiré de : http://lazyfoo.net/tutorials/SDL/21_sound_effects_and_music/index.php
Autres liens : 
https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_11.html
https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h> //Gestion audio
//#include <SDL2/SDL_ttf.h> // Gestion des font string 
#include <stdio.h>
#include <string.h>
#include <iostream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		#ifdef _SDL_TTH_H
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		#endif
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gPromptTexture;

//Musique qui sera jouée
Mix_Music *gMusic = NULL;

//Les sounds effects
Mix_Chunk *gScratch = NULL;
Mix_Chunk *gHigh = NULL;
Mix_Chunk *gMedium = NULL;
Mix_Chunk *gLow = NULL;

// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}
#endif

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_mixer
	if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		fprintf(stderr, "init : Echec de l'initialisation de SDL_mixer ! SDL_mixer Error : %s \n", Mix_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	if(gPromptTexture.loadFromFile("sprite/prompt.png") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une texture\n");
		return false;
	}
	
	//Chargement d'une musique
	gMusic = Mix_LoadMUS("sprite/beat.wav");
	if(gMusic == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement de la musique ! SDL_mixer Error : %s\n", Mix_GetError());
		return false;
	}
	
	//Chargement des sound effects
	
	gScratch = Mix_LoadWAV("sprite/scratch.wav");
	if(gScratch == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'un scratch sound effect ! SDL_mixer Error : %s\n", Mix_GetError());
		return false;
	}
	
	gHigh = Mix_LoadWAV("sprite/high.wav");
	if(gHigh == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'un high sound effect ! SDL_mixer Error : %s\n", Mix_GetError());
		return false;
	}
	
	gMedium = Mix_LoadWAV("sprite/medium.wav");
	if(gHigh == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'un medium sound effect ! SDL_mixer Error : %s\n", Mix_GetError());
		return false;
	}
	
	gLow = Mix_LoadWAV("sprite/low.wav");
	if(gHigh == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'un low sound effect ! SDL_mixer Error : %s\n", Mix_GetError());
		return false;
	}
		
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour les textures
	gPromptTexture.free();
	
	//Free the sound effects
	Mix_FreeChunk(gScratch);
	gScratch = NULL;
	Mix_FreeChunk(gHigh);
	gHigh = NULL;
	Mix_FreeChunk(gMedium);
	gMedium = NULL;
	Mix_FreeChunk(gLow);
	gLow = NULL;
	
	//Free the music
	Mix_FreeMusic(gMusic);
	gMusic = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//Direction du joystick
	int xDir = 0;
	int yDir = 0;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			else if(e.type == SDL_KEYDOWN)
			{
				switch(e.key.keysym.sym)
				{
					case SDLK_a: //Joue le High sound effect
						Mix_PlayChannel(-1, gHigh, 0);
						break;
					case SDLK_z: //Joue le Medium sound effect
						Mix_PlayChannel(-1, gMedium, 0);
						break;
					case SDLK_e : //Joue le Low sound effect
						Mix_PlayChannel(-1, gLow, 0);
						break;
					case SDLK_r : //Joue le scratch sound effect
						Mix_PlayChannel(-1, gScratch, 0);
						break;
					case SDLK_o :
						if(Mix_PlayingMusic() == 0) // S'il n'y a pas de musique
						{
							// Joue la musique
							Mix_PlayMusic(gMusic, -1);
						}
						else
						{
							//Sinon si la musique est en pause
							if(Mix_PausedMusic() == 1)
								Mix_ResumeMusic(); // alors reprends la musique
							else
								Mix_PauseMusic(); //Sinon met la musique en pause
						}
						break;
					case SDLK_p :
						//Arrête la musique
						Mix_HaltMusic();
						break;
				}
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de texture
		gPromptTexture.render(0,0);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

