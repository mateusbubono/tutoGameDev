/*
Boris Merminod 25/09/2017 

Introduction à la gestion d'événements pour la fenêtre. Pour commencer on va créer une classe LWindow qui va permettre de gérer tout autour de la fenêtre, de la création à la destruction ainsi que la gestion des événements. La création du Renderer ce fera également via la classe LWindow. Les événements autour de la fenêtre comprenne le redimensionnement, l'obtention du focus de la souris ou du clavier.

La création d'une fenêtre est gérée par la méthode init de la classe LWindow en utilisant la fonction SDL_CreateWindow. Utilisant un flag SDL_WINDOW_RESIZABLE pour permettre le redimensionnement de la fenêtre
La méthode createRenderer va créer un renderer pour notre fenêtre via l'utilisation de la fonction SDL_CreateRenderer

Les événement sont géré par la méthode handleEvent qui va testé le déclenchement d'un événement window : SDL_WINDOWEVENT. De la on va tester le type d'événement. 

Code source tiré de : http://lazyfoo.net/tutorials/SDL/35_window_events/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
//#include <string.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

class LWindow
{
	public :
		//Initialisation de l'objet
		LWindow();
		
		//Création de fenêtres
		bool init();
		
		//Créer un renderer depuis une fenêtre interne
		SDL_Renderer* createRenderer();
		
		//Gestion des window events
		void handleEvent(SDL_Event & e);
		
		//Libération de la mémoire allouée
		void free();
		
		//Accesseur des dimensions de la fenêtre
		int getWidth();
		int getHeight();
		
		//Window focii
		bool hasMouseFocus();
		bool hasKeyboardFocus();
		bool isMinimized();
		
	private :
	
		//Structure de données de type fenêtre
		SDL_Window* mWindow;
		
		//Dimensions de la fenêtre
		int mWidth;
		int mHeight;
		
		//Window focus
		bool mMouseFocus;
		bool mKeyboardFocus;
		bool mFullScreen;
		bool mMinimized;
};


// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
LWindow gWindow;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gSceneTexture;
TTF_Font * gFont = NULL;



// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

// ~~~~~ Méthodes de la classe LWindow ~~~~~

//Initialisation de l'objet LWindow
LWindow::LWindow()
{
	mWindow = NULL;
	mMouseFocus = false;
	mKeyboardFocus = false;
	mFullScreen = false;
	mMinimized = false;
	mWidth = 0;
	mHeight = 0;
}

//Création d'une fenêtre
bool LWindow::init()
{
	mWindow = SDL_CreateWindow("Tutoriel SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(mWindow != NULL)
	{
		mMouseFocus = true;
		mKeyboardFocus = true;
		mWidth = SCREEN_WIDTH;
		mHeight = SCREEN_HEIGHT;
	}
	
	return mWindow != NULL;	
}

//Création d'un renderer pour la fenêtre
SDL_Renderer* LWindow::createRenderer()
{
	return SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

//Gestion des window events

void LWindow::handleEvent(SDL_Event & e)
{
	//Un window event se produit
	if(e.type == SDL_WINDOWEVENT)
	{
		bool updateCaption = false;
		switch(e.window.event)
		{
			//Changement de dimension d'une fenêtre
			case SDL_WINDOWEVENT_SIZE_CHANGED :
				mWidth = e.window.data1;
				mHeight = e.window.data2;
				SDL_RenderPresent(gRenderer);
				break;
			
			//Repaint on exposure
			case SDL_WINDOWEVENT_EXPOSED :
				SDL_RenderPresent(gRenderer);
				break;
			
			// La souris entre dans la fenêtre
			case SDL_WINDOWEVENT_ENTER :
				mMouseFocus = true;
				updateCaption = true;
				break;
			
			//La souris sort de la fenêtre
			case SDL_WINDOWEVENT_LEAVE :
				mMouseFocus = false;
				updateCaption = true;
			
			//La fenêtre possède le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_GAINED :
				mKeyboardFocus = true;
				updateCaption = true;
				break;
			
			//La fenêtre perd le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_LOST :
				mKeyboardFocus = false;
				updateCaption = true;
				break;
			
			//La fenêtre est réduite
			case SDL_WINDOWEVENT_MINIMIZED :
				mMinimized = true;
				break;
			
			//La fenêtre est agrandie 
			case SDL_WINDOWEVENT_MAXIMIZED : 
				mMinimized = false;
				break;	
		}
		
		//Mise à jour du window caption avec de nouvelle données
		if(updateCaption)
		{
			std::stringstream caption;
			caption << "SDL Tutorial - MouseFocus" << ((mMouseFocus)?"On":"Off") << " KeyboardFocus:"<<((mKeyboardFocus)?"On":"Off");
			SDL_SetWindowTitle(mWindow, caption.str().c_str());
		}
	}
	//Entrer ou sortie du mode full screen via la touche de retour
	else if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
	{
		if(mFullScreen)
		{
			SDL_SetWindowFullscreen(mWindow, SDL_FALSE);
			mFullScreen = false;
		}
		else
		{
			SDL_SetWindowFullscreen(mWindow, SDL_TRUE);
			mFullScreen = true;
			mMinimized = true;
		}
	}
	
}

//Libérer la mémoire allouée pour créer la fenêtre
void LWindow::free()
{
	if(mWindow != NULL)
		SDL_DestroyWindow(mWindow);
	
	mMouseFocus = false;
	mKeyboardFocus = false;
	mWidth = 0;
	mHeight = 0;
}

//Accesseur de la longueur de la fenêtre
int LWindow::getWidth()
{
	return mWidth;
}

//Accesseur de la hauteur de la fenêtre
int LWindow::getHeight()
{
	return mHeight;
}

//Accesseur la fenêtre possède-t'elle le focus de la souris ?
bool LWindow::hasMouseFocus()
{
	return mMouseFocus;
}

//Accesseur la fenêtre possède-t'elle le focus du clavier
bool LWindow::hasKeyboardFocus()
{
	return mKeyboardFocus;
}

//Accesseur la fenêtre est-elle réduite ?
bool LWindow::isMinimized()
{
	return mMinimized;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	if(gWindow.init() == false)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = gWindow.createRenderer();
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	if(gSceneTexture.loadFromFile("sprite/window.png") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une texture\n");
		return false;
	}
		
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{

	//Suppression des textures
	gSceneTexture.free();
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	gWindow.free();
	
	//Fermeture de la SDL
	IMG_Quit();
	SDL_Quit();
}


// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;

	while(!quit)
	{
		
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			gWindow.handleEvent(e);
		}
		
		//Pas de rendu si la fenêtre est réduite
		if(gWindow.isMinimized() == true)
			continue;
			
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu des texture
		gSceneTexture.render((gWindow.getWidth() - gSceneTexture.getWidth()) / 2, (gWindow.getHeight() - gSceneTexture.getHeight()) / 2);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

