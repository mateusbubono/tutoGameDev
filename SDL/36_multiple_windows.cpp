/*
Boris Merminod 25/09/2017 

Gestion de plusieurs fenêtre. Pour la clarté du code j'ai effacé la classe LTexture. La classe LWindow elle se voit étoffée de quelques méthodes supplémentaire pour mieux gérer le rendu dans chaque fenêtre et savoir lorsqu'une fenêtre est réduite ou non.

On va créer ensuite trois fenêtre globale 

LWindow gWindows[ TOTAL_WINDOWS ];

La méthode init de la classe LWindow va donc utiliser une fonction SDL_GetWindowID() pour attribuer un ID à une fenêtre créée

La méthode handleEvent va donc détecter chaque événement de type SDL_WINDOW_EVENT, puis va focaliser sur la fenêtre ciblée grâce à ID récupéré depuis le SDL_Event e de la façon suivante e.window.windowID == mWindowID

Code source tiré de : http://lazyfoo.net/tutorials/SDL/36_multiple_windows/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
//#include <string.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Nombre total de fenêtres
const int TOTAL_WINDOWS = 3;

class LWindow
{
	public :
		//Initialisation de l'objet
		LWindow();
		
		//Création de fenêtres
		bool init();
		
		//Gestion des window events
		void handleEvent(SDL_Event & e);
		
		//Rendu du contenu des fenêtres
		void render();
		
		//Focus sur la fenêtre
		void focus();
		
		//Libération de la mémoire allouée
		void free();
		
		//Accesseur des dimensions de la fenêtre
		int getWidth();
		int getHeight();
		
		//Window focii
		bool hasMouseFocus();
		bool hasKeyboardFocus();
		bool isMinimized();
		bool isShown();
		
	private :
	
		//Structure de données de type fenêtre
		SDL_Window* mWindow;
		SDL_Renderer * mRenderer;
		int mWindowID;
		
		//Dimensions de la fenêtre
		int mWidth;
		int mHeight;
		
		//Window focus
		bool mMouseFocus;
		bool mKeyboardFocus;
		bool mFullScreen;
		bool mMinimized;
		bool mShown;
};


// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
LWindow gWindows[TOTAL_WINDOWS];


// ~~~~~ Méthodes de la classe LWindow ~~~~~

//Initialisation de l'objet LWindow
LWindow::LWindow()
{
	mWindow = NULL;
	mRenderer = NULL;
	
	mMouseFocus = false;
	mKeyboardFocus = false;
	mFullScreen = false;
	mMinimized = false;
	mShown = false;
	
	mWindowID = -1;
	mWidth = 0;
	mHeight = 0;
}

//Création d'une fenêtre et de son renderer
bool LWindow::init()
{
	mWindow = SDL_CreateWindow("Tutoriel SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(mWindow != NULL)
	{
		mMouseFocus = true;
		mKeyboardFocus = true;
		mWidth = SCREEN_WIDTH;
		mHeight = SCREEN_HEIGHT;
		
		//Création du renderer
		mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if(mRenderer == NULL)
		{
			fprintf(stderr, "init : Echec de création du renderer ! SDL Error : %s\n", SDL_GetError());
			SDL_DestroyWindow(mWindow);
			mWindow = NULL;
		}
		else
		{
			SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
			//Grab window identifier
			mWindowID = SDL_GetWindowID(mWindow);
			
			//Flag d'ouverture de la fenêtre
			mShown = true;
		}
	}
	else
	{
		fprintf(stderr, "init : Impossible de créer la fenêtre ! SDL Error : %s\n", SDL_GetError());
	}
	
	return mWindow != NULL && mRenderer != NULL;	
}

//Gestion des window events

void LWindow::handleEvent(SDL_Event & e)
{
	//Un window event se produit 
	if(e.type == SDL_WINDOWEVENT && e.window.windowID == mWindowID)
	{
		bool updateCaption = false;
		switch(e.window.event)
		{
			//La fenêtre apparaît
			case SDL_WINDOWEVENT_SHOWN :
				mShown = true;
				break;
				
			//La fenêtre disparaît
			case SDL_WINDOWEVENT_HIDDEN :
				mShown = false;
				break;
				
			//Changement de dimension d'une fenêtre
			case SDL_WINDOWEVENT_SIZE_CHANGED :
				mWidth = e.window.data1;
				mHeight = e.window.data2;
				SDL_RenderPresent(mRenderer);
				break;
			
			//Repaint on exposure
			case SDL_WINDOWEVENT_EXPOSED :
				SDL_RenderPresent(mRenderer);
				break;
			
			// La souris entre dans la fenêtre
			case SDL_WINDOWEVENT_ENTER :
				mMouseFocus = true;
				updateCaption = true;
				break;
			
			//La souris sort de la fenêtre
			case SDL_WINDOWEVENT_LEAVE :
				mMouseFocus = false;
				updateCaption = true;
			
			//La fenêtre possède le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_GAINED :
				mKeyboardFocus = true;
				updateCaption = true;
				break;
			
			//La fenêtre perd le focus du clavier
			case SDL_WINDOWEVENT_FOCUS_LOST :
				mKeyboardFocus = false;
				updateCaption = true;
				break;
			
			//La fenêtre est réduite
			case SDL_WINDOWEVENT_MINIMIZED :
				mMinimized = true;
				break;
			
			//La fenêtre est agrandie 
			case SDL_WINDOWEVENT_MAXIMIZED : 
				mMinimized = false;
				break;	
			
			//La fenêtre est restaurée
			case SDL_WINDOWEVENT_RESTORED :
				mMinimized = false;
				break;
			
			//Cache lors d'une fermeture
			case SDL_WINDOWEVENT_CLOSE :
				SDL_HideWindow(mWindow);
				break;
		}
		
		//Mise à jour du window caption avec de nouvelle données
		if(updateCaption)
		{
			std::stringstream caption;
			caption << "SDL Tutorial - MouseFocus" << ((mMouseFocus)?"On":"Off") << " KeyboardFocus:"<<((mKeyboardFocus)?"On":"Off");
			SDL_SetWindowTitle(mWindow, caption.str().c_str());
		}
	}
	
}

//Focus sur une fenêtre
void LWindow::focus()
{
	//Restaure la fenêtre si nécessaire
	if(!mShown)
		SDL_ShowWindow(mWindow);
	//Place la fenêtre devant
	SDL_RaiseWindow(mWindow);
}

void LWindow::render()
{
	if(mMinimized == true)
		return;
	//Clear the screen
	SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(mRenderer);
	
	//Update screen
	SDL_RenderPresent(mRenderer);
}

//Libérer la mémoire allouée pour créer la fenêtre
void LWindow::free()
{
	if(mWindow != NULL)
		SDL_DestroyWindow(mWindow);
	
	mMouseFocus = false;
	mKeyboardFocus = false;
	mWidth = 0;
	mHeight = 0;
}

//Accesseur de la longueur de la fenêtre
int LWindow::getWidth()
{
	return mWidth;
}

//Accesseur de la hauteur de la fenêtre
int LWindow::getHeight()
{
	return mHeight;
}

//Accesseur la fenêtre possède-t'elle le focus de la souris ?
bool LWindow::hasMouseFocus()
{
	return mMouseFocus;
}

//Accesseur la fenêtre possède-t'elle le focus du clavier
bool LWindow::hasKeyboardFocus()
{
	return mKeyboardFocus;
}

//Accesseur la fenêtre est-elle réduite ?
bool LWindow::isMinimized()
{
	return mMinimized;
}

//Accesseur la fenêtre est-elle visible
bool LWindow::isShown()
{
	return mShown;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	if(gWindows[0].init() == false)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	for(int i=0; i<TOTAL_WINDOWS; ++i)
		gWindows[i].free();
		
	SDL_Quit();
}


// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Initialisation des fenêtres
	for(int i=1; i<TOTAL_WINDOWS;++i)
		gWindows[i].init();
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;

	while(!quit)
	{
		
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			//Gestion des événement des fenêtres
			for(int i=0; i<TOTAL_WINDOWS; ++i)
				gWindows[i].handleEvent(e);
			
			//Tirer vers l'avant une des fenêtre
			if(e.type == SDL_KEYDOWN)
			{
				switch(e.key.keysym.sym)
				{
					case SDLK_a :
						gWindows[0].focus();
						break;
					case SDLK_b :
						gWindows[1].focus();
						break;
					case SDLK_c :
						gWindows[2].focus();
						break;
					
				}
			}
		}
		
		//Mise à jour de toutes les fenêtres
		for(int i=0; i<TOTAL_WINDOWS; ++i)
			gWindows[i].render();
		
		//Check de toutes les fenêtres
		bool allWindowsClosed = true;
		for(int i=0; i<TOTAL_WINDOWS; ++i)
			if(gWindows[i].isShown())
				allWindowsClosed = false;
		
		//Fermeture si toutes les fenêtre sont fermées
		if(allWindowsClosed)
			quit = true;
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

