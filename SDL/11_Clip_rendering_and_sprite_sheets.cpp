/*
Boris Merminod 10/09/2017

Le code ici montre comment gérer l'affichage de plusieurs sprites organisés dans une image en .png par exemple (voir le fichier dots.png). Le principe est décrit dans la fonction loadMedia et render de la classe Texture. Dans loadMedia une structure globale nommée gSpriteClips de type SDL_Rect est un tableau qui va cibler chaque sprite à décomposer de l'image par rapport à la dimension de se sprite dans l'image et sa position dans l'image. La fonction render va ensuite utiliser via la fonction SDL_RenderCopy deux structures de types SDL_Rect. La première est gSpriteClips qui cible le sprite à afficher depuis l'image. Le second est l'affichage de se sprite via le renderer.

Code source tiré de : http://lazyfoo.net/tutorials/SDL/11_clip_rendering_and_sprite_sheets/index.php

*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

bool init();
bool loadMedia();
void close();

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		//Désallouer une image
		void free();
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement d'une texture");
		return false;
	}
	
	//Color key image
	//Il s'agit de gérer la transparence d'une certaine couleurs de l'image. Par exemple si un sprite est construit sur un fond cyan, cette fonction va permettre de rendre le fond transparent pour l'affichage du sprite.
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopy(gRenderer, mTexture, clip, &renderQuad);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Textures et sprites de la scène

SDL_Rect gSpriteClips[4];
LTexture gSpriteSheetTexture;


int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu du sprite supérieur gauche
		gSpriteSheetTexture.render(0, 0, &gSpriteClips[0]);
		//Rendu du sprite supérieur droit
		gSpriteSheetTexture.render(SCREEN_WIDTH-gSpriteClips[1].w, 0, &gSpriteClips[1]);
		//Rendu du sprite inférieur gauche
		gSpriteSheetTexture.render(0, SCREEN_HEIGHT-gSpriteClips[2].h, &gSpriteClips[2]);
		//Rendu du sprite inférieur droit
		gSpriteSheetTexture.render(SCREEN_WIDTH-gSpriteClips[3].w, SCREEN_HEIGHT-gSpriteClips[3].h, &gSpriteClips[3]);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	return true;
}

//Pas de chargement de média dans cette fonction
bool loadMedia()
{
	bool success = true;
	
	if(gSpriteSheetTexture.loadFromFile("sprite/dots.png") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'un sprite en texture");
		success = false;
	}
	else
	{
		//Positionnement du sprite supérieur gauche
		gSpriteClips[0].x = 0;
		gSpriteClips[0].y = 0;
		gSpriteClips[0].w = 100;
		gSpriteClips[0].h = 100;
		
		//Positionnement du sprite supérieur droit
		gSpriteClips[1].x = 100;
		gSpriteClips[1].y = 0;
		gSpriteClips[1].w = 100;
		gSpriteClips[1].h = 100;
		
		//Positionnement du sprite inférieur gauche
		gSpriteClips[2].x = 0;
		gSpriteClips[2].y = 100;
		gSpriteClips[2].w = 100;
		gSpriteClips[2].h = 100;
		
		//Positionnement du sprite inférieur droit
		gSpriteClips[3].x = 100;
		gSpriteClips[3].y = 100;
		gSpriteClips[3].w = 100;
		gSpriteClips[3].h = 100;
	}
		
	return success;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour les textures
	gSpriteSheetTexture.free();
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	IMG_Quit();
	SDL_Quit();
}
