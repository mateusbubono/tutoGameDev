/*
Boris Merminod 31/07/2017

Utilisation de la librairie d'extension SDL_image pour charger une image .png.

*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h> //Ajout du header suivant si cela ne fonctionner pas essayer #include <SDL_image.h> directement
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

bool init();
bool loadMedia();
void close();
SDL_Surface * loadSurface(string path);


// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
SDL_Surface * gScreenSurface = NULL;
SDL_Surface * gPNGSurface = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}

		
		//Blit de la surface
		SDL_BlitSurface(gPNGSurface, NULL, gScreenSurface, NULL);
		//Mise à jour de la fenêtre après Blit des surfaces
		SDL_UpdateWindowSurface(gWindow);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Initialisation de SDL_image pour la gestion d'image PNG
	if(!IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)
	{
		fprintf(stderr, "init : Echec d'initialisation de SDL_image : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	//Associer une surface à la fenêtre (Cette surface va permettre de blitter d'autre surfaces)
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	
	return true;
}

//Chargement des images .bmp
bool loadMedia()
{
	
	gPNGSurface = loadSurface("sprite/loaded.png");
	if(gPNGSurface == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	return true;
}

//Gestion des images à charger
SDL_Surface * loadSurface(string path)
{
	SDL_Surface * optimizedSurface = NULL;
	
	//Utilisation de IMG_Load pour charger une image .png (plusieurs autres formats sont possibles)
	SDL_Surface * loadedSurface = IMG_Load(path.c_str()); 
	if (loadedSurface == NULL)
	{
		//En cas d'erreur, elle sont relevée par IMG_GetError()
		fprintf(stderr, "loadSurface : Echec de chargement de l'image %s. SDL Error : %s\n", path, IMG_GetError());
		return NULL;
	}
	
	//La fonction retourne une copie de la surface optimisée
	optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
	if(optimizedSurface == NULL)
	{
		fprintf(stderr, "loadSurface : Echec d'optimisation de surface. SDL Error : %s\n", SDL_GetError());
		return NULL;
	}
	//La Surface original étant inutile on la libère
	SDL_FreeSurface(loadedSurface);
	
	return optimizedSurface;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	// Libération des surfaces
	SDL_FreeSurface(gPNGSurface);
	gPNGSurface = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	SDL_Quit();
}
