/*
Boris Merminod 31/07/2017

Ajout de fonction d'initialisation de la SDL + création fenêtre
Ajout fonction chargement d'image .bmp
Ajout dans le main de fonction de blit des surfaces
Ajout de la fonction close qui libère les surfaces, ferme la fenêtre et ferme la SDL

Code source tiré de : http://lazyfoo.net/tutorials/SDL/02_getting_an_image_on_the_screen/index.php

*/

#include <SDL2/SDL.h>
#include <stdio.h>

bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
SDL_Surface * gScreenSurface = NULL;
SDL_Surface * gHelloWorld = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	//Blit des surfaces gHelloWorld sur une surface 
	SDL_BlitSurface(gHelloWorld, NULL, gScreenSurface, NULL);
	
	//Mise à jour de la fenêtre après Blit des surfaces
	SDL_UpdateWindowSurface(gWindow);
	SDL_Delay(2000);
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	//Associer une surface à la fenêtre (Cette surface va permettre de blitter d'autre surfaces)
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	
	return true;
}

//Chargement d'une image .bmp
bool loadMedia()
{
	//Chargement d'une image .bmp associée à une surface
	gHelloWorld = SDL_LoadBMP("sprite/hello_world.bmp");
	if (gHelloWorld == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement de m'image %s, SDL Error : %s\n", "hello_world.bmp", SDL_GetError());
		return false;
	}
	
	return true;
}

//Libération de la mémoire et fermeture de la SDL
void close()
{
	// Libération de la surface gHelloWorld
	SDL_FreeSurface(gHelloWorld);
	gHelloWorld = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	SDL_Quit();
}
