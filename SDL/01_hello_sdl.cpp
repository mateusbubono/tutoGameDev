/*
Boris Merminod 31/07/2017

Code minimal de présentation de la bibliothèque SDL. 
Code source tiré de  : http://lazyfoo.net/tutorials/SDL/01_hello_SDL/index.php
*/

#include <SDL2/SDL.h> //Si cela ne fonctionne pas remplacer par <SDL.h>
#include <stdio.h>

//Dimension de la fenêtre
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int argc, char* argv[])
{
	SDL_Window * window = NULL;
	SDL_Surface *screenSurface = NULL;
	
	//On commence par initialiser la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Echec d'initialisation de la SDL ! SDL_Error : %s\n", SDL_GetError());
	}
	else
	{
		//En cas de réussite on construit la fenêtre
		window = SDL_CreateWindow("Tutoriel SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	
		if(window == NULL)
		{
			printf("Impossible de créer une fenêtre ! SDL_Error : %s\n", SDL_GetError()) ;
		}
		else
		{
			//Si la fenêtre se construit on lui ajoute une surface
			screenSurface = SDL_GetWindowSurface(window);
			//On remplit la surface avec une couleur, ici blanche
			SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));
			//Les modifications ne seront prise en compte qu'après mise à jour via la fonction ci-dessous
			SDL_UpdateWindowSurface(window);
			//On attend deux secondes
			SDL_Delay(2000);
		}
		
	}
	
	//On libère la mémoire allouée pour la fenêtre
	SDL_DestroyWindow(window);
	// On Stoppe la SDL avant de fermer le programme
	SDL_Quit();
	return 0;
	
	return 0;
}
