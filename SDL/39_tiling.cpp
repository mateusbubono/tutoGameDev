/*
Boris Merminod le 16.11.2017

Introduction au tiling. Le principe du tiling est de composer un niveau en utilisant des pièces réutilisables de sprites. Pour se faire on va utiliser un tile set, il s'agit d'une image dans lequel on va trouver plusieurs sprites appelé des tiles ou mosaïques en français. Ses tiles vont être associées à des valeurs qui vont permettre de construire des niveaux. Ses valeurs sont entrées dans un documents portant une extension .map. A la lecture les valeurs du .map sont remplacés par le tile correspondant via une opération de clipping sur l'image .png

La première partie du code défini des constantes définissant la taille du niveau, la taille de l'écran ainsi que plusieurs constantes décrivant les différent types de tile

Une classe Tile est définie ici, elle permet de gérer la position, le rendu et les collisions. Dans ce cas précis, position et box de collision sont groupés en un seul et même paramètre, mais il est plus courant et plus efficace de séparer les deux.

La classe Dot est mise à jout notamment pour gérer les collisions avec les tiles

Plusieurs fonctions ont été ajoutée. La fonction touchesWall va tester les collisions de tous les murs possibles du tile set. 

La fonction setTiles va charger tous les tiles du tile set et les associer à une valeur qui devrait ensuite correspondre au fichier .map. Cette fonction commence par déclarer un offset x et y à 0. Cette offset va subir un décalage en x et y (en fonction de la taille par défaut des TILES) à mesure que chaque tile est charger. Voici le contenu du fichier lazy.map :

00 01 02 00 01 02 00 01 02 00 01 02 00 01 02 00
01 02 00 01 02 00 01 02 00 01 02 00 01 02 00 01
02 00 11 04 04 04 04 04 04 04 04 04 04 05 01 02
00 01 10 03 03 03 03 03 03 03 03 03 03 06 02 00
01 02 10 03 08 08 08 08 08 08 08 03 03 06 00 01
02 00 10 06 00 01 02 00 01 02 00 10 03 06 01 02
00 01 10 06 01 11 05 01 02 00 01 10 03 06 02 00
01 02 10 06 02 09 07 02 00 01 02 10 03 06 00 01
02 00 10 06 00 01 02 00 01 02 00 10 03 06 01 02
00 01 10 03 04 04 04 05 02 00 01 09 08 07 02 00
01 02 09 08 08 08 08 07 00 01 02 00 01 02 00 01
02 00 01 02 00 01 02 00 01 02 00 01 02 00 01 02 

Ce fichier est chargé dans une variable map qui doit être testé par if(map == true) ou if(map.is_open() == true) celà dépend du compilateur utilisé.
La fonction va ensuite entrer dans une boucle for qui va récupérer chacune des valeur du .map puis charger un objet Tile en fonction de la valeur récupérée à la position donnée par l'offset mis à jour à chaque tour de boucle. La dernière partie de la fonction génère un clipping pour gérer le sprite qui doit s'afficher en fonction du tile choisit.

Code source tiré de : http://lazyfoo.net/tutorials/SDL/39_tiling/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Dimension du level
const int LEVEL_WIDTH = 1280;
const int LEVEL_HEIGHT = 960;

//Tile constants
const int TILE_WIDTH = 80;
const int TILE_HEIGHT = 80;
const int TOTAL_TILES = 192;
const int TOTAL_TILE_SPRITES = 12;

//Les différents tile sprites
const int TILE_RED = 0;
const int TILE_GREEN = 1;
const int TILE_BLUE = 2;
const int TILE_CENTER = 3;
const int TILE_TOP = 4;
const int TILE_TOPRIGHT = 5;
const int TILE_RIGHT = 6;
const int TILE_BOTTOMRIGHT =7;
const int TILE_BOTTOM = 8;
const int TILE_BOTTOMLEFT = 9;
const int TILE_LEFT = 10;
const int TILE_TOPLEFT = 11;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Gestion des tiles
class Tile
{
	public :
		//Initialisation de la position et du type de tile
		Tile(int x, int y, int tileType);
		
		//Rendu du tile
		void render(SDL_Rect& camera);
		
		//Obtenir le type de tile
		int getType();
		
		//Obtenir la box de collision
		SDL_Rect getBox();
	
	private :
		//Les attributs des tiles
		SDL_Rect mBox;
		
		//Le type de tile
		int mType;
};


//Une balle qui va bouger dans l'écran
class Dot
{
	public :
		//Les dimensions de la balle
		static const int DOT_WIDTH = 20;
		static const int DOT_HEIGHT = 20;
		
		//Vélocité maximum
		static const int DOT_VEL = 10;
		
		//Initialise les variable et allocation des particules
		Dot();
		
		//Ajuste la vélocité de la balles par pression sur le clavier
		void handleEvent(SDL_Event &e);
		
		//Mouvement de la balle et check de colision contre des tiles
		void move(Tile *tiles[]);
		
		//Centrage de la caméra par rapport à la balle
		void setCamera(SDL_Rect& camera);
		
		//Rendu de la balle à l'écran
		void render(SDL_Rect& camera);
		
	private :
		
		//Box de collision de la balle
		SDL_Rect mBox;
		
		//La vélocité de la balle
		int mVelX, mVelY;
		
};

// Prototypes fonctions
bool init();
bool loadMedia(Tile * tiles[]);
void close(Tile * tiles[]);
bool checkCollision(SDL_Rect a, SDL_Rect b);
bool touchesWall(SDL_Rect box, Tile* tiles[]);
bool setTiles(Tile * tiles[]);

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
TTF_Font *gFont = NULL;

LTexture gDotTexture;
LTexture gTileTexture;
SDL_Rect gTileClips[TOTAL_TILE_SPRITES];


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

// ~~~~~ Méthodes de la classe Tile ~~~~~

//Constructeur
Tile::Tile(int x, int y, int tileType)
{
	//Obtenir la position
	mBox.x = x;
	mBox.y = y;
	
	//Mise en place de la box de collision
	mBox.w = TILE_WIDTH;
	mBox.h = TILE_HEIGHT;
	
	//Obtenir le type de tile
	mType = tileType;
}

//Rendu des tiles
void Tile::render(SDL_Rect & camera)
{
	//Si le tile est à l'écran
	if(checkCollision(camera, mBox) == true)
	{
		gTileTexture.render(mBox.x - camera.x, mBox.y - camera.y, &gTileClips[mType]);
	}
}

//Accesseur de mType
int Tile::getType()
{
	return mType;
}

//Accesseur de mBox
SDL_Rect Tile::getBox()
{
	return mBox;
}

// ~~~~~ Méthodes de la classe Dot ~~~~~

//Constructeur
Dot::Dot()
{	
	//Initialisation du box de collision
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = DOT_WIDTH;
	mBox.h = DOT_HEIGHT;	
	
	//Initialise la vélocité
	mVelX = 0;
	mVelY = 0;
}

//Gestion des événements autour de la balle
void Dot::handleEvent(SDL_Event &e)
{
	//Si une touche est pressée
	if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Ajuster la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY -= DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY += DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX -= DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX += DOT_VEL;
				break;
		}
	}
	//Si la touche est relâchée
	else if(e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Ajuster la élocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY += DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY -= DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX += DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX -= DOT_VEL;
				break;
		}
	}
}

//Mouvement de la balle
void Dot::move(Tile * tiles[])
{
	//Mouvement vers la gauche ou la droite
	mBox.x += mVelX;
	
	//La balle va sortir de l'écran
	if( (mBox.x < 0) || (mBox.x + DOT_WIDTH > LEVEL_WIDTH) || (touchesWall(mBox, tiles) == true))
		//Ne sort pas du niveau
		mBox.x -= mVelX;
	
	//Mouvement de la balle vers la gauche ou la droite
	mBox.y += mVelY;
	
	//La balle va sortir de l'écran
	if((mBox.y < 0) || (mBox.y + DOT_HEIGHT > LEVEL_HEIGHT) || (touchesWall(mBox, tiles) == true))
		//Ne sort pas
		mBox.y -= mVelY;
}

//Ajuster la position de la caméra
void Dot::setCamera(SDL_Rect& camera)
{
	//Centrer la caméra sur la balle
	camera.x = (mBox.x + DOT_WIDTH /2) - SCREEN_WIDTH / 2;
	camera.y = (mBox.y + DOT_HEIGHT / 2) - SCREEN_HEIGHT / 2;
	
	//La caméra ne doit pas sortir du niveau
	if(camera.x < 0)
		camera.x = 0;
	if(camera.y < 0)
		camera.y = 0;
	if(camera.x > LEVEL_WIDTH - camera.w)
		camera.x = LEVEL_WIDTH - camera.w;
	if(camera.y > LEVEL_HEIGHT - camera.h)
		camera.y = LEVEL_HEIGHT - camera.h; 
}

//Rendu de la balle
void Dot::render(SDL_Rect& camera)
{
	gDotTexture.render(mBox.x - camera.x, mBox.y - camera.y);
}


// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia(Tile * tiles[])
{	
	//Chargement des sprites et textures
	
	if(!gDotTexture.loadFromFile("sprite/dot.bmp"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de texture\n");
		return false;
	}
	if(!gTileTexture.loadFromFile("sprite/tiles.png"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de texture\n");
		return false;
	}
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Chargement du tile map
	if(!setTiles(tiles))
	{
		fprintf(stderr, "loadMedia : Echec de chargement du tile set\n");
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close(Tile * tiles[])
{
	//Suppression des tiles
	for(int i = 0; i<TOTAL_TILES; i++)
		//A controler
		if(tiles[i] != NULL)	
		{
			delete tiles[i];
			tiles[i] = NULL;
		}
	
	//Libération de la mémoire pour les textures
	gDotTexture.free();
	gTileTexture.free();
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool checkCollision(SDL_Rect a, SDL_Rect b)
{
	//Les bords du rectangle
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	
	//Calculer les côtés de A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;
	
	//Calculer les côtés de B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;
	
	//Si tous les côtés de A sont hors de B
	if(bottomA <= topB)
		return false;
	if(topA >= bottomB)
		return false;
	if(rightA <= leftB)
		return false;
	if(leftA >= rightB)
		return false;
	
	//Il y a collision
	return true;
}

//Mise en place des tiles
bool setTiles(Tile * tiles[])
{
	int x = 0, y = 0;
	
	//Overture de la carte
	std::ifstream map("sprite/lazy.map");
	if(map.is_open() == false) //Ou map == false
	{
		fprintf(stderr, "setTiles : Echec de chargement de la map file\n");
		return false;
	}
	
	for(int i = 0; i<TOTAL_TILES ; i++)
	{
		int tileType = -1;
		
		//Lecture du tile depuis la map
		map >> tileType;
		if(map.fail())
		{
			fprintf(stderr, "setTiles : Echec de lecture du fichier .map");
			return false;
		}
		
		if((tileType >= 0) && (tileType < TOTAL_TILE_SPRITES))
		{
			tiles[i] = new Tile(x, y, tileType);
			if(tiles[i] == NULL)
			{
				fprintf(stderr, "setTiles : Echec d'allocation de mémoire\n");
				return false;
			}
		}
		else
		{
			fprintf(stderr, "setTiles : tileType invalide %d\n", i);
			return false;
		}
		
		//Move to the next tile spot
		x += TILE_WIDTH;
		
		//Si on atteint la limite
		if(x >= LEVEL_WIDTH)
		{
			//Move back
			x = 0;
			//Move to the next row
			y += TILE_HEIGHT;
		}
	}
	
	//Clip the sprite sheet
	gTileClips[TILE_RED].x = 0;
	gTileClips[TILE_RED].y = 0;
	gTileClips[TILE_RED].w = TILE_WIDTH;
	gTileClips[TILE_RED].h = TILE_HEIGHT;
	
	gTileClips[TILE_GREEN].x = 0;
	gTileClips[TILE_GREEN].y = 80;
	gTileClips[TILE_GREEN].w = TILE_WIDTH;
	gTileClips[TILE_GREEN].h = TILE_HEIGHT;
	
	gTileClips[TILE_BLUE].x = 0;
	gTileClips[TILE_BLUE].y = 160;
	gTileClips[TILE_BLUE].w = TILE_WIDTH;
	gTileClips[TILE_BLUE].h = TILE_HEIGHT;
	
	gTileClips[TILE_TOPLEFT].x = 80;
	gTileClips[TILE_TOPLEFT].y = 0;
	gTileClips[TILE_TOPLEFT].w = TILE_WIDTH;
	gTileClips[TILE_TOPLEFT].h = TILE_HEIGHT;
	
	gTileClips[TILE_LEFT].x = 80;
	gTileClips[TILE_LEFT].y = 80;
	gTileClips[TILE_LEFT].w = TILE_WIDTH;
	gTileClips[TILE_LEFT].h = TILE_HEIGHT;
	
	gTileClips[TILE_BOTTOMLEFT].x = 80;
	gTileClips[TILE_BOTTOMLEFT].y = 160;
	gTileClips[TILE_BOTTOMLEFT].w = TILE_WIDTH;
	gTileClips[TILE_BOTTOMLEFT].h = TILE_HEIGHT;
	
	gTileClips[TILE_TOP].x = 160;
	gTileClips[TILE_TOP].y = 0;
	gTileClips[TILE_TOP].w = TILE_WIDTH;
	gTileClips[TILE_TOP].h = TILE_HEIGHT;
	
	gTileClips[TILE_CENTER].x = 160;
	gTileClips[TILE_CENTER].y = 80;
	gTileClips[TILE_CENTER].w = TILE_WIDTH;
	gTileClips[TILE_CENTER].h = TILE_HEIGHT;
	
	gTileClips[TILE_BOTTOM].x = 160;
	gTileClips[TILE_BOTTOM].y = 160;
	gTileClips[TILE_BOTTOM].w = TILE_WIDTH;
	gTileClips[TILE_BOTTOM].h = TILE_HEIGHT;
	
	gTileClips[TILE_TOPRIGHT].x = 240;
	gTileClips[TILE_TOPRIGHT].y = 0;
	gTileClips[TILE_TOPRIGHT].w = TILE_WIDTH;
	gTileClips[TILE_TOPRIGHT].h = TILE_HEIGHT;
	
	gTileClips[TILE_RIGHT].x = 240;
	gTileClips[TILE_RIGHT].y = 80;
	gTileClips[TILE_RIGHT].w = TILE_WIDTH;
	gTileClips[TILE_RIGHT].h = TILE_HEIGHT;
	
	gTileClips[TILE_BOTTOMRIGHT].x = 240;
	gTileClips[TILE_BOTTOMRIGHT].y = 160;
	gTileClips[TILE_BOTTOMRIGHT].w = TILE_WIDTH;
	gTileClips[TILE_BOTTOMRIGHT].h = TILE_HEIGHT;
	
	map.close();
	return true;	
}

bool touchesWall(SDL_Rect box, Tile * tiles[])
{
	//Go through the tiles	
	for(int i = 0; i<TOTAL_TILES; i++)
		if((tiles[i]->getType() >= TILE_CENTER) && (tiles[i]->getType() <= TILE_TOPLEFT))
			if(checkCollision(box, tiles[i]->getBox()))
				return true;
	
	return false;
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//The level tiles
	Tile * tileSet[TOTAL_TILES];
	
	//Chargement des images et des surfaces
	if(!loadMedia(tileSet))
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	Dot dot;
	
	//Level camera
	SDL_Rect camera = {0,0, SCREEN_WIDTH, SCREEN_HEIGHT};
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			dot.handleEvent(e);
		}
		
		//Mouvement de la balle
		dot.move(tileSet);
		dot.setCamera(camera);
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu des tiles
		for(int i = 0; i< TOTAL_TILES; i++)
		{
			tileSet[i]->render(camera);
		}
		
		//Rendu de texture
		dot.render(camera);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close(tileSet);
	
	return EXIT_SUCCESS;
}

