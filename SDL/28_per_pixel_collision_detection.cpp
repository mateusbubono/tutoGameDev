/*
Boris Merminod 21/09/2017 

Introduction à la collision par pixel. Une image peut être découpée en un ensemble de pixels qui ensembles forment plusieurs lignes empilées de rectangles c'est le cas d'une balle par exemple. En décomposant notre balle en un ensemble de plusieurs petits box collider, il est possible d'obtenir plus de précision dans les collisions testées. 

Dans notre classe Dot notre collider SDL_Rect a alors cédé la place à un vecteur de plusieurs petit box de collisions.
Le constructeur Dot va alors construire cette liste de colliders puis l'aligner les aligner à la position de la balle dans l'espace grâce à la méthode shiftColliders

La méthode move avant de bouger la balle va tester la collision avec tous les box collider de l'autre balle présente dans la scène avant d'autoriser ou non un mouvement

La méthode shiftColliders va ré-aligner à chaque fois l'ensemble des box colliders à la position dans l'espace de la balle lorsqu'elle change

La méthode checkCollision va tester précisément quel collider est en collision

Un exemple de box colliders est montré dans le jeu street fighter via le lien posté ici

Code source tiré de : http://lazyfoo.net/tutorials/SDL/28_per-pixel_collision_detection/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 150;
const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Gestion du temps
class LTimer
{
	public :
		//Initialisation des objets
		LTimer();
	
		//Action sur horloge diverses
		void start();
		void stop();
		void pause();
		void unpause();
	
		//Obtenir le temps du timer
		Uint32 getTicks();
	
		//Checker le status du timer
		bool isStarted();
		bool isPaused();
	
	private :
		//Le temps à l'horloge quand le timer est démarré
		Uint32 mStartTicks;
		
		//Le ticks stocké quand le timer est pausé
		Uint32 mPausedTicks;
		
		//Le status du timer
		bool mPaused;
		bool mStarted;
};

//Point mobile
class Dot
{
	public :
		//Dimension du point
		static const int DOT_WIDTH = 20;
		static const int DOT_HEIGHT = 20;
		
		//Vélocité maximum de l'axe de déplacement du point
		static const int DOT_VEL = 2;
		
		//Initialisation de l'objet
		Dot(int x, int y);
		
		//Récupère l'appuie sur une touche et ajuste la vélocité du point
		void handleEvent(SDL_Event & e);
		
		//Déplace le point
		void move(std::vector<SDL_Rect> & otherColliders);
		
		//Rendu du point à l'écran
		void render();
		
		//Obtenir les boxs de collision
		std::vector<SDL_Rect>& getColliders();
	
	private :
		//Position X et Y du point 
		int mPosX, mPosY;
		
		//Vélocité du point
		int mVelX, mVelY; 
		
		//Boxs de collision de notre point
		std::vector<SDL_Rect> mColliders;
		
		//Mouvement des boxs colliders relatif à la position du point
		void shiftColliders();
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();
bool checkCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b);

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gDotTexture;
TTF_Font * gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

// ~~~~~ Méthodes de la classe LTimer ~~~~~

//Initialisation de l'objet
LTimer::LTimer()
{
	mStartTicks = 0;
	mPausedTicks = 0;
	
	mPaused = false;
	mStarted = false;
}

//Démarrage du timer
void LTimer::start()
{
	//Démarrage du timer
	mStarted = true;
	
	//Retrait de la pause
	mPaused = false;
	
	//Obtenir le temps d'horloge courant
	mStartTicks = SDL_GetTicks();
	mPausedTicks = 0;
}

//Arrêt du timer
void LTimer::stop()
{
	//le timer est stoppé et la pause retirée
	mStarted = false;
	mPaused = false;
	
	//Les attributs sont réinitialisés
	mStartTicks = 0;
	mPausedTicks = 0;
}

//Pause du timer
void LTimer::pause()
{
	//Le timer doit tourner donc ne pas déja être en pause
	if(mStarted == false || mPaused == true)
		return;
	
	mPaused = true;
	//Calcul du ticks de pause
	mPausedTicks = SDL_GetTicks() - mStartTicks;
	mStartTicks = 0;
}

//Reprise d'un timer pausé
void LTimer::unpause()
{
	//Le timer doit tourner et être en pause
	if(mStarted == false || mPaused == false)
		return;
	mPaused = false;
	//Reset le ticks de départ
	mStartTicks = SDL_GetTicks() - mPausedTicks;
	
	//Reset le ticks de pause
	mPausedTicks = 0;
}

//Obtenir le temps du timer
Uint32 LTimer::getTicks()
{
	//Le temps actuel du timer
	Uint32 time = 0;
	
	//Le timer doit tourner 
	if(mStarted == true)
	{
		//Si le timer est en pause
		if(mPaused == true)
			//Retourne le ticks du moment ou le timer a été pausé
			time = mPausedTicks;
		else
			//Retourne le temps courant moins le temps de départ
			time = SDL_GetTicks() - mStartTicks;
	}
		
	return time;
}

//Test si le timer est actif
bool LTimer::isStarted()
{
	return mStarted;
}

//Test si le timer est en pause
bool LTimer::isPaused()
{
	return mPaused && mStarted;
}

// ~~~~~ Méthodes de la classe Dot ~~~~~

Dot::Dot(int x, int y)
{
	//Initialisation de la position
	mPosX = x;
	mPosY = y;
	
	//Création du nombre de box colliders nécessaire
	mColliders.resize(11);
	
	//Initialisation de la vélocité
	mVelX = 0;
	mVelY = 0;
	
	//Initialisation des dimensions des box colliders :
	mColliders[0].w = 6;
	mColliders[0].h = 1;
	
	mColliders[1].w = 10;
	mColliders[1].h = 1;
	
	mColliders[2].w = 14;
	mColliders[2].h = 1;
	
	mColliders[3].w = 16;
	mColliders[3].h = 2;
	
	mColliders[4].w = 18;
	mColliders[4].h = 2;
	
	mColliders[5].w = 20;
	mColliders[5].h = 6;
	
	mColliders[6].w = 18;
	mColliders[6].h = 2;
	
	mColliders[7].w = 16;
	mColliders[7].h = 2;
	
	mColliders[8].w = 14;
	mColliders[8].h = 1;
	
	mColliders[9].w = 10;
	mColliders[9].h = 1;
	
	mColliders[10].w = 6;
	mColliders[10].h = 1;
	
	//Initialisation des colliders relatif à la position du point
	shiftColliders();
}

//Gestion des événements clavier autour du point
void Dot::handleEvent(SDL_Event & e)
{
	//Si une touche est pressée
	if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Ajuster la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY -= DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY += DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX -= DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX += DOT_VEL;
				break;
		}
	}
	//Si une touche est relaché
	else if(e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Ajuster la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY += DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY -= DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX += DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX -= DOT_VEL;
				break;
		}
	}
	
}

//Mouvement du point
void Dot::move(std::vector<SDL_Rect> & otherColliders)
{
	//Bouger le point vers la gauche ou la droite
	mPosX += mVelX;
	shiftColliders();
	
	//Si le point va trop loin à gauche ou à droite
	if((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH) || checkCollision(mColliders, otherColliders))
	{
		mPosX -= mVelX;
		shiftColliders();
	}
		
	//Bouger le point vers le haut ou le bas
	mPosY += mVelY;
	shiftColliders();
	
	//Si le point va trop loin en haut ou en bas
	if((mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT) || checkCollision(mColliders, otherColliders))
	{
		mPosY -= mVelY;
		shiftColliders();
	}
}

//Effectuer le rendu du point
void Dot::render()
{
	gDotTexture.render(mPosX, mPosY);
}

//Gestion de la position des box colliders par rapport à la position du point
void Dot::shiftColliders()
{
	//Position d'une ligne
	int r = 0;
	
	for(int set=0; set<mColliders.size(); ++set)
	{
		//Centrage du box de collision
		mColliders[set].x = mPosX + (DOT_WIDTH - mColliders[set].w) /2;
		//Positionner le box de collision par rapport à sa ligne relative
		mColliders[set].y = mPosY+r;
		//Déplacer la position de la ligne par rapport à la hauteur du box de collision
		r+= mColliders[set].h;
	}
}

//Obtenir la liste des box de collisions
std::vector<SDL_Rect>& Dot::getColliders()
{
	return mColliders;
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	if(gDotTexture.loadFromFile("sprite/dot.bmp") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une texture\n");
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour les textures
	gDotTexture.free();
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	IMG_Quit();
	SDL_Quit();
}

//Gestion de collisions rectangulaires
bool checkCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b)
{
	//Les côtés d'un rectangle
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	
	//Parcours de toutes les boxs de A
	for(int Abox = 0; Abox<a.size(); Abox++)
	{
		//Calcul des côtés du rectangle A
		leftA = a[Abox].x;
		rightA = a[Abox].x + a[Abox].w;
		topA = a[Abox].y;
		bottomA = a[Abox].y + a[Abox].h;
		
		//Parcours de tous les boxs de B
		for(int Bbox = 0; Bbox<b.size();Bbox++)
		{	
	
			//Calcul des côtés du rectangle B
			leftB = b[Bbox].x;
			rightB = b[Bbox].x + b[Bbox].w;
			topB = b[Bbox].y;
			bottomB = b[Bbox].y + b[Bbox].h; 
			
			//Si aucun côté de A n'est en dehors de B
			if(
				(
					(bottomA <= topB) ||
					(topA >= bottomB) ||
					(rightA <= leftB) ||
					(leftA >= rightB)
				) == false
			)
				//Alors une collision est détectée
				return true;
		}
	}
	
	//Si on arrive la il n'y a pas eu de collision détectée
	return false;
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//L'objet point à déplacer
	Dot dot(10,10);
	
	//L'objet point numéro 2 contre lequel on va réaliser des collisions
	Dot otherDot(SCREEN_WIDTH / 4, SCREEN_HEIGHT /4);
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			//Gestion des événements du point
			dot.handleEvent(e);
		}
		
		//Mouvement du point
		dot.move(otherDot.getColliders());
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de texture
		dot.render();
		otherDot.render();
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

