/*
Boris Merminod 31/07/2017

Evènements claviers ajoutés à la boucle d'évènements 

Code source tiré de : http://lazyfoo.net/tutorials/SDL/04_key_presses/index.php

*/

#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

//Enumération pour la sélection de l'image à afficher
enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL
};

bool init();
bool loadMedia();
void close();
SDL_Surface * loadSurface(string path);


// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
SDL_Surface * gScreenSurface = NULL;

//Les sprites sont gérés dans dans une table de surfaces
SDL_Surface * gKeyPressSurfaces[KEY_PRESS_SURFACE_TOTAL];
//La Surface en court est affichée
SDL_Surface * gCurrentSurface = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			else if(e.type == SDL_KEYDOWN) //Si j'appuie sur un bouton du clavier
			{
				switch(e.key.keysym.sym)
				{
					case SDLK_UP : //Flèche du haut
						gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_UP];
						break;
					case SDLK_DOWN : //Flèche du bas
						gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN];	
						break;
					case SDLK_LEFT : //Flèche de gauche
						gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT];	
						break;
					case SDLK_RIGHT : //Flèche de droite
						gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT];	
						break;
					default : // Si rien n'est pressé
						gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];	
						break;
				}
			}
		}
		//Blit des surfaces gHelloWorld sur une surface 
		SDL_BlitSurface(gCurrentSurface, NULL, gScreenSurface, NULL);
		//Mise à jour de la fenêtre après Blit des surfaces
		SDL_UpdateWindowSurface(gWindow);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	//Associer une surface à la fenêtre (Cette surface va permettre de blitter d'autre surfaces)
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	
	return true;
}

//Chargement des images .bmp
bool loadMedia()
{
	
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadSurface("sprite/press.bmp");
	if(gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] = loadSurface("sprite/up.bmp");
	if(gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] = loadSurface("sprite/down.bmp");
	if(gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] = loadSurface("sprite/left.bmp");
	if(gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] = loadSurface("sprite/right.bmp");
	if(gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	return true;
}

//Gestion des images à charger
SDL_Surface * loadSurface(string path)
{
	SDL_Surface * loadedSurface = SDL_LoadBMP(path.c_str());
	if (loadedSurface == NULL)
	{
		fprintf(stderr, "loadSurface : Echec de chargement de l'image %s. SDL Error : %s\n", path, SDL_GetError());
		return NULL;
	}
	
	return loadedSurface;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	// Libération des surfaces
	for(int i = 0;i<KEY_PRESS_SURFACE_TOTAL;i++)
	{
		SDL_FreeSurface(gKeyPressSurfaces[i]);
		gKeyPressSurfaces[i] = NULL;
	}
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	SDL_Quit();
}
