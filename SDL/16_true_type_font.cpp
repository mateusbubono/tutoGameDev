/*
Boris Merminod 16/09/2017

Introduction à la bibliothèque true type font (TTF). Le principe est d'utiliser un style de font pour créer une surface à partir d'un string. Cette surface peut ensuite être convertie en texture pour en effectuer le rendu via un renderer.

Plusieurs fonction de TTF sont utilisées ici :
TTF_Init() : dans la fonction init() pour initialiser cette bibliothèque
TTF_Close() : Dans close() pour fermer cette bibliothèque
TTF_OpenFont() : dans la fonction loadMedia qui prend en paramètre un .ttf et la taille du texte à afficher.
TTF_RenderTextSolid() : va créer à partir du fond chargé une surface. Cette fonction est utilisée par la méthode loadFromRenderedText de la classe LTexture. Le rendu est ensuite effectué par la fonction habituelle de conversion


Code source tiré de : http://lazyfoo.net/tutorials/SDL/16_true_type_fonts/index.php
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h> // Gestion des font string 
#include <stdio.h>
#include <string.h>
#include <iostream>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//font global
TTF_Font * gFont = NULL;

bool init();
bool loadMedia();
void close();

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	//Il s'agit de gérer la transparence d'une certaine couleurs de l'image. Par exemple si un sprite est construit sur un fond cyan, cette fonction va permettre de rendre le fond transparent pour l'affichage du sprite.
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Textures et sprites de la scène

LTexture gTextTexture;


int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//Angle de rotation
	double degrees = 0;
	
	SDL_RendererFlip flipType = SDL_FLIP_NONE;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu du texte
		gTextTexture.render((SCREEN_WIDTH - gTextTexture.getWidth())/2, (SCREEN_HEIGHT - gTextTexture.getHeight())/2);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "init : Echec d'initialisation de SDL_ttf ! SDL_ttf Error : %s\n",TTF_GetError());
		return false;
	}
	
	return true;
}

//Pas de chargement de média dans cette fonction
bool loadMedia()
{
	bool success = true;
	
	//Ouverture du font
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement du font ! SDL_ttf Error : %s\n", TTF_GetError());
		success = false;
	}
	else
	{
		SDL_Color textColor = {0,0,0};
		if(gTextTexture.loadFromRenderedText("The quick brown fox jumps over the lazy dog", textColor) == false)
		{
			fprintf(stderr, "loadMedia : Echec de rendu de texte\n");
			success = false;
		}
	}
	
		
	return success;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour les textures
	gTextTexture.free();
	
	//Suppression des font chargés
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}
