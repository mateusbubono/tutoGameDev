/*
Boris Merminod le 02/03/2018

Introduction au Frame Independent Movement. Le principe ici est d'utiliser un timer pour rendre le mouvement des sprites indépendant du frame rate, donc indépendant de la vitesse pure de la machine. 

	La classe Dot se voit équipée d'une méthode move avec un paramètre timeStep pour prendre en compte le passage du temps. La vélcité est fixée à 640 mais ne va pas correspondre à 640 pixels par frame mais va être ramené à 640 pixels par seconde ce qui réduit la vitesse de déplacement à environ 10 pixels par frame pour une appli qui tourne à 60 frame par seconde.
	La méthode move de cette classe calcul le déplacement en multipliant la vélocité par le temps. On a une vélocité de 640 pixels pour une seconde. Il faut 60 frames pour une seconde environ (en général mais ça dépend des ordis). donc le déplacement par frame sera de 640 x 1/60 soit environ 10 pixels par frames
	
	La boucle d'événement de la fonction main contient un gestionnaire de temps de la classe LTimer. A chaque frame la méthode getTicks va récupérer le temps passé depuis la dernière frame, mais pour cela elle à besoin de restarter le LTimer via la méthode start afin de calculer le temps passé entre chaque frame. Ce temps est converti en seconde et utilisé par la méthode move de la balle pour effectuer le mouvement en fonction du temps.

Code source tiré de : http://lazyfoo.net/tutorials/SDL/44_frame_independent_movement/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Création d'une texture vide
		bool createBlank(int width, int height, SDL_TextureAccess = SDL_TEXTUREACCESS_STREAMING);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		//Rendre l'objet self comme cible de rendu
		void setAsRenderTarget();
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
		
		//Pixel manipulators
		bool lockTexture();
		bool unlockTexture();
		void * getPixels();
		void copyPixels(void * pixels);
		int getPitch();
		Uint32 getPixel32(unsigned int x, unsigned int y);
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		void * mPixels;
		int mPitch;
		
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Classe de gestion du temps
class LTimer
{
	public :
		//Constructeur
		LTimer();
		
		//Actions possibles sur le temps
		void start();
		void stop();
		void pause();
		void unpause();
		
		//Obtenir le temps du timer
		Uint32 getTicks();
		
		//Check le statut du timer
		bool isStarted();
		bool isPaused();
	
	private :
		//Le temps au démarrage du timer
		Uint32 mStartTicks;
		// Le temps au moment ou le timer est en pause
		Uint32 mPausedTicks;
		
		//Statut du timer
		bool mPaused;
		bool mStarted;
};

//Balle mobile
class Dot
{
	public :
		//Les dimensions de la balle
		static const int DOT_WIDTH = 20;
		static const int DOT_HEIGHT = 20;
		
		//Vélocité maximale de la balle 
		static const int DOT_VEL = 640;
		
		//Constructeur
		Dot();
		
		//Ajustement de la vélocité de la balle à l'appuie sur une touche
		void handleEvent(SDL_Event & e);
		
		//Mouvement de la balle
		void move(float timeStep);
		
		//Rendu de la balle
		void render();
		
	private :
		float mPosX, mPosY;
		double mVelX, mVelY;
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gDotTexture;

TTF_Font *gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Conversion de la surface au format d'affichage
	SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
	if(formattedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de convertir la surface en format d'affichage : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une texture vide streamable
	newTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de créer la texture ! SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Permettre le blending
	SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
	
	//Verrouillage de la texture pour la manipulation
	SDL_LockTexture(newTexture, &formattedSurface->clip_rect, &mPixels, &mPitch);
	
	//Copie de la surface de pixels copiée/formattée
	memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	//Dimensions des images
	mWidth = formattedSurface->w;
	mHeight = formattedSurface->h;
	
	//Obtenir les données du pixel dans un format modifiable
	Uint32* pixels = (Uint32*)mPixels;
	int pixelCount = (mPitch/4) * mHeight;
	
	//Map color
	Uint32 colorKey = SDL_MapRGB(formattedSurface->format, 0, 0xFF, 0xFF);
	Uint32 transparent = SDL_MapRGBA(formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00);
	
	//Color key pixel
	for(int i=0; i<pixelCount; i++)
	{
		if(pixels[i] == colorKey)
		{
			pixels[i] = transparent;
		}
	}
	
	//Déverrouillage de la texture pour mise à jour
	SDL_UnlockTexture(newTexture);
	mPixels = NULL;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(formattedSurface); 
	SDL_FreeSurface(loadedSurface); 
	
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

bool LTexture::createBlank(int width, int height, SDL_TextureAccess access)
{
	//Création d'une texture non initialisée
	mTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);
	if(mTexture == NULL)
	{
		fprintf(stderr, "LTexture::createBlank : Impossible de créer la texture vide ! SDL Error : %s\n", SDL_GetError());
	}
	else
	{
		mWidth = width;
		mHeight = height;
	}
	
	return mTexture != NULL;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Set self as render target
void LTexture::setAsRenderTarget()
{
	SDL_SetRenderTarget(gRenderer, mTexture);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Accesseur attribut mPixels
void * LTexture::getPixels()
{
	return mPixels;
}

void LTexture::copyPixels(void * pixels)
{
	//Copie des pixels verrouillés
	memcpy(mPixels, pixels, mPitch * mHeight);
}

//Accesseur mPixels au format Uint32
Uint32 LTexture::getPixel32(unsigned int x, unsigned int y)
{
	//Convert the pixels to 32 bit
	Uint32 * pixels = (Uint32*)mPixels;
	
	//Get the pixel requested
	return pixels[(y * (mPitch/4)) + x];
}

//Accesseur de l'attribut mPitch
int LTexture::getPitch()
{
	return mPitch;
}

bool LTexture::lockTexture()
{
	//La texture est déjà vérrouillée
	if(mPixels != NULL)
	{
		fprintf(stderr, "lockTexture : la texture est déjà vérrouillée\n");
		return false;
	}
	//Vérouillage de la texture
	if(SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
	{
		fprintf(stderr, "lockTexture : Impossible de vérouiller la texture ! %s\n", SDL_GetError());
		return false;
	}
	
	return true;
	
}

bool LTexture::unlockTexture()
{
	if(mPixels == NULL)
	{
		fprintf(stderr, "unlockTexture : La texture n'est pas vérouillée\n");
		return false;
	}
	
	//Déverrouillage de la texture
	SDL_UnlockTexture(mTexture);
	mPixels = NULL;
	mPitch = 0;
	
	return true;
}

//~~~~~ Méthodes de la classe LTimer ~~~~~
LTimer::LTimer()
{
	//Initialisation des attributs
	mStartTicks = 0;
	mPausedTicks = 0;
	mPaused = false;
	mStarted = false;
}

//Démarrage du timer
void LTimer::start()
{
	mStarted = true;
	mPaused = false;
	mStartTicks = SDL_GetTicks();
	mPausedTicks = 0;
}

//Arrêt du timer
void LTimer::stop()
{
	mStarted = false;
	mPaused = false;
	mStartTicks = 0;
	mPausedTicks = 0;
}

//Mise en pause du timer
void LTimer::pause()
{
	//Le timer doit être actif et ne pas déja être en pause
	if(!mStarted || mPaused)
		return;
	
	mPausedTicks = SDL_GetTicks() - mStartTicks;
	mStartTicks = 0;
}

//Reprise du timer en pause
void LTimer::unpause()
{
	//Le timer doit être actif et être en pause
	if(!mStarted && !mPaused)
		return;
	
	mStartTicks = SDL_GetTicks() - mPausedTicks;
	mPausedTicks = 0;
}

Uint32 LTimer::getTicks()
{
	Uint32 time = 0;
	if(mStarted)
	{
		if(mPaused)
			time = mPausedTicks;
		else
			time = SDL_GetTicks() - mStartTicks;
	}
	
	return time;
}

bool LTimer::isStarted()
{
	return mStarted;
}

bool LTimer::isPaused()
{
	return mPaused && mStarted;
}

//~~~~~ Méthodes de la classe Dot ~~~~~

//Constructeur
Dot::Dot()
{
	//Position
	mPosX = 0;
	mPosY = 0;
	
	//Vélocité
	mVelX = 0;
	mVelY = 0;
	
}

//Gestionnaire d'événements de la balle
void Dot::handleEvent(SDL_Event & e)
{
	//Si une touche est pressée
	if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//On ajuste la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY -= DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY += DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX -= DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX = mVelX +DOT_VEL;
				break;
		}
	}
	//Si une touche est relâchée
	else if(e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//On ajuste la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY += DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY -= DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX += DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX -= DOT_VEL;
				break;
		}
	}
}

//Bouger la balle
void Dot::move(float timeStep)
{
	//Bouger la balle vers la gauche ou la droite
	mPosX += mVelX * timeStep;
	
	//Si on sort de la fenêtre
	if(mPosX < 0)
		mPosX = 0;
	else if(mPosX > SCREEN_WIDTH - DOT_WIDTH)
		mPosX = SCREEN_WIDTH - DOT_WIDTH;
	
	//Bouger la balle vers le haut ou vers le bas
	mPosY += mVelY * timeStep;
	
	//Si on sort de la fenêtre
	if(mPosY < 0)
		mPosY = 0;
	else if(mPosY > SCREEN_HEIGHT - DOT_HEIGHT)
		mPosX = SCREEN_HEIGHT - DOT_HEIGHT;
	 
}

//Rendu de la balle
void Dot::render()
{
	gDotTexture.render((int)mPosX, (int)mPosY);
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Seed random
	srand(SDL_GetTicks());
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	//Chargement d'une texture vide
	if(!gDotTexture.loadFromFile("sprite/dot.bmp"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de la texture\n");
		return false;
	}
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	
	//Libération de la mémoire pour les textures
	gDotTexture.free();
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//La balle
	Dot dot;
	//Garder une trace du temps à chaque étape
	LTimer stepTimer;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			dot.handleEvent(e);
		}
		
		//Calculer le time step
		float timeStep = stepTimer.getTicks() / 1000.f;
		
		//Bouger la balle au rythme de chaque time step
		dot.move(timeStep);
		stepTimer.start();
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de la balle
		dot.render();
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

