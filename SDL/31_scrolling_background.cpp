/*
Boris Merminod 21/09/2017 

Introduction au scrolling de background. Le principe est de donner une impression de mouvement en réalisant un déplacement du background en boucle, le lien de la description illustre le scrolling background de façon plutôt claire. Dans notre exemple le background va glisser verticalement de droite à gauche. 

C'est plutôt simple, dans le main on déclare un int nommé scrollingOffset qui va représenter le déplacement de notre background. L'offset est décrémenté à chaque tour de boucle. Lorsque l'offset atteint la taille en largeur du background on rénitialise l'offset. On effectue un double rendu du background le premier avec un offset à 0 et un second background avec l'offset plus la taille du background en largeur. la présence des deux background permet d'éviter d'avoir un trou dans le scrolling.

Code source tiré de : http://lazyfoo.net/tutorials/SDL/31_scrolling_backgrounds/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <vector>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Point mobile
class Dot
{
	public :
		//Dimension du point
		static const int DOT_WIDTH = 20;
		static const int DOT_HEIGHT = 20;
		
		//Vélocité maximum de l'axe de déplacement du point
		static const int DOT_VEL = 2;
		
		//Initialisation de l'objet
		Dot();
		
		//Récupère l'appuie sur une touche et ajuste la vélocité du point
		void handleEvent(SDL_Event & e);
		
		//Déplace le point
		void move();
		
		//Rendu du point à l'écran
		void render();
	
	private :
		//Position X et Y du point 
		int mPosX, mPosY;
		
		//Vélocité du point
		int mVelX, mVelY; 
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gDotTexture;
LTexture gBGTexture;
TTF_Font * gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Color key image
	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
	
	//Création d'une texture depuis une surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "Echec de création d'une structure : %s | SDL Error : %s\n", path.c_str(), SDL_GetError());
		return false;
	}
	
	//Dimensions des images
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(loadedSurface); 
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

// ~~~~~ Méthodes de la classe Dot ~~~~~

Dot::Dot()
{
	//Initialisation de la position
	mPosX = 0;
	mPosY = 0;
	
	//Initialisation de la vélocité
	mVelX = 0;
	mVelY = 0;
}

//Gestion des événements clavier autour du point
void Dot::handleEvent(SDL_Event & e)
{
	//Si une touche est pressée
	if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Ajuster la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY -= DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY += DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX -= DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX += DOT_VEL;
				break;
		}
	}
	//Si une touche est relaché
	else if(e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Ajuster la vélocité
		switch(e.key.keysym.sym)
		{
			case SDLK_UP :
				mVelY += DOT_VEL;
				break;
			case SDLK_DOWN :
				mVelY -= DOT_VEL;
				break;
			case SDLK_LEFT :
				mVelX += DOT_VEL;
				break;
			case SDLK_RIGHT :
				mVelX -= DOT_VEL;
				break;
		}
	}
	
}

//Mouvement du point
void Dot::move()
{
	//Bouger le point vers la gauche ou la droite
	mPosX += mVelX;
	
	//Si le point va trop loin à gauche ou à droite
	if((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH))
		mPosX -= mVelX;
		
	//Bouger le point vers le haut ou le bas
	mPosY += mVelY;
	
	//Si le point va trop loin en haut ou en bas
	if( (mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT))
		mPosY -= mVelY;
}

//Effectuer le rendu du point
void Dot::render()
{
	gDotTexture.render(mPosX, mPosY);
}


// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	if(gDotTexture.loadFromFile("sprite/dot.bmp") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une texture\n");
		return false;
	}
	
	if(gBGTexture.loadFromFile("sprite/bg2.png") == false)
	{
		fprintf(stderr, "loadMedia : Echec de chargement d'une texture\n");
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour les textures
	gDotTexture.free();
	gBGTexture.free();
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	IMG_Quit();
	SDL_Quit();
}


// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	//L'objet point à déplacer
	Dot dot;
	
	//Background scrolling offset
	int scrollingOffset = 0;

	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
			
			//Gestion des événements du point
			dot.handleEvent(e);
		}
		
		//Mouvement du point
		dot.move();
		
		//Scroll background
		--scrollingOffset;
		if(scrollingOffset < - gBGTexture.getWidth())
			scrollingOffset = 0;
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu du background
		gBGTexture.render(scrollingOffset, 0);
		gBGTexture.render(scrollingOffset+gBGTexture.getWidth(), 0);
		
		//Rendu de texture
		dot.render();
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

