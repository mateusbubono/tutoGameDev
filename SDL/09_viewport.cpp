/*
Boris Merminod 10/09/2017

Presentation de la gestion de viewport, c'est à dire la possibilité de splitter l'écran de rendu en plusieurs point de vue (utile pour réaliser une minimap par exemple).

Code source tiré de : http://lazyfoo.net/tutorials/SDL/09_the_viewport/index.php

*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

bool init();
bool loadMedia();
void close();
SDL_Texture * loadTexture(string path);


// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

SDL_Texture * gTexture = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		// Rendu d'un viewport en haut à gauche
		SDL_Rect topLeftViewport;
		topLeftViewport.x = 0;
		topLeftViewport.y = 0;
		topLeftViewport.w = SCREEN_WIDTH / 2;
		topLeftViewport.h = SCREEN_HEIGHT / 2;
		// Création d'un viewport
		SDL_RenderSetViewport(gRenderer, &topLeftViewport);
		//Rendu d'une texture dans le viewport
		SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);
		
		// Rendu d'un viewport en haut à droite
		SDL_Rect topRightViewport;
		topRightViewport.x = SCREEN_WIDTH / 2;
		topRightViewport.y = 0;
		topRightViewport.w = SCREEN_WIDTH / 2;
		topRightViewport.h = SCREEN_HEIGHT / 2;
		SDL_RenderSetViewport(gRenderer, &topRightViewport);
		SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);
		
		//Rendu d'un viewport en bas
		SDL_Rect topBottomViewport;
		topBottomViewport.x = 0;
		topBottomViewport.y = SCREEN_HEIGHT / 2;
		topBottomViewport.w = SCREEN_WIDTH;
		topBottomViewport.h = SCREEN_HEIGHT / 2;
		SDL_RenderSetViewport(gRenderer, &topBottomViewport);
		SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
		
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé");
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	return true;
}

//Pas de chargement de média dans cette fonction
bool loadMedia()
{
	bool success = true;
	gTexture = loadTexture("sprite/viewport.png");
	if(gTexture == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargment de la texture viewport");
		success = false;
	}
	return success;
}

//Gestion des textures à charger
SDL_Texture * loadTexture(string path)
{
	SDL_Texture * newTexture = NULL;
	
	//Utilisation de IMG_Load pour charger une image .png (plusieurs autres formats sont possibles)
	SDL_Surface * loadedSurface = IMG_Load(path.c_str()); 
	if (loadedSurface == NULL)
	{
		//En cas d'erreur, elle sont relevée par IMG_GetError()
		fprintf(stderr, "loadTexture : Echec de chargement de l'image %s. SDL Error : %s\n", path, IMG_GetError());
		return NULL;
	}
	
	//Création d'une texture à partir des pixels de la surface
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadTexture : Impossible de créer une texture à partir de %s. SDL Error : %s\n", path.c_str(), SDL_GetError());
		return NULL;
	}
	//La Surface original étant inutile on la libère
	SDL_FreeSurface(loadedSurface);
	
	return newTexture;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	//Libération de la mémoire pour la texture
	SDL_DestroyTexture(gTexture);
	gTexture = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	IMG_Quit();
	SDL_Quit();
}
