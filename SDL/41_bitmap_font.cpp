/*
Boris Merminod le 25/11/2017

	Introduction au Bitmap font. Le principe ici est d'utiliser le format bitmap pour effectuer du rendu textuelle car après tout le rendu de texte est le rendu d'images de caractères. Le principe du bitmap font est donc de réaliser le rendu de caractères qui se présente sous la forme d'images dans un fichier .png par exemple.

	La classe Texture se voit ajoutée un accesseur getPixel32 car dans notre exemple nous allons travailler avec des pixels de 32 bits à chaque fois.

	Ensuite on ajoute une classe BitmapFont contenant plusieurs méthodes qui sont comme des enveloppes pour un sprite représentant une feuille de symboles. Cette classe contient un constructeur pour initialiser les variables internes, une méthode pour construire la police et une fonction pour effectuer le rendu du texte. A la construction de la police, chaque caractères va être trouvé, puis on calcule une distance pour la nouvelle ligne et l'espace. 

	Dans la classe LTexture. La méthode loadFromFile va s'occuper du color keying au niveau du pixel comme dans le chapitre précédent. Ensuite elle va spécifier le format de texture du pixel en utilisant le flag SDL_PIXELFORMAT_RGBA8888 qui signifie que l'on utilise des pixels RGBA de 32 bits.
	L'accesseur getPixel32 doit retourner le contenu d'un pixel qui est stocké dans la table à une dimension mPixels. Les pixels composent des images en 2D, et sont rangés dans une table à une dimension ainsi pour retourner le contenu du pixel il suffit d'effectuer le calcul suivant permettant de trouver le bon pixel dans la table mPixel : Yoffset * Pitch + Xoffset. La division du pitch par 4 vient pour rappel du fait qu'il y a 4 octets par pixel
	
	Dans la classe LBitmapFont. La méthode buildFont permet de construire la police en définissant par clipping tous les rectangles qui vont définir les sprites. Cette opération doit être précédée du verrouillage de la texture pour réaliser les opérations au pixel. Les caractères de l'images doivent être organisés sous la formes de cellules de même taille il doit y avoir 16 colonnes et 16 lignes et les caractères doivent être dans l'ordre ASCII. La méthode va alors détecter la couleurs du fond, ce qui ne sera pas éliminé pourra constituer ce que l'on va avoir à afficher. 
		La taille des cellules est ensuite calculée et on va garder dans une variable top la taille du glyphe dont la taille est la plus élevée. La variable baseA va contenir la valeur offset de la base du caractère A qui va être gardé comme ligne de base pour le rendu des caractères. La variable currentChar va garder une trace du caractère courant que l'on recherche. 
		Une double boucle va parcourir chaque cellule. Par défaut la position de la cellules est fixée au sommet gauche de chaque cellule, la taille des caractères est par défaut celles des cellules.
		On va ensuite pour chacune des cellules parcourir chaque pixel. Sur chaque pixel on va boucler colonne par colonne jusqu'à tomber sur un pixel ne correspondant pas à la couleur du fond de l'image. A ce moment on va fixer la position x du caractère trouvé à la position x du pixel correspondant. Ensuite on va parcourir la cellule de la fin vers le début de la même façon afin de déterminer la largeur du caractère. Ensuite on va parcourir la cellule ligne par ligne pour détecter la hauteur du caractère. Enfin on va parcourir la cellule ligne par ligne en commançant par la fin pour déterminer la position en y de la cellule.
	A la fin de ce procédé on va calculer la taille de l'espace : défini comme la moitié de la largeur d'une cellule. On va également calculer la taille du saut de ligne en utilisant la ligne de base et la taille du caractère le plus haut
	
	La méthode renderText va effectuer le rendu textuel en retrouvant les caractères enregistrés dans la table mChars en utilisant le code ascii

Code source tiré de : http://lazyfoo.net/tutorials/SDL/41_bitmap_fonts/index.php
*/

// ~~~~~En-tête du programme~~~~~

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <cmath>
//using namespace std;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Création d'une classe pour la gestion des textures
class LTexture
{
	public :
		//Initialisation de l'objet
		LTexture();
		//Destruction de l'objet
		~LTexture();
		
		//Chargement d'une image
		bool loadFromFile(std::string path);
		
		//Créer une image à partir d'un font string
		bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
		
		//Désallouer une image
		void free();
		//Utilisation de la modulation de couleurs
		void setColor(Uint8 red, Uint8 green, Uint8 blue);
		//Gestion du blending
		void setBlendMode(SDL_BlendMode blending);
		//Gestion de la modulation alpha
		void setAlpha(Uint8 alpha);
		//Rendu d'une texture à un point de coordonnées donné
		void render(int x, int y, SDL_Rect * clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
		
		//Obtenir les dimensions de l'image
		int getWidth();
		int getHeight();
		
		//Pixel manipulators
		bool lockTexture();
		bool unlockTexture();
		void * getPixels();
		int getPitch();
		Uint32 getPixel32(unsigned int x, unsigned int y);
	
	private :
		//La Texture 
		SDL_Texture * mTexture;
		void * mPixels;
		int mPitch;
		
		//Dimension de l'image
		int mWidth;
		int mHeight;		
};

//Le bitmap font
class LBitmapFont
{
	public : 
		//Constructeur
		LBitmapFont();
		
		//Générer le font
		bool buildFont(LTexture * bitmap);
		
		//Rendu du texte
		void renderText(int x, int y, std::string text);
		
	private :
		//Texture du font
		LTexture * mBitmap;
		
		//Les caractères individuels de la surface
		SDL_Rect mChars[256];
		
		//Variables d'espace
		int mNewLine, mSpace;
};

// Prototypes fonctions
bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
//Dans ce chapitre nous allons créer un Renderer
SDL_Renderer * gRenderer = NULL;

//Textures de la scène
LTexture gBitmapTexture;
LBitmapFont gBitmapFont;
TTF_Font *gFont = NULL;


// ~~~~~Méthodes de la classe LTexture~~~~~

//Constructeur de la classe LTexture
LTexture::LTexture()
{
	//Initialisation
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Destructeur de la classe LTexture
LTexture::~LTexture()
{
	free();
}

//Chargement d'une Texture de la classe LTexture
bool LTexture::loadFromFile(std::string path)
{
	//Suppression d'une éventuel texture précédente
	free();
	// Texture finale
	SDL_Texture * newTexture = NULL;
	
	//Chargement d'une image depuis un path spécifié
	SDL_Surface * loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Echec de chargement de la texture : %s | SDL_Image Error : %s\n", path.c_str(), IMG_GetError());
		return false;
	}
	
	//Conversion de la surface au format d'affichage
	SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
	if(formattedSurface == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de convertir la surface en format d'affichage : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une texture vide streamable
	newTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
	if(newTexture == NULL)
	{
		fprintf(stderr, "loadFromFile : Impossible de créer la texture ! SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Permettre le blending
	SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
	
	//Verrouillage de la texture pour la manipulation
	SDL_LockTexture(newTexture, &formattedSurface->clip_rect, &mPixels, &mPitch);
	
	//Copie de la surface de pixels copiée/formattée
	memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	//Dimensions des images
	mWidth = formattedSurface->w;
	mHeight = formattedSurface->h;
	
	//Obtenir les données du pixel dans un format modifiable
	Uint32* pixels = (Uint32*)mPixels;
	int pixelCount = (mPitch/4) * mHeight;
	
	//Map color
	Uint32 colorKey = SDL_MapRGB(formattedSurface->format, 0, 0xFF, 0xFF);
	Uint32 transparent = SDL_MapRGBA(formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00);
	
	//Color key pixel
	for(int i=0; i<pixelCount; i++)
	{
		if(pixels[i] == colorKey)
		{
			pixels[i] = transparent;
		}
	}
	
	//Déverrouillage de la texture pour mise à jour
	SDL_UnlockTexture(newTexture);
	mPixels = NULL;
	
	//Suppression des surfaces devenues inutiles
	SDL_FreeSurface(formattedSurface); 
	SDL_FreeSurface(loadedSurface); 
	
	mTexture = newTexture;
	
	return true;
}

bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Suppression de la texture précédente
	free();
	
	//Rendu d'une surface de type texte
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if(textSurface == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte depuis une surface de type texte! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	//Créer la texture à partir de la surface
	mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	if(mTexture == NULL)
	{
		fprintf(stderr, "loadFromRenderedText : Echec de création du rendu de texte ! SDL Error : %s\n",SDL_GetError());
		return false;
	}
	
	//Obtenir les dimensions de l'image
	mWidth = textSurface->w;
	mHeight = textSurface->h;
	
	//Suppression de la surface
	SDL_FreeSurface(textSurface);
	
	return true;
}

//Suppression d'une Texture de la classe LTexture
void LTexture::free()
{
	//Libère la mémoire allouée pour une texture
	if(mTexture == NULL)
		return;
	SDL_DestroyTexture(mTexture);
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

//Modulation de la couleur d'une texture
void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

//Gestion du blending
void LTexture::setBlendMode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(mTexture, blending);
}

//Modulation de l'alpha de la Texture
void LTexture::setAlpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

//Rendu d'une texture de la classe LTexture	
void LTexture::render(int x, int y, SDL_Rect * clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	// Mise en place de l'espace de rendu et cet espace à l'écran
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};
	
	if(clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

//Accesseur de l'attribut mWidth de la classe LTexture
int LTexture::getWidth()
{
	return mWidth;
}

//Accesseur de l'attribut mHeight de la classe LTexture
int LTexture::getHeight()
{
	return mHeight;
}

//Accesseur attribut mPixels
void * LTexture::getPixels()
{
	return mPixels;
}

//Accesseur mPixels au format Uint32
Uint32 LTexture::getPixel32(unsigned int x, unsigned int y)
{
	//Convert the pixels to 32 bit
	Uint32 * pixels = (Uint32*)mPixels;
	
	//Get the pixel requested
	return pixels[(y * (mPitch/4)) + x];
}

//Accesseur de l'attribut mPitch
int LTexture::getPitch()
{
	return mPitch;
}

bool LTexture::lockTexture()
{
	//La texture est déjà vérrouillée
	if(mPixels != NULL)
	{
		fprintf(stderr, "lockTexture : la texture est déjà vérrouillée\n");
		return false;
	}
	//Vérouillage de la texture
	if(SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
	{
		fprintf(stderr, "lockTexture : Impossible de vérouiller la texture ! %s\n", SDL_GetError());
		return false;
	}
	
	return true;
	
}

bool LTexture::unlockTexture()
{
	if(mPixels == NULL)
	{
		fprintf(stderr, "unlockTexture : La texture n'est pas vérouillée\n");
		return false;
	}
	
	//Déverrouillage de la texture
	SDL_UnlockTexture(mTexture);
	mPixels = NULL;
	mPitch = 0;
	
	return true;
}

// ~~~~~ Méthodes de la classe LBitmapFont ~~~~~

LBitmapFont::LBitmapFont()
{
	//Initialisation des attributs
	mBitmap = NULL;
	mNewLine = 0;
	mSpace = 0;
} 

bool LBitmapFont::buildFont(LTexture * bitmap)
{
	if(!bitmap->lockTexture())
	{
		fprintf(stderr, "lBitmapFont::buildFont : Impossible de verrouiller le bitmap font texture\n");
		return false;
	}
	
	//Mise en place de la couleur du font
	Uint32 bgColor = bitmap->getPixel32(0,0);
	
	//Mise en place des dimension d'une cellule
	int cellW = bitmap->getWidth() / 16;
	int cellH = bitmap->getHeight() / 16;
	
	//Variables de la new line
	int top = cellH;
	int baseA = cellH;
	
	//Setting du caractère courant
	int currentChar = 0;
	
	//Parcours des lignes de cellules
	for(int rows=0; rows<16; rows++)
	{
		//Parcours des colonnes de cellules
		for(int cols=0; cols<16; cols++) 
		{
			//Set the character offset
			mChars[currentChar].x = cellW * cols;
			mChars[currentChar].y = cellH * rows;
			
			//Set the dimensions of the character
			mChars[currentChar].w = cellW;
			mChars[currentChar].h = cellH;
			
			//Find Left Side
			//Parcours des colonnes de pixel
			for(int pCol = 0; pCol<cellW; pCol++)
			{
				//Parcours des lignes de pixels
				for(int pRow = 0; pRow<cellH; pRow++)
				{
					//Obtenir le décalage par pixel
					int pX = (cellW * cols) + pCol;
					int pY = (cellH * rows) + pRow;
					
					//if a non colorkey pixel is found
					if(bitmap->getPixel32(pX,pY) != bgColor)
					{
						//Mise en place du décalage en x
						mChars[currentChar].x = pX;
						
						//Sortie de la boucle
						pCol = cellW;
						pRow = cellH;
					}
				}
			}
			
			//Find the Right Side
			//Parcourir les colonnes de pixels
			for(int pColW = cellW-1; pColW>=0; pColW--)
			{
				//Parcourir les lignes de pixels
				for(int pRowW = 0; pRowW < cellH; pRowW++)
				{
					//Obtenir le décalage de pixel
					int pX = (cellW * cols) + pColW;
					int pY = (cellH * rows) + pRowW;
					
					//if a non colorkey pixel is found
					if(bitmap->getPixel32(pX, pY) != bgColor)
					{
						//Set the width
						mChars[currentChar].w = (pX - mChars[currentChar].x) + 1;
						//Sortie de la boucle
						pColW = -1;
						pRowW = cellH;
					} 
				}
			}
			
			//Find the top
			//Parcours des lignes de pixel
			for(int pRow = 0; pRow<cellH; pRow++)
			{
				//Parcours des colonnes de pixel
				for(int pCol = 0; pCol < cellW; pCol++)
				{
					//Obtenir le décalage des pixels
					int pX = (cellW * cols) + pCol;
					int pY = (cellH * rows) + pRow;
					
					//If a non colorkey pixel is found
					if(bitmap->getPixel32(pX, pY) != bgColor)
					{
						//If a new top is found
						if(pRow < top)
						{
							top = pRow;
						}
						
						//Sortie de la boucle
						pCol = cellW;
						pRow = cellH;
					}
				}
			}
			
			//Find Bottom of A
			if(currentChar == 'A')
			{
				//Parcours des lignes de pixels
				for(int pRow = cellH - 1; pRow >= 0; pRow--)
				{
					//Parcours des colonnes de pixels
					for(int pCol = 0 ; pCol < cellW; pCol++)
					{
						//Obtenir le décalage des pixels
						int pX = (cellW * cols) + pCol;
						int pY = (cellH * rows) + pRow;
						
						//If a non colorkey pixel is found
						if(bitmap->getPixel32(pX, pY) != bgColor)
						{
							// Bottom of 'A' is found
							baseA = pRow;
							
							//Sortie de la boucle
							pCol = cellW;
							pRow = -1;
						}
					}
				}
			}
			
			//Aller au prochain caractère
			printf("%c, %i %i, %i\n",currentChar, mChars[currentChar].x, mChars[currentChar].y, currentChar);
			currentChar++;	
		}
	}
	
	//Calcul de l'espace
	mSpace = cellW/2;
	
	//Calcul du new line
	mNewLine = baseA - top;
	
	//Lop off excess top pixels
	for(int i = 0; i<256; i++)
	{
		mChars[i].y += top;
		mChars[i].h -= top;
	}
	
	bitmap->unlockTexture();
	mBitmap = bitmap;
	
	return true;
}

void LBitmapFont::renderText(int x, int y, std::string text)
{
	//Si le font a été construit
	if(mBitmap != NULL)
	{
		//Temp offsets
		int curX = x, curY = y;
		
		//Parcourir le texte
		for(int i = 0; i<text.length(); i++)
		{
			//Si le caractère courant est un espace
			if(text[i] == ' ')
			{
				//Move over
				curX += mSpace;
			}
			//Si le caractère courant est un newline
			else if(text[i] == '\n')
			{
				//Move down
				curY += mNewLine;
				
				//Move back
				curX = x;
			}
			else
			{
				//Get the ASCII value of the character
				int ascii = (unsigned char)text[i];
				
				//printf("%i, %c, %i | %i, %i\n",i, text[i],ascii, mChars[ascii].x, mChars[ascii].y);
				
				//Montrer le caractère
				mBitmap->render(curX, curY, &mChars[ascii]);
				
				//Move over the width of the character with one pixel of padding
				curX += mChars[ascii].w + 1;
			}
		}
	}
}

// ~~~~~ Fonctions du programme ~~~~~

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL du mode vidéo et du mode audio
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Set Texture filtering to linear
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
	{
		fprintf(stderr, "Attention Linear Texture Filtering non activé\n");
	}
	
	//Seed random
	srand(SDL_GetTicks());
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", IMG_GetError());
		return false;
	}
	
	// Création d'un renderer pour effectuer le rendu d'une texture, renderer de type vsynced
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED| SDL_RENDERER_PRESENTVSYNC);
	if(gRenderer == NULL)
	{
		fprintf(stderr, "Init : Echec de création du 'Renderer'. SDL Error : %s\n", SDL_GetError());
		return false;
	}
	
	//Choix de la couleurs pour diverse opération de rendu
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	
	//Initialisation de IMG
	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		fprintf(stderr, "Init : Echec d'initialisation de SDL_Image. IMG Error : %s\n",IMG_GetError());
		return false;
	}
	
	//Initialisation de SDL_ttf
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "loadMedia : Initialisation impossible de SDL_ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}

//Chargement textures et audio
bool loadMedia()
{	
	//Chargement des sprites et textures
	
	if(!gBitmapTexture.loadFromFile("sprite/lazyfont.png"))
	{
		fprintf(stderr, "loadMedia : Echec de chargement de texture\n");
		return false;
	}
	
	gBitmapFont.buildFont(&gBitmapTexture);
	
	//Chargement d'un font string
	gFont = TTF_OpenFont("sprite/lazy.ttf", 28);
	if(gFont == NULL)
	{
		fprintf(stderr, "loadMedia : Impossible de charger un font ttf ! SDL_ttf Error : %s\n", TTF_GetError());
		return false;
	}
	
	return true;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	
	//Libération de la mémoire pour les textures
	gBitmapTexture.free();
	
	//Free global font
	TTF_CloseFont(gFont);
	gFont = NULL;
	
	// Libération de la mémoire allouée pour le renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// ~~~~~ Fonction main ~~~~~

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Effacer l'écran (le renderer)
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);
		
		//Rendu de la figure
		gBitmapFont.renderText(0, 0, "Bitmap Font:\nABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz\n0123456789");
		
		//Mise à jour du renderer
		SDL_RenderPresent(gRenderer);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

