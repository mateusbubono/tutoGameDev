/*
Boris Merminod 31/07/2017

Optimisation d'une surface et soft stretching : Le principe ici est de faire en sorte que les images chargées soient toutes du même format. Si une image brut de 24 bit est blittée sur une surface de 32 bits, la conversion qui sera réalisée à la volée va prendre du temps. On peut éviter ça via une fonction d'optimisation : SDL_ConvertSurface. 
On peut via l'utilisation d'une structure nommée SDL_Rect réaliser un changement d'échelle (scale) d'une image. L'image ainsi modifiée peut être blitée via l'utilisation d'une fonction spécifique : SDL_BlitScaled

Code source tiré de : http://lazyfoo.net/tutorials/SDL/05_optimized_surface_loading_and_soft_stretching/index.php

*/

#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

bool init();
bool loadMedia();
void close();
SDL_Surface * loadSurface(string path);


// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
SDL_Surface * gScreenSurface = NULL;
SDL_Surface * gStretchedSurface = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
			{
				quit = true; //Déclenche la fin de la boucle d'évènements
			}
		}
		
		//Positionnement d'un rectangle dans l'écran ainsi que déterminer son échelle
		SDL_Rect stretchRect;
		stretchRect.x = 0;
		stretchRect.y = 0;
		stretchRect.w = SCREEN_WIDTH/2;
		stretchRect.h = SCREEN_HEIGHT/2;
		
		//Blit de la surface à laquelle on associe la structure SDL_Rect indiquant la position et le changement d'échelle
		SDL_BlitScaled(gStretchedSurface, NULL, gScreenSurface, &stretchRect);
		//Mise à jour de la fenêtre après Blit des surfaces
		SDL_UpdateWindowSurface(gWindow);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	//Associer une surface à la fenêtre (Cette surface va permettre de blitter d'autre surfaces)
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	
	return true;
}

//Chargement des images .bmp
bool loadMedia()
{
	
	gStretchedSurface = loadSurface("sprite/stretch.bmp");
	if(gStretchedSurface == NULL)
	{
		fprintf(stderr, "Echec de chargement d'image\n");
		return false;
	}
	
	return true;
}

//Gestion des images à charger
SDL_Surface * loadSurface(string path)
{
	SDL_Surface * optimizedSurface = NULL;
	SDL_Surface * loadedSurface = SDL_LoadBMP(path.c_str());
	if (loadedSurface == NULL)
	{
		fprintf(stderr, "loadSurface : Echec de chargement de l'image %s. SDL Error : %s\n", path, SDL_GetError());
		return NULL;
	}
	
	//La fonction retourne une copie de la surface optimisée
	optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
	if(optimizedSurface == NULL)
	{
		fprintf(stderr, "loadSurface : Echec d'optimisation de surface. SDL Error : %s\n", SDL_GetError());
		return NULL;
	}
	//La Surface original étant inutile on la libère
	SDL_FreeSurface(loadedSurface);
	
	return optimizedSurface;
}


//Libération de la mémoire et fermeture de la SDL
void close()
{
	// Libération des surfaces
	SDL_FreeSurface(gStretchedSurface);
	gStretchedSurface = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	SDL_Quit();
}
