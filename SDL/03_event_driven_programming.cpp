/*
Boris Merminod 31/07/2017

Ajout d'une boucle d'évènements, avec l'évènement SDL_QUIT qui détecte la fermeture d'une fenêtre

Code source tiré de : http://lazyfoo.net/tutorials/SDL/03_event_driven_programming/index.php
*/

#include <SDL2/SDL.h>
#include <stdio.h>

bool init();
bool loadMedia();
void close();

// Variables globales

// Gestion fenêtre et surface à l'écran
SDL_Window * gWindow = NULL;
SDL_Surface * gScreenSurface = NULL;
SDL_Surface * gXOut = NULL;

//Dimension de l'écran
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main (int argc, char* argv [])
{
	//Initilisation SDL et création de la fenêtre
	if(!init())
	{
		fprintf(stderr, "Echec d'initialisation\n");
		return EXIT_FAILURE;
	}
	
	//Chargement des images et des surfaces
	if(!loadMedia())
	{
		fprintf(stderr, "Echec de chargement des images\n");
		return EXIT_FAILURE;
	}
	
	// Boucle d'évènements
	bool quit = false;
	SDL_Event e;
	while(!quit)
	{
		//On guette les évènements ici
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT) //Fermeture de la fenêtre 
				quit = true; //Déclenche la fin de la boucle d'évènements
		}
		//Blit des surfaces gHelloWorld sur une surface 
		SDL_BlitSurface(gXOut, NULL, gScreenSurface, NULL);
		//Mise à jour de la fenêtre après Blit des surfaces
		SDL_UpdateWindowSurface(gWindow);
	}
	
	//Libération de la mémoire + fermeture de la SDL
	close();
	
	return EXIT_SUCCESS;
}

//Initialisation SDL, construction d'une fenêtre
bool init()
{
	//Initialisation de la SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "init : Echec d'initialisation de la SDL : %s\n", SDL_GetError());
		return false;
	}
	
	//Création d'une fenêtre	
	gWindow = SDL_CreateWindow("Tuto SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		fprintf(stderr, "Init : Echec de création d'une fenêtre : %s\n", SDL_GetError());
		return false;
	}
	
	//Associer une surface à la fenêtre (Cette surface va permettre de blitter d'autre surfaces)
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	
	return true;
}

//Chargement d'une image .bmp
bool loadMedia()
{
	//Chargement d'une image .bmp associée à une surface
	gXOut = SDL_LoadBMP("sprite/xout.bmp");
	if (gXOut == NULL)
	{
		fprintf(stderr, "loadMedia : Echec de chargement de m'image %s, SDL Error : %s\n", "xout.bmp", SDL_GetError());
		return false;
	}
	
	return true;
}

//Libération de la mémoire et fermeture de la SDL
void close()
{
	// Libération de la surface gHelloWorld
	SDL_FreeSurface(gXOut);
	gXOut = NULL;
	
	//Libération de la surface et de la mémoire allouée pour créer une fenêtre
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	//Fermeture de la SDL
	SDL_Quit();
}
